package com.accelfin.server.demo.common.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MathUtils {
    public static BigDecimal getChangePercent(BigDecimal initial, BigDecimal update) {
        BigDecimal difference = update.subtract(initial);
        BigDecimal percent = difference.divide(initial, RoundingMode.HALF_DOWN);
        return percent.multiply(BigDecimal.valueOf(100));
    }
}
