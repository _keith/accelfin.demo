package com.accelfin.server.demo.common.messaging;

import com.accelfin.server.ServiceModelBase;
import com.accelfin.server.SessionToken;
import com.accelfin.server.messaging.Connection;
import com.accelfin.server.messaging.Connections;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.esp.EventContext;
import com.esp.Router;
import com.esp.disposables.Disposable;
import com.esp.reactive.EventObservable;
import com.google.inject.Inject;
import com.google.protobuf.Message;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Proxies message to the model responsible for the underlying mq connection for the message in question.
 */
public class ConnectionBroker {

    private Router<ServiceModelBase> _router;
    private Connection _webConnection;
    private Connection _internalConnection;

    @Inject
    public ConnectionBroker(Router<ServiceModelBase> router, Connections connections) {
        _router = router;
        _webConnection = connections.getConnection(ConnectionIds.WEB);
        _internalConnection = connections.getConnection(ConnectionIds.INTERNAL);
    }

    public void setOperationStatus(String operationName, boolean isAvailable) {
        // TODO we need to enforce unique operation names, currently this just sets the operation against both connections
        _internalConnection.getStatusMonitor().setOperationStatus(operationName, isAvailable);
        _webConnection.getStatusMonitor().setOperationStatus(operationName, isAvailable);
    }


    /**
     * TODO
     * - this should move to connection to it's all in one place
     * - can we remote the
     */
    public <TEvent extends InboundMessageEvent> EventObservable<TEvent, EventContext, ServiceModelBase> sendRequest(
            Class<TEvent> responseEvent,
            String serviceType,
            String operationName,
            String correlationId,
            Message payload
    ) {
        return EventObservable.create(o -> {
            Disposable disposable = _router.getEventObservable(responseEvent)
                    .where((e, c, m) -> Objects.equals(e.getCorrelationId(), correlationId))
                    .take(1)
                    .observe(o::onNext);
            // TODO what happens if this blows up?
            // TODO why not just use the same path as we do for sending responses, i.e. with a session token?
            sendRequestMessage(serviceType, operationName, correlationId, payload);
            return disposable;
        });
    }

    public void sendRequestMessage(String serviceType, String operationName, String correlationId, Message payload) {
        // any requests only happen on the the internal messaging connection
        _internalConnection.sendRequestMessage(serviceType, operationName, correlationId, payload);
    }

    public void sendStreamResponseMessage(String operationName, Message payload, boolean streamCompleted, SessionToken... sessionTokens) {
        ArrayList<String> webSessionIds = new ArrayList<>();
        ArrayList<String> internalSessionIds = new ArrayList<>();
        for (SessionToken token : sessionTokens) {
            if (token.getConnectionId().equals(_webConnection.getConnectionId())) {
                webSessionIds.add(token.getSessionId());
            } else if (token.getConnectionId().equals(_internalConnection.getConnectionId())) {
                internalSessionIds.add(token.getSessionId());
            }
        }
        if (webSessionIds.size() > 0) {
            _webConnection.sendStreamResponseMessage(operationName, payload, streamCompleted, webSessionIds.toArray(new String[webSessionIds.size()]));
        }
        if (internalSessionIds.size() > 0) {
            _internalConnection.sendStreamResponseMessage(operationName, payload, streamCompleted, internalSessionIds.toArray(new String[internalSessionIds.size()]));
        }
    }
//
//    public void sendRpcRequest(String serviceType, String operationName, Object payload) {
//        // any requests only happen on the the internal messaging connection
//        _internalMessaging.sendRpcRequest(serviceType, operationName, payload);
//    }

    public void sendRpcResponse(InboundMessageEvent initialEvent, Message payload) {
        if (initialEvent.getSessionToken().getConnectionId().equals(_webConnection.getConnectionId())) {
            _webConnection.sendRpcResponse(initialEvent, payload);
        } else if (initialEvent.getSessionToken().getConnectionId().equals(_internalConnection.getConnectionId())) {
            _internalConnection.sendRpcResponse(initialEvent, payload);
        }
    }

//    public void sendRpcResponse(String operationName, Object payload, SessionToken token) {
//        if(token.getConnectionId().equals(_webMessaging.getConnectionId())) {
//            _webMessaging.sendRpcResponse(operationName, payload,  token);
//        } else if(token.getConnectionId().equals(_internalMessaging.getConnectionId())) {
//            _internalMessaging.sendRpcResponse(operationName, payload, token);
//        }
//    }
}
