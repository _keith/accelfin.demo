package com.accelfin.server.demo.common.model;

public class CurrencyPair {
    private String _symbol;
    private String _base;
    private String _terms;
    private int _pipsPosition;
    private int _ratePrecision;
    private boolean _isEnabled;

    public CurrencyPair(String symbol, int pipsPosition, int ratePrecision, boolean isEnabled) {
        _symbol = symbol;
        _base = symbol.substring(0, 3);
        _terms = symbol.substring(3, 3);
        _pipsPosition = pipsPosition;
        _ratePrecision = ratePrecision;
        _isEnabled = isEnabled;
    }

    public String getSymbol() {
        return _symbol;
    }

    public String getBase() {
        return _base;
    }

    public String getTerms() {
        return _terms;
    }

    public int getPipsPosition() {
        return _pipsPosition;
    }

    public int getRatePrecision() {
        return _ratePrecision;
    }

    public boolean isEnabled() {
        return _isEnabled;
    }
}
