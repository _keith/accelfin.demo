package com.accelfin.server.demo.common;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TimeZone;

public abstract class ServiceBootstrapper {

    private static Logger logger = LoggerFactory.getLogger(ServiceBootstrapper.class);

    private Injector _injector;

    public void run() throws Exception {
        String startupMessage = "\n" +
        "________    _________                           .__              \n" +
        "\\______ \\  /   _____/   ______ ______________  _|__| ____  ____  \n" +
        " |    |  \\ \\_____  \\   /  ___// __ \\_  __ \\  \\/ /  |/ ___\\/ __ \\ \n" +
        " |    `   \\/        \\  \\___ \\\\  ___/|  | \\/\\   /|  \\  \\__\\  ___/ \n" +
        "/_______  /_______  / /____  >\\___  >__|    \\_/ |__|\\___  >___  >\n" +
        "        \\/        \\/       \\/     \\/                    \\/    \\/ ";
        logger.info(startupMessage);
        logger.debug("debug enabled?");
        logger.info("info enabled?");
        logger.warn("warn enabled?");
        logger.error("error enabled?");
        logger.trace("trace enabled?");
        setDefaultTimeZone();
        printStartupMessage();
        logger.debug("Configuring Container");
        configureContainer();
        logger.debug("Starting Event Store");
        startEventStore();
        logger.debug("Starting Model");
        startModel();
        logger.debug("bootstrapper done");
    }

    private void setDefaultTimeZone() {
        logger.debug("Setting default timezone to UTC");
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    protected Injector getInjector() {
        return _injector;
    }

    protected abstract Collection<AbstractModule> getContainerModules();

    private void configureContainer() {
        List<AbstractModule> modules = new ArrayList<>();
        modules.addAll(getContainerModules());
        _injector = Guice.createInjector(modules);
    }

    protected abstract void startModel();

    private void startEventStore() {
    }

    private void printStartupMessage() {
        logger.debug("Service up");
        logger.debug("Java version: {}", System.getProperty("java.version"));
        logger.debug("Java VM {}", System.getProperty("java.vm.name"));
    }
}
