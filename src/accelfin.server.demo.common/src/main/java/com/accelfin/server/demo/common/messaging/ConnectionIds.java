package com.accelfin.server.demo.common.messaging;

public class ConnectionIds {
    public static final String WEB = "web";
    public static final String INTERNAL = "internal";
}
