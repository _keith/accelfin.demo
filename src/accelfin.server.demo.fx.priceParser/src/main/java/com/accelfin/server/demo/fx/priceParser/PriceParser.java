package com.accelfin.server.demo.fx.priceParser;

import java.io.*;
import java.nio.file.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Iterator;
import java.util.stream.Stream;

/**
 * This program parses tick prices available from here https://www.truefx.com/?page=downloads
 * It runs with a hardcoded path which assums all tick prices have been extracted to.
 * It reads in all .csv files, reduces the output to a days work of bid/ask at a much reduced tick frequency and writes files to an output directory
 *
 * You need to first create the output directory.
 * You need to compress resulting files in the output directory to prices.zip and add them to the fx demo resources for consumption.
 */
public class PriceParser {
    public static void main(String[] args) throws IOException {
        new PriceParser().run();

    }

    private void run() throws IOException {
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**/*.csv");
        Files.list(Paths.get("/home/keith/dev/prices"))
               // .filter(matcher::matches)
                .forEach(filePath -> {
            try {
                if(matcher.matches(filePath)) {
                    reduce(filePath);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void reduce(Path inputFilePath) throws IOException {

        PricerTimeTracker tracker = new PricerTimeTracker();

        int totalCount = 0;
        int procuredCount = 0;

        File outputFile = getOutputFile(inputFilePath);
        if (outputFile.exists()) {
            outputFile.delete();
        }
        outputFile.createNewFile();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, false))) {
            try (Stream<String> stream = Files.lines(Paths.get(inputFilePath.toString()))) {
                Iterator<String> lines = stream.iterator();
                while(lines.hasNext()) {
                    totalCount++;
                    // example line format AUD/JPY,20160601 00:00:00.046,80.044998,80.063004
                    String[] items =  lines.next().split(",");
                    if (tracker.shouldProcure(items[1])) {
                        procuredCount++;
                        // System.out.printf("%s - Pair:%s, bid:%s ask:%s", items[1], items[0], items[2], items[3]).println();
                        String lineFormat = String.format("%s,%s", items[2], items[3]);
                        writer.write(lineFormat);
                        writer.newLine();
                    }
                    if(tracker.shouldFinish()) {
                        break;
                    }
                }
                double p = (double)procuredCount/(double)totalCount ;
                System.out.printf("Done. total: %s, reduced: %s. percentage reduction %s ", totalCount, procuredCount, p).println();
            }
        }catch (FileNotFoundException ex) {
            System.out.println(ex.toString());
        }
    }

    private File getOutputFile(Path inputFilePath) {
        String fileNameWithOutExt = com.google.common.io.Files.getNameWithoutExtension(inputFilePath.toString());
        fileNameWithOutExt = fileNameWithOutExt.substring(0, 6); // pull the ccy from the file name
        Path outputPath = Paths.get(inputFilePath.getParent().toString(), "output", String.format("%s.processed.csv", fileNameWithOutExt));
        return outputPath.toFile();
    }

    class PricerTimeTracker {
        LocalDateTime start;
        LocalDateTime end;
        LocalDateTime last;
        boolean shouldProcure(String time) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
            LocalDateTime next = LocalDateTime.parse(time, formatter);
            if(start == null) {
                start = next;
                end = start.plus(8, ChronoUnit.HOURS);
            }
            boolean shouldProcure = last == null || last.plus(100, ChronoUnit.MILLIS).isBefore(next);
            if (shouldProcure) {
                last = next;
            }
            return shouldProcure;
        }
        boolean shouldFinish() {
            return last.isAfter(end);
        }
    }
}
