// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

/**
 * Protobuf type {@code com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto}
 */
public  final class GetStartingPricesRequestDto extends
    com.google.protobuf.GeneratedMessage implements
    // @@protoc_insertion_point(message_implements:com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto)
    GetStartingPricesRequestDtoOrBuilder {
  // Use GetStartingPricesRequestDto.newBuilder() to construct.
  private GetStartingPricesRequestDto(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
    super(builder);
  }
  private GetStartingPricesRequestDto() {
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private GetStartingPricesRequestDto(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            com.accelfin.server.messaging.dtos.TimestampDto.Builder subBuilder = null;
            if (start_ != null) {
              subBuilder = start_.toBuilder();
            }
            start_ = input.readMessage(com.accelfin.server.messaging.dtos.TimestampDto.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(start_);
              start_ = subBuilder.buildPartial();
            }

            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetStartingPricesRequestDto_descriptor;
  }

  protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetStartingPricesRequestDto_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto.class, com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto.Builder.class);
  }

  public static final int START_FIELD_NUMBER = 1;
  private com.accelfin.server.messaging.dtos.TimestampDto start_;
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
   */
  public boolean hasStart() {
    return start_ != null;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
   */
  public com.accelfin.server.messaging.dtos.TimestampDto getStart() {
    return start_ == null ? com.accelfin.server.messaging.dtos.TimestampDto.getDefaultInstance() : start_;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
   */
  public com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder getStartOrBuilder() {
    return getStart();
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (start_ != null) {
      output.writeMessage(1, getStart());
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (start_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getStart());
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessage.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessage.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto)
      com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDtoOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetStartingPricesRequestDto_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetStartingPricesRequestDto_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto.class, com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto.Builder.class);
    }

    // Construct using com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      if (startBuilder_ == null) {
        start_ = null;
      } else {
        start_ = null;
        startBuilder_ = null;
      }
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetStartingPricesRequestDto_descriptor;
    }

    public com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto getDefaultInstanceForType() {
      return com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto.getDefaultInstance();
    }

    public com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto build() {
      com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto buildPartial() {
      com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto result = new com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto(this);
      if (startBuilder_ == null) {
        result.start_ = start_;
      } else {
        result.start_ = startBuilder_.build();
      }
      onBuilt();
      return result;
    }

    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto) {
        return mergeFrom((com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto other) {
      if (other == com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto.getDefaultInstance()) return this;
      if (other.hasStart()) {
        mergeStart(other.getStart());
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private com.accelfin.server.messaging.dtos.TimestampDto start_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.TimestampDto, com.accelfin.server.messaging.dtos.TimestampDto.Builder, com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder> startBuilder_;
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
     */
    public boolean hasStart() {
      return startBuilder_ != null || start_ != null;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
     */
    public com.accelfin.server.messaging.dtos.TimestampDto getStart() {
      if (startBuilder_ == null) {
        return start_ == null ? com.accelfin.server.messaging.dtos.TimestampDto.getDefaultInstance() : start_;
      } else {
        return startBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
     */
    public Builder setStart(com.accelfin.server.messaging.dtos.TimestampDto value) {
      if (startBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        start_ = value;
        onChanged();
      } else {
        startBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
     */
    public Builder setStart(
        com.accelfin.server.messaging.dtos.TimestampDto.Builder builderForValue) {
      if (startBuilder_ == null) {
        start_ = builderForValue.build();
        onChanged();
      } else {
        startBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
     */
    public Builder mergeStart(com.accelfin.server.messaging.dtos.TimestampDto value) {
      if (startBuilder_ == null) {
        if (start_ != null) {
          start_ =
            com.accelfin.server.messaging.dtos.TimestampDto.newBuilder(start_).mergeFrom(value).buildPartial();
        } else {
          start_ = value;
        }
        onChanged();
      } else {
        startBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
     */
    public Builder clearStart() {
      if (startBuilder_ == null) {
        start_ = null;
        onChanged();
      } else {
        start_ = null;
        startBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
     */
    public com.accelfin.server.messaging.dtos.TimestampDto.Builder getStartBuilder() {
      
      onChanged();
      return getStartFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
     */
    public com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder getStartOrBuilder() {
      if (startBuilder_ != null) {
        return startBuilder_.getMessageOrBuilder();
      } else {
        return start_ == null ?
            com.accelfin.server.messaging.dtos.TimestampDto.getDefaultInstance() : start_;
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.TimestampDto, com.accelfin.server.messaging.dtos.TimestampDto.Builder, com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder> 
        getStartFieldBuilder() {
      if (startBuilder_ == null) {
        startBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            com.accelfin.server.messaging.dtos.TimestampDto, com.accelfin.server.messaging.dtos.TimestampDto.Builder, com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder>(
                getStart(),
                getParentForChildren(),
                isClean());
        start_ = null;
      }
      return startBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto)
  }

  // @@protoc_insertion_point(class_scope:com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto)
  private static final com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto();
  }

  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<GetStartingPricesRequestDto>
      PARSER = new com.google.protobuf.AbstractParser<GetStartingPricesRequestDto>() {
    public GetStartingPricesRequestDto parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new GetStartingPricesRequestDto(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<GetStartingPricesRequestDto> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<GetStartingPricesRequestDto> getParserForType() {
    return PARSER;
  }

  public com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

