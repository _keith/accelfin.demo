package com.accelfin.server.demo;

import com.accelfin.server.messaging.OperationNames;

public class AnalyicsOperationNameConst extends OperationNames {
    public static final String ReportUpdate = "reportUpdate";
    public static final String ReportSubscriptionUpdate = "reportSubscriptionUpdate";
}
