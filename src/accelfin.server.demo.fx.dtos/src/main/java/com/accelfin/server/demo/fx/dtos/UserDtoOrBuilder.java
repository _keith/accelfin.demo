// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

public interface UserDtoOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.accelfin.server.demo.fx.dtos.UserDto)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>optional string user_name = 1;</code>
   */
  java.lang.String getUserName();
  /**
   * <code>optional string user_name = 1;</code>
   */
  com.google.protobuf.ByteString
      getUserNameBytes();

  /**
   * <code>optional string first_name = 2;</code>
   */
  java.lang.String getFirstName();
  /**
   * <code>optional string first_name = 2;</code>
   */
  com.google.protobuf.ByteString
      getFirstNameBytes();

  /**
   * <code>optional string last_name = 3;</code>
   */
  java.lang.String getLastName();
  /**
   * <code>optional string last_name = 3;</code>
   */
  com.google.protobuf.ByteString
      getLastNameBytes();

  /**
   * <code>repeated string permissions = 4;</code>
   */
  com.google.protobuf.ProtocolStringList
      getPermissionsList();
  /**
   * <code>repeated string permissions = 4;</code>
   */
  int getPermissionsCount();
  /**
   * <code>repeated string permissions = 4;</code>
   */
  java.lang.String getPermissions(int index);
  /**
   * <code>repeated string permissions = 4;</code>
   */
  com.google.protobuf.ByteString
      getPermissionsBytes(int index);
}
