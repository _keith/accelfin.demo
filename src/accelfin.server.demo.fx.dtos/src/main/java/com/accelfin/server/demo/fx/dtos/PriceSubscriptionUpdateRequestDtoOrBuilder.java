// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

public interface PriceSubscriptionUpdateRequestDtoOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.accelfin.server.demo.fx.dtos.PriceSubscriptionUpdateRequestDto)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>repeated string symbols = 1;</code>
   */
  com.google.protobuf.ProtocolStringList
      getSymbolsList();
  /**
   * <code>repeated string symbols = 1;</code>
   */
  int getSymbolsCount();
  /**
   * <code>repeated string symbols = 1;</code>
   */
  java.lang.String getSymbols(int index);
  /**
   * <code>repeated string symbols = 1;</code>
   */
  com.google.protobuf.ByteString
      getSymbolsBytes(int index);
}
