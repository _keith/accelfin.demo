// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

/**
 * Protobuf type {@code com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto}
 */
public  final class ExecuteSimpleOrderRequestDto extends
    com.google.protobuf.GeneratedMessage implements
    // @@protoc_insertion_point(message_implements:com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto)
    ExecuteSimpleOrderRequestDtoOrBuilder {
  // Use ExecuteSimpleOrderRequestDto.newBuilder() to construct.
  private ExecuteSimpleOrderRequestDto(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
    super(builder);
  }
  private ExecuteSimpleOrderRequestDto() {
    currencyPair_ = "";
    dealtCurrency_ = "";
    side_ = 0;
    type_ = 0;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private ExecuteSimpleOrderRequestDto(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            java.lang.String s = input.readStringRequireUtf8();

            currencyPair_ = s;
            break;
          }
          case 18: {
            java.lang.String s = input.readStringRequireUtf8();

            dealtCurrency_ = s;
            break;
          }
          case 24: {
            int rawValue = input.readEnum();

            side_ = rawValue;
            break;
          }
          case 32: {
            int rawValue = input.readEnum();

            type_ = rawValue;
            break;
          }
          case 42: {
            com.accelfin.server.messaging.dtos.DecimalDto.Builder subBuilder = null;
            if (notional_ != null) {
              subBuilder = notional_.toBuilder();
            }
            notional_ = input.readMessage(com.accelfin.server.messaging.dtos.DecimalDto.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(notional_);
              notional_ = subBuilder.buildPartial();
            }

            break;
          }
          case 50: {
            com.accelfin.server.messaging.dtos.DecimalDto.Builder subBuilder = null;
            if (rate_ != null) {
              subBuilder = rate_.toBuilder();
            }
            rate_ = input.readMessage(com.accelfin.server.messaging.dtos.DecimalDto.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(rate_);
              rate_ = subBuilder.buildPartial();
            }

            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_ExecuteSimpleOrderRequestDto_descriptor;
  }

  protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_ExecuteSimpleOrderRequestDto_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto.class, com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto.Builder.class);
  }

  public static final int CURRENCY_PAIR_FIELD_NUMBER = 1;
  private volatile java.lang.Object currencyPair_;
  /**
   * <code>optional string currency_pair = 1;</code>
   */
  public java.lang.String getCurrencyPair() {
    java.lang.Object ref = currencyPair_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      currencyPair_ = s;
      return s;
    }
  }
  /**
   * <code>optional string currency_pair = 1;</code>
   */
  public com.google.protobuf.ByteString
      getCurrencyPairBytes() {
    java.lang.Object ref = currencyPair_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      currencyPair_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int DEALT_CURRENCY_FIELD_NUMBER = 2;
  private volatile java.lang.Object dealtCurrency_;
  /**
   * <code>optional string dealt_currency = 2;</code>
   */
  public java.lang.String getDealtCurrency() {
    java.lang.Object ref = dealtCurrency_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      dealtCurrency_ = s;
      return s;
    }
  }
  /**
   * <code>optional string dealt_currency = 2;</code>
   */
  public com.google.protobuf.ByteString
      getDealtCurrencyBytes() {
    java.lang.Object ref = dealtCurrency_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      dealtCurrency_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int SIDE_FIELD_NUMBER = 3;
  private int side_;
  /**
   * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
   */
  public int getSideValue() {
    return side_;
  }
  /**
   * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
   */
  public com.accelfin.server.demo.fx.dtos.SideDto getSide() {
    com.accelfin.server.demo.fx.dtos.SideDto result = com.accelfin.server.demo.fx.dtos.SideDto.forNumber(side_);
    return result == null ? com.accelfin.server.demo.fx.dtos.SideDto.UNRECOGNIZED : result;
  }

  public static final int TYPE_FIELD_NUMBER = 4;
  private int type_;
  /**
   * <code>optional .com.accelfin.server.demo.fx.dtos.OrderTypeDto type = 4;</code>
   */
  public int getTypeValue() {
    return type_;
  }
  /**
   * <code>optional .com.accelfin.server.demo.fx.dtos.OrderTypeDto type = 4;</code>
   */
  public com.accelfin.server.demo.fx.dtos.OrderTypeDto getType() {
    com.accelfin.server.demo.fx.dtos.OrderTypeDto result = com.accelfin.server.demo.fx.dtos.OrderTypeDto.forNumber(type_);
    return result == null ? com.accelfin.server.demo.fx.dtos.OrderTypeDto.UNRECOGNIZED : result;
  }

  public static final int NOTIONAL_FIELD_NUMBER = 5;
  private com.accelfin.server.messaging.dtos.DecimalDto notional_;
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
   */
  public boolean hasNotional() {
    return notional_ != null;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
   */
  public com.accelfin.server.messaging.dtos.DecimalDto getNotional() {
    return notional_ == null ? com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : notional_;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
   */
  public com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getNotionalOrBuilder() {
    return getNotional();
  }

  public static final int RATE_FIELD_NUMBER = 6;
  private com.accelfin.server.messaging.dtos.DecimalDto rate_;
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
   */
  public boolean hasRate() {
    return rate_ != null;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
   */
  public com.accelfin.server.messaging.dtos.DecimalDto getRate() {
    return rate_ == null ? com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : rate_;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
   */
  public com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getRateOrBuilder() {
    return getRate();
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (!getCurrencyPairBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessage.writeString(output, 1, currencyPair_);
    }
    if (!getDealtCurrencyBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessage.writeString(output, 2, dealtCurrency_);
    }
    if (side_ != com.accelfin.server.demo.fx.dtos.SideDto.BUY.getNumber()) {
      output.writeEnum(3, side_);
    }
    if (type_ != com.accelfin.server.demo.fx.dtos.OrderTypeDto.TAKE_PROFIT.getNumber()) {
      output.writeEnum(4, type_);
    }
    if (notional_ != null) {
      output.writeMessage(5, getNotional());
    }
    if (rate_ != null) {
      output.writeMessage(6, getRate());
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (!getCurrencyPairBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessage.computeStringSize(1, currencyPair_);
    }
    if (!getDealtCurrencyBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessage.computeStringSize(2, dealtCurrency_);
    }
    if (side_ != com.accelfin.server.demo.fx.dtos.SideDto.BUY.getNumber()) {
      size += com.google.protobuf.CodedOutputStream
        .computeEnumSize(3, side_);
    }
    if (type_ != com.accelfin.server.demo.fx.dtos.OrderTypeDto.TAKE_PROFIT.getNumber()) {
      size += com.google.protobuf.CodedOutputStream
        .computeEnumSize(4, type_);
    }
    if (notional_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(5, getNotional());
    }
    if (rate_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(6, getRate());
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  public static com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessage.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessage.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto)
      com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDtoOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_ExecuteSimpleOrderRequestDto_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_ExecuteSimpleOrderRequestDto_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto.class, com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto.Builder.class);
    }

    // Construct using com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      currencyPair_ = "";

      dealtCurrency_ = "";

      side_ = 0;

      type_ = 0;

      if (notionalBuilder_ == null) {
        notional_ = null;
      } else {
        notional_ = null;
        notionalBuilder_ = null;
      }
      if (rateBuilder_ == null) {
        rate_ = null;
      } else {
        rate_ = null;
        rateBuilder_ = null;
      }
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_ExecuteSimpleOrderRequestDto_descriptor;
    }

    public com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto getDefaultInstanceForType() {
      return com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto.getDefaultInstance();
    }

    public com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto build() {
      com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto buildPartial() {
      com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto result = new com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto(this);
      result.currencyPair_ = currencyPair_;
      result.dealtCurrency_ = dealtCurrency_;
      result.side_ = side_;
      result.type_ = type_;
      if (notionalBuilder_ == null) {
        result.notional_ = notional_;
      } else {
        result.notional_ = notionalBuilder_.build();
      }
      if (rateBuilder_ == null) {
        result.rate_ = rate_;
      } else {
        result.rate_ = rateBuilder_.build();
      }
      onBuilt();
      return result;
    }

    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto) {
        return mergeFrom((com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto other) {
      if (other == com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto.getDefaultInstance()) return this;
      if (!other.getCurrencyPair().isEmpty()) {
        currencyPair_ = other.currencyPair_;
        onChanged();
      }
      if (!other.getDealtCurrency().isEmpty()) {
        dealtCurrency_ = other.dealtCurrency_;
        onChanged();
      }
      if (other.side_ != 0) {
        setSideValue(other.getSideValue());
      }
      if (other.type_ != 0) {
        setTypeValue(other.getTypeValue());
      }
      if (other.hasNotional()) {
        mergeNotional(other.getNotional());
      }
      if (other.hasRate()) {
        mergeRate(other.getRate());
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private java.lang.Object currencyPair_ = "";
    /**
     * <code>optional string currency_pair = 1;</code>
     */
    public java.lang.String getCurrencyPair() {
      java.lang.Object ref = currencyPair_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        currencyPair_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string currency_pair = 1;</code>
     */
    public com.google.protobuf.ByteString
        getCurrencyPairBytes() {
      java.lang.Object ref = currencyPair_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        currencyPair_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string currency_pair = 1;</code>
     */
    public Builder setCurrencyPair(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      currencyPair_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string currency_pair = 1;</code>
     */
    public Builder clearCurrencyPair() {
      
      currencyPair_ = getDefaultInstance().getCurrencyPair();
      onChanged();
      return this;
    }
    /**
     * <code>optional string currency_pair = 1;</code>
     */
    public Builder setCurrencyPairBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      currencyPair_ = value;
      onChanged();
      return this;
    }

    private java.lang.Object dealtCurrency_ = "";
    /**
     * <code>optional string dealt_currency = 2;</code>
     */
    public java.lang.String getDealtCurrency() {
      java.lang.Object ref = dealtCurrency_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        dealtCurrency_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string dealt_currency = 2;</code>
     */
    public com.google.protobuf.ByteString
        getDealtCurrencyBytes() {
      java.lang.Object ref = dealtCurrency_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        dealtCurrency_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string dealt_currency = 2;</code>
     */
    public Builder setDealtCurrency(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      dealtCurrency_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string dealt_currency = 2;</code>
     */
    public Builder clearDealtCurrency() {
      
      dealtCurrency_ = getDefaultInstance().getDealtCurrency();
      onChanged();
      return this;
    }
    /**
     * <code>optional string dealt_currency = 2;</code>
     */
    public Builder setDealtCurrencyBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      dealtCurrency_ = value;
      onChanged();
      return this;
    }

    private int side_ = 0;
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
     */
    public int getSideValue() {
      return side_;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
     */
    public Builder setSideValue(int value) {
      side_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
     */
    public com.accelfin.server.demo.fx.dtos.SideDto getSide() {
      com.accelfin.server.demo.fx.dtos.SideDto result = com.accelfin.server.demo.fx.dtos.SideDto.forNumber(side_);
      return result == null ? com.accelfin.server.demo.fx.dtos.SideDto.UNRECOGNIZED : result;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
     */
    public Builder setSide(com.accelfin.server.demo.fx.dtos.SideDto value) {
      if (value == null) {
        throw new NullPointerException();
      }
      
      side_ = value.getNumber();
      onChanged();
      return this;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
     */
    public Builder clearSide() {
      
      side_ = 0;
      onChanged();
      return this;
    }

    private int type_ = 0;
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.OrderTypeDto type = 4;</code>
     */
    public int getTypeValue() {
      return type_;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.OrderTypeDto type = 4;</code>
     */
    public Builder setTypeValue(int value) {
      type_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.OrderTypeDto type = 4;</code>
     */
    public com.accelfin.server.demo.fx.dtos.OrderTypeDto getType() {
      com.accelfin.server.demo.fx.dtos.OrderTypeDto result = com.accelfin.server.demo.fx.dtos.OrderTypeDto.forNumber(type_);
      return result == null ? com.accelfin.server.demo.fx.dtos.OrderTypeDto.UNRECOGNIZED : result;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.OrderTypeDto type = 4;</code>
     */
    public Builder setType(com.accelfin.server.demo.fx.dtos.OrderTypeDto value) {
      if (value == null) {
        throw new NullPointerException();
      }
      
      type_ = value.getNumber();
      onChanged();
      return this;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.OrderTypeDto type = 4;</code>
     */
    public Builder clearType() {
      
      type_ = 0;
      onChanged();
      return this;
    }

    private com.accelfin.server.messaging.dtos.DecimalDto notional_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder> notionalBuilder_;
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
     */
    public boolean hasNotional() {
      return notionalBuilder_ != null || notional_ != null;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDto getNotional() {
      if (notionalBuilder_ == null) {
        return notional_ == null ? com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : notional_;
      } else {
        return notionalBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
     */
    public Builder setNotional(com.accelfin.server.messaging.dtos.DecimalDto value) {
      if (notionalBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        notional_ = value;
        onChanged();
      } else {
        notionalBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
     */
    public Builder setNotional(
        com.accelfin.server.messaging.dtos.DecimalDto.Builder builderForValue) {
      if (notionalBuilder_ == null) {
        notional_ = builderForValue.build();
        onChanged();
      } else {
        notionalBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
     */
    public Builder mergeNotional(com.accelfin.server.messaging.dtos.DecimalDto value) {
      if (notionalBuilder_ == null) {
        if (notional_ != null) {
          notional_ =
            com.accelfin.server.messaging.dtos.DecimalDto.newBuilder(notional_).mergeFrom(value).buildPartial();
        } else {
          notional_ = value;
        }
        onChanged();
      } else {
        notionalBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
     */
    public Builder clearNotional() {
      if (notionalBuilder_ == null) {
        notional_ = null;
        onChanged();
      } else {
        notional_ = null;
        notionalBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDto.Builder getNotionalBuilder() {
      
      onChanged();
      return getNotionalFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getNotionalOrBuilder() {
      if (notionalBuilder_ != null) {
        return notionalBuilder_.getMessageOrBuilder();
      } else {
        return notional_ == null ?
            com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : notional_;
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto notional = 5;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder> 
        getNotionalFieldBuilder() {
      if (notionalBuilder_ == null) {
        notionalBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder>(
                getNotional(),
                getParentForChildren(),
                isClean());
        notional_ = null;
      }
      return notionalBuilder_;
    }

    private com.accelfin.server.messaging.dtos.DecimalDto rate_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder> rateBuilder_;
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
     */
    public boolean hasRate() {
      return rateBuilder_ != null || rate_ != null;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDto getRate() {
      if (rateBuilder_ == null) {
        return rate_ == null ? com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : rate_;
      } else {
        return rateBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
     */
    public Builder setRate(com.accelfin.server.messaging.dtos.DecimalDto value) {
      if (rateBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        rate_ = value;
        onChanged();
      } else {
        rateBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
     */
    public Builder setRate(
        com.accelfin.server.messaging.dtos.DecimalDto.Builder builderForValue) {
      if (rateBuilder_ == null) {
        rate_ = builderForValue.build();
        onChanged();
      } else {
        rateBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
     */
    public Builder mergeRate(com.accelfin.server.messaging.dtos.DecimalDto value) {
      if (rateBuilder_ == null) {
        if (rate_ != null) {
          rate_ =
            com.accelfin.server.messaging.dtos.DecimalDto.newBuilder(rate_).mergeFrom(value).buildPartial();
        } else {
          rate_ = value;
        }
        onChanged();
      } else {
        rateBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
     */
    public Builder clearRate() {
      if (rateBuilder_ == null) {
        rate_ = null;
        onChanged();
      } else {
        rate_ = null;
        rateBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDto.Builder getRateBuilder() {
      
      onChanged();
      return getRateFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getRateOrBuilder() {
      if (rateBuilder_ != null) {
        return rateBuilder_.getMessageOrBuilder();
      } else {
        return rate_ == null ?
            com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : rate_;
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto rate = 6;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder> 
        getRateFieldBuilder() {
      if (rateBuilder_ == null) {
        rateBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder>(
                getRate(),
                getParentForChildren(),
                isClean());
        rate_ = null;
      }
      return rateBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto)
  }

  // @@protoc_insertion_point(class_scope:com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto)
  private static final com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto();
  }

  public static com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<ExecuteSimpleOrderRequestDto>
      PARSER = new com.google.protobuf.AbstractParser<ExecuteSimpleOrderRequestDto>() {
    public ExecuteSimpleOrderRequestDto parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new ExecuteSimpleOrderRequestDto(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<ExecuteSimpleOrderRequestDto> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<ExecuteSimpleOrderRequestDto> getParserForType() {
    return PARSER;
  }

  public com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

