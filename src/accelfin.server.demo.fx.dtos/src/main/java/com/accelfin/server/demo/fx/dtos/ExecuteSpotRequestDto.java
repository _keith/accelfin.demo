// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

/**
 * Protobuf type {@code com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto}
 */
public  final class ExecuteSpotRequestDto extends
    com.google.protobuf.GeneratedMessage implements
    // @@protoc_insertion_point(message_implements:com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto)
    ExecuteSpotRequestDtoOrBuilder {
  // Use ExecuteSpotRequestDto.newBuilder() to construct.
  private ExecuteSpotRequestDto(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
    super(builder);
  }
  private ExecuteSpotRequestDto() {
    priceId_ = "";
    symbol_ = "";
    side_ = 0;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private ExecuteSpotRequestDto(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            java.lang.String s = input.readStringRequireUtf8();

            priceId_ = s;
            break;
          }
          case 18: {
            java.lang.String s = input.readStringRequireUtf8();

            symbol_ = s;
            break;
          }
          case 24: {
            int rawValue = input.readEnum();

            side_ = rawValue;
            break;
          }
          case 34: {
            com.accelfin.server.messaging.dtos.DecimalDto.Builder subBuilder = null;
            if (amount_ != null) {
              subBuilder = amount_.toBuilder();
            }
            amount_ = input.readMessage(com.accelfin.server.messaging.dtos.DecimalDto.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(amount_);
              amount_ = subBuilder.buildPartial();
            }

            break;
          }
          case 42: {
            com.accelfin.server.messaging.dtos.DecimalDto.Builder subBuilder = null;
            if (price_ != null) {
              subBuilder = price_.toBuilder();
            }
            price_ = input.readMessage(com.accelfin.server.messaging.dtos.DecimalDto.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(price_);
              price_ = subBuilder.buildPartial();
            }

            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_ExecuteSpotRequestDto_descriptor;
  }

  protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_ExecuteSpotRequestDto_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto.class, com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto.Builder.class);
  }

  public static final int PRICE_ID_FIELD_NUMBER = 1;
  private volatile java.lang.Object priceId_;
  /**
   * <code>optional string price_id = 1;</code>
   */
  public java.lang.String getPriceId() {
    java.lang.Object ref = priceId_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      priceId_ = s;
      return s;
    }
  }
  /**
   * <code>optional string price_id = 1;</code>
   */
  public com.google.protobuf.ByteString
      getPriceIdBytes() {
    java.lang.Object ref = priceId_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      priceId_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int SYMBOL_FIELD_NUMBER = 2;
  private volatile java.lang.Object symbol_;
  /**
   * <code>optional string symbol = 2;</code>
   */
  public java.lang.String getSymbol() {
    java.lang.Object ref = symbol_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      symbol_ = s;
      return s;
    }
  }
  /**
   * <code>optional string symbol = 2;</code>
   */
  public com.google.protobuf.ByteString
      getSymbolBytes() {
    java.lang.Object ref = symbol_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      symbol_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int SIDE_FIELD_NUMBER = 3;
  private int side_;
  /**
   * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
   */
  public int getSideValue() {
    return side_;
  }
  /**
   * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
   */
  public com.accelfin.server.demo.fx.dtos.SideDto getSide() {
    com.accelfin.server.demo.fx.dtos.SideDto result = com.accelfin.server.demo.fx.dtos.SideDto.forNumber(side_);
    return result == null ? com.accelfin.server.demo.fx.dtos.SideDto.UNRECOGNIZED : result;
  }

  public static final int AMOUNT_FIELD_NUMBER = 4;
  private com.accelfin.server.messaging.dtos.DecimalDto amount_;
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
   */
  public boolean hasAmount() {
    return amount_ != null;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
   */
  public com.accelfin.server.messaging.dtos.DecimalDto getAmount() {
    return amount_ == null ? com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : amount_;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
   */
  public com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getAmountOrBuilder() {
    return getAmount();
  }

  public static final int PRICE_FIELD_NUMBER = 5;
  private com.accelfin.server.messaging.dtos.DecimalDto price_;
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
   */
  public boolean hasPrice() {
    return price_ != null;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
   */
  public com.accelfin.server.messaging.dtos.DecimalDto getPrice() {
    return price_ == null ? com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : price_;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
   */
  public com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getPriceOrBuilder() {
    return getPrice();
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (!getPriceIdBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessage.writeString(output, 1, priceId_);
    }
    if (!getSymbolBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessage.writeString(output, 2, symbol_);
    }
    if (side_ != com.accelfin.server.demo.fx.dtos.SideDto.BUY.getNumber()) {
      output.writeEnum(3, side_);
    }
    if (amount_ != null) {
      output.writeMessage(4, getAmount());
    }
    if (price_ != null) {
      output.writeMessage(5, getPrice());
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (!getPriceIdBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessage.computeStringSize(1, priceId_);
    }
    if (!getSymbolBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessage.computeStringSize(2, symbol_);
    }
    if (side_ != com.accelfin.server.demo.fx.dtos.SideDto.BUY.getNumber()) {
      size += com.google.protobuf.CodedOutputStream
        .computeEnumSize(3, side_);
    }
    if (amount_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(4, getAmount());
    }
    if (price_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(5, getPrice());
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  public static com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessage.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessage.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto)
      com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDtoOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_ExecuteSpotRequestDto_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_ExecuteSpotRequestDto_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto.class, com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto.Builder.class);
    }

    // Construct using com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      priceId_ = "";

      symbol_ = "";

      side_ = 0;

      if (amountBuilder_ == null) {
        amount_ = null;
      } else {
        amount_ = null;
        amountBuilder_ = null;
      }
      if (priceBuilder_ == null) {
        price_ = null;
      } else {
        price_ = null;
        priceBuilder_ = null;
      }
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_ExecuteSpotRequestDto_descriptor;
    }

    public com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto getDefaultInstanceForType() {
      return com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto.getDefaultInstance();
    }

    public com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto build() {
      com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto buildPartial() {
      com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto result = new com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto(this);
      result.priceId_ = priceId_;
      result.symbol_ = symbol_;
      result.side_ = side_;
      if (amountBuilder_ == null) {
        result.amount_ = amount_;
      } else {
        result.amount_ = amountBuilder_.build();
      }
      if (priceBuilder_ == null) {
        result.price_ = price_;
      } else {
        result.price_ = priceBuilder_.build();
      }
      onBuilt();
      return result;
    }

    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto) {
        return mergeFrom((com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto other) {
      if (other == com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto.getDefaultInstance()) return this;
      if (!other.getPriceId().isEmpty()) {
        priceId_ = other.priceId_;
        onChanged();
      }
      if (!other.getSymbol().isEmpty()) {
        symbol_ = other.symbol_;
        onChanged();
      }
      if (other.side_ != 0) {
        setSideValue(other.getSideValue());
      }
      if (other.hasAmount()) {
        mergeAmount(other.getAmount());
      }
      if (other.hasPrice()) {
        mergePrice(other.getPrice());
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private java.lang.Object priceId_ = "";
    /**
     * <code>optional string price_id = 1;</code>
     */
    public java.lang.String getPriceId() {
      java.lang.Object ref = priceId_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        priceId_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string price_id = 1;</code>
     */
    public com.google.protobuf.ByteString
        getPriceIdBytes() {
      java.lang.Object ref = priceId_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        priceId_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string price_id = 1;</code>
     */
    public Builder setPriceId(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      priceId_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string price_id = 1;</code>
     */
    public Builder clearPriceId() {
      
      priceId_ = getDefaultInstance().getPriceId();
      onChanged();
      return this;
    }
    /**
     * <code>optional string price_id = 1;</code>
     */
    public Builder setPriceIdBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      priceId_ = value;
      onChanged();
      return this;
    }

    private java.lang.Object symbol_ = "";
    /**
     * <code>optional string symbol = 2;</code>
     */
    public java.lang.String getSymbol() {
      java.lang.Object ref = symbol_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        symbol_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>optional string symbol = 2;</code>
     */
    public com.google.protobuf.ByteString
        getSymbolBytes() {
      java.lang.Object ref = symbol_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        symbol_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>optional string symbol = 2;</code>
     */
    public Builder setSymbol(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      symbol_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional string symbol = 2;</code>
     */
    public Builder clearSymbol() {
      
      symbol_ = getDefaultInstance().getSymbol();
      onChanged();
      return this;
    }
    /**
     * <code>optional string symbol = 2;</code>
     */
    public Builder setSymbolBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      symbol_ = value;
      onChanged();
      return this;
    }

    private int side_ = 0;
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
     */
    public int getSideValue() {
      return side_;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
     */
    public Builder setSideValue(int value) {
      side_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
     */
    public com.accelfin.server.demo.fx.dtos.SideDto getSide() {
      com.accelfin.server.demo.fx.dtos.SideDto result = com.accelfin.server.demo.fx.dtos.SideDto.forNumber(side_);
      return result == null ? com.accelfin.server.demo.fx.dtos.SideDto.UNRECOGNIZED : result;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
     */
    public Builder setSide(com.accelfin.server.demo.fx.dtos.SideDto value) {
      if (value == null) {
        throw new NullPointerException();
      }
      
      side_ = value.getNumber();
      onChanged();
      return this;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.SideDto side = 3;</code>
     */
    public Builder clearSide() {
      
      side_ = 0;
      onChanged();
      return this;
    }

    private com.accelfin.server.messaging.dtos.DecimalDto amount_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder> amountBuilder_;
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
     */
    public boolean hasAmount() {
      return amountBuilder_ != null || amount_ != null;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDto getAmount() {
      if (amountBuilder_ == null) {
        return amount_ == null ? com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : amount_;
      } else {
        return amountBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
     */
    public Builder setAmount(com.accelfin.server.messaging.dtos.DecimalDto value) {
      if (amountBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        amount_ = value;
        onChanged();
      } else {
        amountBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
     */
    public Builder setAmount(
        com.accelfin.server.messaging.dtos.DecimalDto.Builder builderForValue) {
      if (amountBuilder_ == null) {
        amount_ = builderForValue.build();
        onChanged();
      } else {
        amountBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
     */
    public Builder mergeAmount(com.accelfin.server.messaging.dtos.DecimalDto value) {
      if (amountBuilder_ == null) {
        if (amount_ != null) {
          amount_ =
            com.accelfin.server.messaging.dtos.DecimalDto.newBuilder(amount_).mergeFrom(value).buildPartial();
        } else {
          amount_ = value;
        }
        onChanged();
      } else {
        amountBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
     */
    public Builder clearAmount() {
      if (amountBuilder_ == null) {
        amount_ = null;
        onChanged();
      } else {
        amount_ = null;
        amountBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDto.Builder getAmountBuilder() {
      
      onChanged();
      return getAmountFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getAmountOrBuilder() {
      if (amountBuilder_ != null) {
        return amountBuilder_.getMessageOrBuilder();
      } else {
        return amount_ == null ?
            com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : amount_;
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto amount = 4;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder> 
        getAmountFieldBuilder() {
      if (amountBuilder_ == null) {
        amountBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder>(
                getAmount(),
                getParentForChildren(),
                isClean());
        amount_ = null;
      }
      return amountBuilder_;
    }

    private com.accelfin.server.messaging.dtos.DecimalDto price_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder> priceBuilder_;
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
     */
    public boolean hasPrice() {
      return priceBuilder_ != null || price_ != null;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDto getPrice() {
      if (priceBuilder_ == null) {
        return price_ == null ? com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : price_;
      } else {
        return priceBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
     */
    public Builder setPrice(com.accelfin.server.messaging.dtos.DecimalDto value) {
      if (priceBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        price_ = value;
        onChanged();
      } else {
        priceBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
     */
    public Builder setPrice(
        com.accelfin.server.messaging.dtos.DecimalDto.Builder builderForValue) {
      if (priceBuilder_ == null) {
        price_ = builderForValue.build();
        onChanged();
      } else {
        priceBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
     */
    public Builder mergePrice(com.accelfin.server.messaging.dtos.DecimalDto value) {
      if (priceBuilder_ == null) {
        if (price_ != null) {
          price_ =
            com.accelfin.server.messaging.dtos.DecimalDto.newBuilder(price_).mergeFrom(value).buildPartial();
        } else {
          price_ = value;
        }
        onChanged();
      } else {
        priceBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
     */
    public Builder clearPrice() {
      if (priceBuilder_ == null) {
        price_ = null;
        onChanged();
      } else {
        price_ = null;
        priceBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDto.Builder getPriceBuilder() {
      
      onChanged();
      return getPriceFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
     */
    public com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getPriceOrBuilder() {
      if (priceBuilder_ != null) {
        return priceBuilder_.getMessageOrBuilder();
      } else {
        return price_ == null ?
            com.accelfin.server.messaging.dtos.DecimalDto.getDefaultInstance() : price_;
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto price = 5;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder> 
        getPriceFieldBuilder() {
      if (priceBuilder_ == null) {
        priceBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            com.accelfin.server.messaging.dtos.DecimalDto, com.accelfin.server.messaging.dtos.DecimalDto.Builder, com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder>(
                getPrice(),
                getParentForChildren(),
                isClean());
        price_ = null;
      }
      return priceBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto)
  }

  // @@protoc_insertion_point(class_scope:com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto)
  private static final com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto();
  }

  public static com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<ExecuteSpotRequestDto>
      PARSER = new com.google.protobuf.AbstractParser<ExecuteSpotRequestDto>() {
    public ExecuteSpotRequestDto parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new ExecuteSpotRequestDto(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<ExecuteSpotRequestDto> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<ExecuteSpotRequestDto> getParserForType() {
    return PARSER;
  }

  public com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

