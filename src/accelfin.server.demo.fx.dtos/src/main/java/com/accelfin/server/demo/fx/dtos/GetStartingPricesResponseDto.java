// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

/**
 * Protobuf type {@code com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto}
 */
public  final class GetStartingPricesResponseDto extends
    com.google.protobuf.GeneratedMessage implements
    // @@protoc_insertion_point(message_implements:com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto)
    GetStartingPricesResponseDtoOrBuilder {
  // Use GetStartingPricesResponseDto.newBuilder() to construct.
  private GetStartingPricesResponseDto(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
    super(builder);
  }
  private GetStartingPricesResponseDto() {
    startPrices_ = java.util.Collections.emptyList();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private GetStartingPricesResponseDto(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            if (!((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
              startPrices_ = new java.util.ArrayList<com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto>();
              mutable_bitField0_ |= 0x00000001;
            }
            startPrices_.add(input.readMessage(com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.parser(), extensionRegistry));
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      if (((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
        startPrices_ = java.util.Collections.unmodifiableList(startPrices_);
      }
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetStartingPricesResponseDto_descriptor;
  }

  protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetStartingPricesResponseDto_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto.class, com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto.Builder.class);
  }

  public static final int START_PRICES_FIELD_NUMBER = 1;
  private java.util.List<com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto> startPrices_;
  /**
   * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
   */
  public java.util.List<com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto> getStartPricesList() {
    return startPrices_;
  }
  /**
   * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
   */
  public java.util.List<? extends com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDtoOrBuilder> 
      getStartPricesOrBuilderList() {
    return startPrices_;
  }
  /**
   * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
   */
  public int getStartPricesCount() {
    return startPrices_.size();
  }
  /**
   * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
   */
  public com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto getStartPrices(int index) {
    return startPrices_.get(index);
  }
  /**
   * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
   */
  public com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDtoOrBuilder getStartPricesOrBuilder(
      int index) {
    return startPrices_.get(index);
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    for (int i = 0; i < startPrices_.size(); i++) {
      output.writeMessage(1, startPrices_.get(i));
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    for (int i = 0; i < startPrices_.size(); i++) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, startPrices_.get(i));
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessage.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessage.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto)
      com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDtoOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetStartingPricesResponseDto_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetStartingPricesResponseDto_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto.class, com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto.Builder.class);
    }

    // Construct using com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        getStartPricesFieldBuilder();
      }
    }
    public Builder clear() {
      super.clear();
      if (startPricesBuilder_ == null) {
        startPrices_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
      } else {
        startPricesBuilder_.clear();
      }
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetStartingPricesResponseDto_descriptor;
    }

    public com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto getDefaultInstanceForType() {
      return com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto.getDefaultInstance();
    }

    public com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto build() {
      com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto buildPartial() {
      com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto result = new com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto(this);
      int from_bitField0_ = bitField0_;
      if (startPricesBuilder_ == null) {
        if (((bitField0_ & 0x00000001) == 0x00000001)) {
          startPrices_ = java.util.Collections.unmodifiableList(startPrices_);
          bitField0_ = (bitField0_ & ~0x00000001);
        }
        result.startPrices_ = startPrices_;
      } else {
        result.startPrices_ = startPricesBuilder_.build();
      }
      onBuilt();
      return result;
    }

    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto) {
        return mergeFrom((com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto other) {
      if (other == com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto.getDefaultInstance()) return this;
      if (startPricesBuilder_ == null) {
        if (!other.startPrices_.isEmpty()) {
          if (startPrices_.isEmpty()) {
            startPrices_ = other.startPrices_;
            bitField0_ = (bitField0_ & ~0x00000001);
          } else {
            ensureStartPricesIsMutable();
            startPrices_.addAll(other.startPrices_);
          }
          onChanged();
        }
      } else {
        if (!other.startPrices_.isEmpty()) {
          if (startPricesBuilder_.isEmpty()) {
            startPricesBuilder_.dispose();
            startPricesBuilder_ = null;
            startPrices_ = other.startPrices_;
            bitField0_ = (bitField0_ & ~0x00000001);
            startPricesBuilder_ = 
              com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders ?
                 getStartPricesFieldBuilder() : null;
          } else {
            startPricesBuilder_.addAllMessages(other.startPrices_);
          }
        }
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private java.util.List<com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto> startPrices_ =
      java.util.Collections.emptyList();
    private void ensureStartPricesIsMutable() {
      if (!((bitField0_ & 0x00000001) == 0x00000001)) {
        startPrices_ = new java.util.ArrayList<com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto>(startPrices_);
        bitField0_ |= 0x00000001;
       }
    }

    private com.google.protobuf.RepeatedFieldBuilder<
        com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto, com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.Builder, com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDtoOrBuilder> startPricesBuilder_;

    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public java.util.List<com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto> getStartPricesList() {
      if (startPricesBuilder_ == null) {
        return java.util.Collections.unmodifiableList(startPrices_);
      } else {
        return startPricesBuilder_.getMessageList();
      }
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public int getStartPricesCount() {
      if (startPricesBuilder_ == null) {
        return startPrices_.size();
      } else {
        return startPricesBuilder_.getCount();
      }
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto getStartPrices(int index) {
      if (startPricesBuilder_ == null) {
        return startPrices_.get(index);
      } else {
        return startPricesBuilder_.getMessage(index);
      }
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public Builder setStartPrices(
        int index, com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto value) {
      if (startPricesBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureStartPricesIsMutable();
        startPrices_.set(index, value);
        onChanged();
      } else {
        startPricesBuilder_.setMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public Builder setStartPrices(
        int index, com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.Builder builderForValue) {
      if (startPricesBuilder_ == null) {
        ensureStartPricesIsMutable();
        startPrices_.set(index, builderForValue.build());
        onChanged();
      } else {
        startPricesBuilder_.setMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public Builder addStartPrices(com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto value) {
      if (startPricesBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureStartPricesIsMutable();
        startPrices_.add(value);
        onChanged();
      } else {
        startPricesBuilder_.addMessage(value);
      }
      return this;
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public Builder addStartPrices(
        int index, com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto value) {
      if (startPricesBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        ensureStartPricesIsMutable();
        startPrices_.add(index, value);
        onChanged();
      } else {
        startPricesBuilder_.addMessage(index, value);
      }
      return this;
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public Builder addStartPrices(
        com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.Builder builderForValue) {
      if (startPricesBuilder_ == null) {
        ensureStartPricesIsMutable();
        startPrices_.add(builderForValue.build());
        onChanged();
      } else {
        startPricesBuilder_.addMessage(builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public Builder addStartPrices(
        int index, com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.Builder builderForValue) {
      if (startPricesBuilder_ == null) {
        ensureStartPricesIsMutable();
        startPrices_.add(index, builderForValue.build());
        onChanged();
      } else {
        startPricesBuilder_.addMessage(index, builderForValue.build());
      }
      return this;
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public Builder addAllStartPrices(
        java.lang.Iterable<? extends com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto> values) {
      if (startPricesBuilder_ == null) {
        ensureStartPricesIsMutable();
        com.google.protobuf.AbstractMessageLite.Builder.addAll(
            values, startPrices_);
        onChanged();
      } else {
        startPricesBuilder_.addAllMessages(values);
      }
      return this;
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public Builder clearStartPrices() {
      if (startPricesBuilder_ == null) {
        startPrices_ = java.util.Collections.emptyList();
        bitField0_ = (bitField0_ & ~0x00000001);
        onChanged();
      } else {
        startPricesBuilder_.clear();
      }
      return this;
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public Builder removeStartPrices(int index) {
      if (startPricesBuilder_ == null) {
        ensureStartPricesIsMutable();
        startPrices_.remove(index);
        onChanged();
      } else {
        startPricesBuilder_.remove(index);
      }
      return this;
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.Builder getStartPricesBuilder(
        int index) {
      return getStartPricesFieldBuilder().getBuilder(index);
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDtoOrBuilder getStartPricesOrBuilder(
        int index) {
      if (startPricesBuilder_ == null) {
        return startPrices_.get(index);  } else {
        return startPricesBuilder_.getMessageOrBuilder(index);
      }
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public java.util.List<? extends com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDtoOrBuilder> 
         getStartPricesOrBuilderList() {
      if (startPricesBuilder_ != null) {
        return startPricesBuilder_.getMessageOrBuilderList();
      } else {
        return java.util.Collections.unmodifiableList(startPrices_);
      }
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.Builder addStartPricesBuilder() {
      return getStartPricesFieldBuilder().addBuilder(
          com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.getDefaultInstance());
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.Builder addStartPricesBuilder(
        int index) {
      return getStartPricesFieldBuilder().addBuilder(
          index, com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.getDefaultInstance());
    }
    /**
     * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto start_prices = 1;</code>
     */
    public java.util.List<com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.Builder> 
         getStartPricesBuilderList() {
      return getStartPricesFieldBuilder().getBuilderList();
    }
    private com.google.protobuf.RepeatedFieldBuilder<
        com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto, com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.Builder, com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDtoOrBuilder> 
        getStartPricesFieldBuilder() {
      if (startPricesBuilder_ == null) {
        startPricesBuilder_ = new com.google.protobuf.RepeatedFieldBuilder<
            com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto, com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto.Builder, com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDtoOrBuilder>(
                startPrices_,
                ((bitField0_ & 0x00000001) == 0x00000001),
                getParentForChildren(),
                isClean());
        startPrices_ = null;
      }
      return startPricesBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto)
  }

  // @@protoc_insertion_point(class_scope:com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto)
  private static final com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto();
  }

  public static com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<GetStartingPricesResponseDto>
      PARSER = new com.google.protobuf.AbstractParser<GetStartingPricesResponseDto>() {
    public GetStartingPricesResponseDto parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new GetStartingPricesResponseDto(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<GetStartingPricesResponseDto> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<GetStartingPricesResponseDto> getParserForType() {
    return PARSER;
  }

  public com.accelfin.server.demo.fx.dtos.GetStartingPricesResponseDto getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

