// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

/**
 * Protobuf type {@code com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto}
 */
public  final class GetHistoricalPricesRequestDto extends
    com.google.protobuf.GeneratedMessage implements
    // @@protoc_insertion_point(message_implements:com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto)
    GetHistoricalPricesRequestDtoOrBuilder {
  // Use GetHistoricalPricesRequestDto.newBuilder() to construct.
  private GetHistoricalPricesRequestDto(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
    super(builder);
  }
  private GetHistoricalPricesRequestDto() {
    symbols_ = com.google.protobuf.LazyStringArrayList.EMPTY;
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
  }
  private GetHistoricalPricesRequestDto(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    int mutable_bitField0_ = 0;
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          default: {
            if (!input.skipField(tag)) {
              done = true;
            }
            break;
          }
          case 10: {
            java.lang.String s = input.readStringRequireUtf8();
            if (!((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
              symbols_ = new com.google.protobuf.LazyStringArrayList();
              mutable_bitField0_ |= 0x00000001;
            }
            symbols_.add(s);
            break;
          }
          case 18: {
            com.accelfin.server.messaging.dtos.TimestampDto.Builder subBuilder = null;
            if (start_ != null) {
              subBuilder = start_.toBuilder();
            }
            start_ = input.readMessage(com.accelfin.server.messaging.dtos.TimestampDto.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(start_);
              start_ = subBuilder.buildPartial();
            }

            break;
          }
          case 26: {
            com.accelfin.server.messaging.dtos.TimestampDto.Builder subBuilder = null;
            if (end_ != null) {
              subBuilder = end_.toBuilder();
            }
            end_ = input.readMessage(com.accelfin.server.messaging.dtos.TimestampDto.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(end_);
              end_ = subBuilder.buildPartial();
            }

            break;
          }
          case 34: {
            com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto.Builder subBuilder = null;
            if (interpolationSettings_ != null) {
              subBuilder = interpolationSettings_.toBuilder();
            }
            interpolationSettings_ = input.readMessage(com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(interpolationSettings_);
              interpolationSettings_ = subBuilder.buildPartial();
            }

            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      if (((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
        symbols_ = symbols_.getUnmodifiableView();
      }
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetHistoricalPricesRequestDto_descriptor;
  }

  protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetHistoricalPricesRequestDto_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto.class, com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto.Builder.class);
  }

  private int bitField0_;
  public static final int SYMBOLS_FIELD_NUMBER = 1;
  private com.google.protobuf.LazyStringList symbols_;
  /**
   * <code>repeated string symbols = 1;</code>
   */
  public com.google.protobuf.ProtocolStringList
      getSymbolsList() {
    return symbols_;
  }
  /**
   * <code>repeated string symbols = 1;</code>
   */
  public int getSymbolsCount() {
    return symbols_.size();
  }
  /**
   * <code>repeated string symbols = 1;</code>
   */
  public java.lang.String getSymbols(int index) {
    return symbols_.get(index);
  }
  /**
   * <code>repeated string symbols = 1;</code>
   */
  public com.google.protobuf.ByteString
      getSymbolsBytes(int index) {
    return symbols_.getByteString(index);
  }

  public static final int START_FIELD_NUMBER = 2;
  private com.accelfin.server.messaging.dtos.TimestampDto start_;
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
   */
  public boolean hasStart() {
    return start_ != null;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
   */
  public com.accelfin.server.messaging.dtos.TimestampDto getStart() {
    return start_ == null ? com.accelfin.server.messaging.dtos.TimestampDto.getDefaultInstance() : start_;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
   */
  public com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder getStartOrBuilder() {
    return getStart();
  }

  public static final int END_FIELD_NUMBER = 3;
  private com.accelfin.server.messaging.dtos.TimestampDto end_;
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
   */
  public boolean hasEnd() {
    return end_ != null;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
   */
  public com.accelfin.server.messaging.dtos.TimestampDto getEnd() {
    return end_ == null ? com.accelfin.server.messaging.dtos.TimestampDto.getDefaultInstance() : end_;
  }
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
   */
  public com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder getEndOrBuilder() {
    return getEnd();
  }

  public static final int INTERPOLATION_SETTINGS_FIELD_NUMBER = 4;
  private com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolationSettings_;
  /**
   * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
   */
  public boolean hasInterpolationSettings() {
    return interpolationSettings_ != null;
  }
  /**
   * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
   */
  public com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto getInterpolationSettings() {
    return interpolationSettings_ == null ? com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto.getDefaultInstance() : interpolationSettings_;
  }
  /**
   * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
   */
  public com.accelfin.server.demo.fx.dtos.InterpolationSettingsDtoOrBuilder getInterpolationSettingsOrBuilder() {
    return getInterpolationSettings();
  }

  private byte memoizedIsInitialized = -1;
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    for (int i = 0; i < symbols_.size(); i++) {
      com.google.protobuf.GeneratedMessage.writeString(output, 1, symbols_.getRaw(i));
    }
    if (start_ != null) {
      output.writeMessage(2, getStart());
    }
    if (end_ != null) {
      output.writeMessage(3, getEnd());
    }
    if (interpolationSettings_ != null) {
      output.writeMessage(4, getInterpolationSettings());
    }
  }

  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    {
      int dataSize = 0;
      for (int i = 0; i < symbols_.size(); i++) {
        dataSize += computeStringSizeNoTag(symbols_.getRaw(i));
      }
      size += dataSize;
      size += 1 * getSymbolsList().size();
    }
    if (start_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getStart());
    }
    if (end_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(3, getEnd());
    }
    if (interpolationSettings_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(4, getInterpolationSettings());
    }
    memoizedSize = size;
    return size;
  }

  private static final long serialVersionUID = 0L;
  public static com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input);
  }
  public static com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessage
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessage.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessage.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto)
      com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDtoOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetHistoricalPricesRequestDto_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetHistoricalPricesRequestDto_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto.class, com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto.Builder.class);
    }

    // Construct using com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
      }
    }
    public Builder clear() {
      super.clear();
      symbols_ = com.google.protobuf.LazyStringArrayList.EMPTY;
      bitField0_ = (bitField0_ & ~0x00000001);
      if (startBuilder_ == null) {
        start_ = null;
      } else {
        start_ = null;
        startBuilder_ = null;
      }
      if (endBuilder_ == null) {
        end_ = null;
      } else {
        end_ = null;
        endBuilder_ = null;
      }
      if (interpolationSettingsBuilder_ == null) {
        interpolationSettings_ = null;
      } else {
        interpolationSettings_ = null;
        interpolationSettingsBuilder_ = null;
      }
      return this;
    }

    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.internal_static_com_accelfin_server_demo_fx_dtos_GetHistoricalPricesRequestDto_descriptor;
    }

    public com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto getDefaultInstanceForType() {
      return com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto.getDefaultInstance();
    }

    public com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto build() {
      com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    public com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto buildPartial() {
      com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto result = new com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto(this);
      int from_bitField0_ = bitField0_;
      int to_bitField0_ = 0;
      if (((bitField0_ & 0x00000001) == 0x00000001)) {
        symbols_ = symbols_.getUnmodifiableView();
        bitField0_ = (bitField0_ & ~0x00000001);
      }
      result.symbols_ = symbols_;
      if (startBuilder_ == null) {
        result.start_ = start_;
      } else {
        result.start_ = startBuilder_.build();
      }
      if (endBuilder_ == null) {
        result.end_ = end_;
      } else {
        result.end_ = endBuilder_.build();
      }
      if (interpolationSettingsBuilder_ == null) {
        result.interpolationSettings_ = interpolationSettings_;
      } else {
        result.interpolationSettings_ = interpolationSettingsBuilder_.build();
      }
      result.bitField0_ = to_bitField0_;
      onBuilt();
      return result;
    }

    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto) {
        return mergeFrom((com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto other) {
      if (other == com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto.getDefaultInstance()) return this;
      if (!other.symbols_.isEmpty()) {
        if (symbols_.isEmpty()) {
          symbols_ = other.symbols_;
          bitField0_ = (bitField0_ & ~0x00000001);
        } else {
          ensureSymbolsIsMutable();
          symbols_.addAll(other.symbols_);
        }
        onChanged();
      }
      if (other.hasStart()) {
        mergeStart(other.getStart());
      }
      if (other.hasEnd()) {
        mergeEnd(other.getEnd());
      }
      if (other.hasInterpolationSettings()) {
        mergeInterpolationSettings(other.getInterpolationSettings());
      }
      onChanged();
      return this;
    }

    public final boolean isInitialized() {
      return true;
    }

    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int bitField0_;

    private com.google.protobuf.LazyStringList symbols_ = com.google.protobuf.LazyStringArrayList.EMPTY;
    private void ensureSymbolsIsMutable() {
      if (!((bitField0_ & 0x00000001) == 0x00000001)) {
        symbols_ = new com.google.protobuf.LazyStringArrayList(symbols_);
        bitField0_ |= 0x00000001;
       }
    }
    /**
     * <code>repeated string symbols = 1;</code>
     */
    public com.google.protobuf.ProtocolStringList
        getSymbolsList() {
      return symbols_.getUnmodifiableView();
    }
    /**
     * <code>repeated string symbols = 1;</code>
     */
    public int getSymbolsCount() {
      return symbols_.size();
    }
    /**
     * <code>repeated string symbols = 1;</code>
     */
    public java.lang.String getSymbols(int index) {
      return symbols_.get(index);
    }
    /**
     * <code>repeated string symbols = 1;</code>
     */
    public com.google.protobuf.ByteString
        getSymbolsBytes(int index) {
      return symbols_.getByteString(index);
    }
    /**
     * <code>repeated string symbols = 1;</code>
     */
    public Builder setSymbols(
        int index, java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  ensureSymbolsIsMutable();
      symbols_.set(index, value);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string symbols = 1;</code>
     */
    public Builder addSymbols(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  ensureSymbolsIsMutable();
      symbols_.add(value);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string symbols = 1;</code>
     */
    public Builder addAllSymbols(
        java.lang.Iterable<java.lang.String> values) {
      ensureSymbolsIsMutable();
      com.google.protobuf.AbstractMessageLite.Builder.addAll(
          values, symbols_);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string symbols = 1;</code>
     */
    public Builder clearSymbols() {
      symbols_ = com.google.protobuf.LazyStringArrayList.EMPTY;
      bitField0_ = (bitField0_ & ~0x00000001);
      onChanged();
      return this;
    }
    /**
     * <code>repeated string symbols = 1;</code>
     */
    public Builder addSymbolsBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      ensureSymbolsIsMutable();
      symbols_.add(value);
      onChanged();
      return this;
    }

    private com.accelfin.server.messaging.dtos.TimestampDto start_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.TimestampDto, com.accelfin.server.messaging.dtos.TimestampDto.Builder, com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder> startBuilder_;
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
     */
    public boolean hasStart() {
      return startBuilder_ != null || start_ != null;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
     */
    public com.accelfin.server.messaging.dtos.TimestampDto getStart() {
      if (startBuilder_ == null) {
        return start_ == null ? com.accelfin.server.messaging.dtos.TimestampDto.getDefaultInstance() : start_;
      } else {
        return startBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
     */
    public Builder setStart(com.accelfin.server.messaging.dtos.TimestampDto value) {
      if (startBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        start_ = value;
        onChanged();
      } else {
        startBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
     */
    public Builder setStart(
        com.accelfin.server.messaging.dtos.TimestampDto.Builder builderForValue) {
      if (startBuilder_ == null) {
        start_ = builderForValue.build();
        onChanged();
      } else {
        startBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
     */
    public Builder mergeStart(com.accelfin.server.messaging.dtos.TimestampDto value) {
      if (startBuilder_ == null) {
        if (start_ != null) {
          start_ =
            com.accelfin.server.messaging.dtos.TimestampDto.newBuilder(start_).mergeFrom(value).buildPartial();
        } else {
          start_ = value;
        }
        onChanged();
      } else {
        startBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
     */
    public Builder clearStart() {
      if (startBuilder_ == null) {
        start_ = null;
        onChanged();
      } else {
        start_ = null;
        startBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
     */
    public com.accelfin.server.messaging.dtos.TimestampDto.Builder getStartBuilder() {
      
      onChanged();
      return getStartFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
     */
    public com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder getStartOrBuilder() {
      if (startBuilder_ != null) {
        return startBuilder_.getMessageOrBuilder();
      } else {
        return start_ == null ?
            com.accelfin.server.messaging.dtos.TimestampDto.getDefaultInstance() : start_;
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.TimestampDto, com.accelfin.server.messaging.dtos.TimestampDto.Builder, com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder> 
        getStartFieldBuilder() {
      if (startBuilder_ == null) {
        startBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            com.accelfin.server.messaging.dtos.TimestampDto, com.accelfin.server.messaging.dtos.TimestampDto.Builder, com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder>(
                getStart(),
                getParentForChildren(),
                isClean());
        start_ = null;
      }
      return startBuilder_;
    }

    private com.accelfin.server.messaging.dtos.TimestampDto end_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.TimestampDto, com.accelfin.server.messaging.dtos.TimestampDto.Builder, com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder> endBuilder_;
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
     */
    public boolean hasEnd() {
      return endBuilder_ != null || end_ != null;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
     */
    public com.accelfin.server.messaging.dtos.TimestampDto getEnd() {
      if (endBuilder_ == null) {
        return end_ == null ? com.accelfin.server.messaging.dtos.TimestampDto.getDefaultInstance() : end_;
      } else {
        return endBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
     */
    public Builder setEnd(com.accelfin.server.messaging.dtos.TimestampDto value) {
      if (endBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        end_ = value;
        onChanged();
      } else {
        endBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
     */
    public Builder setEnd(
        com.accelfin.server.messaging.dtos.TimestampDto.Builder builderForValue) {
      if (endBuilder_ == null) {
        end_ = builderForValue.build();
        onChanged();
      } else {
        endBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
     */
    public Builder mergeEnd(com.accelfin.server.messaging.dtos.TimestampDto value) {
      if (endBuilder_ == null) {
        if (end_ != null) {
          end_ =
            com.accelfin.server.messaging.dtos.TimestampDto.newBuilder(end_).mergeFrom(value).buildPartial();
        } else {
          end_ = value;
        }
        onChanged();
      } else {
        endBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
     */
    public Builder clearEnd() {
      if (endBuilder_ == null) {
        end_ = null;
        onChanged();
      } else {
        end_ = null;
        endBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
     */
    public com.accelfin.server.messaging.dtos.TimestampDto.Builder getEndBuilder() {
      
      onChanged();
      return getEndFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
     */
    public com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder getEndOrBuilder() {
      if (endBuilder_ != null) {
        return endBuilder_.getMessageOrBuilder();
      } else {
        return end_ == null ?
            com.accelfin.server.messaging.dtos.TimestampDto.getDefaultInstance() : end_;
      }
    }
    /**
     * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto end = 3;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.messaging.dtos.TimestampDto, com.accelfin.server.messaging.dtos.TimestampDto.Builder, com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder> 
        getEndFieldBuilder() {
      if (endBuilder_ == null) {
        endBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            com.accelfin.server.messaging.dtos.TimestampDto, com.accelfin.server.messaging.dtos.TimestampDto.Builder, com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder>(
                getEnd(),
                getParentForChildren(),
                isClean());
        end_ = null;
      }
      return endBuilder_;
    }

    private com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolationSettings_ = null;
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto, com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto.Builder, com.accelfin.server.demo.fx.dtos.InterpolationSettingsDtoOrBuilder> interpolationSettingsBuilder_;
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
     */
    public boolean hasInterpolationSettings() {
      return interpolationSettingsBuilder_ != null || interpolationSettings_ != null;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
     */
    public com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto getInterpolationSettings() {
      if (interpolationSettingsBuilder_ == null) {
        return interpolationSettings_ == null ? com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto.getDefaultInstance() : interpolationSettings_;
      } else {
        return interpolationSettingsBuilder_.getMessage();
      }
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
     */
    public Builder setInterpolationSettings(com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto value) {
      if (interpolationSettingsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        interpolationSettings_ = value;
        onChanged();
      } else {
        interpolationSettingsBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
     */
    public Builder setInterpolationSettings(
        com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto.Builder builderForValue) {
      if (interpolationSettingsBuilder_ == null) {
        interpolationSettings_ = builderForValue.build();
        onChanged();
      } else {
        interpolationSettingsBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
     */
    public Builder mergeInterpolationSettings(com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto value) {
      if (interpolationSettingsBuilder_ == null) {
        if (interpolationSettings_ != null) {
          interpolationSettings_ =
            com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto.newBuilder(interpolationSettings_).mergeFrom(value).buildPartial();
        } else {
          interpolationSettings_ = value;
        }
        onChanged();
      } else {
        interpolationSettingsBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
     */
    public Builder clearInterpolationSettings() {
      if (interpolationSettingsBuilder_ == null) {
        interpolationSettings_ = null;
        onChanged();
      } else {
        interpolationSettings_ = null;
        interpolationSettingsBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
     */
    public com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto.Builder getInterpolationSettingsBuilder() {
      
      onChanged();
      return getInterpolationSettingsFieldBuilder().getBuilder();
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
     */
    public com.accelfin.server.demo.fx.dtos.InterpolationSettingsDtoOrBuilder getInterpolationSettingsOrBuilder() {
      if (interpolationSettingsBuilder_ != null) {
        return interpolationSettingsBuilder_.getMessageOrBuilder();
      } else {
        return interpolationSettings_ == null ?
            com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto.getDefaultInstance() : interpolationSettings_;
      }
    }
    /**
     * <code>optional .com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto interpolation_settings = 4;</code>
     */
    private com.google.protobuf.SingleFieldBuilder<
        com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto, com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto.Builder, com.accelfin.server.demo.fx.dtos.InterpolationSettingsDtoOrBuilder> 
        getInterpolationSettingsFieldBuilder() {
      if (interpolationSettingsBuilder_ == null) {
        interpolationSettingsBuilder_ = new com.google.protobuf.SingleFieldBuilder<
            com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto, com.accelfin.server.demo.fx.dtos.InterpolationSettingsDto.Builder, com.accelfin.server.demo.fx.dtos.InterpolationSettingsDtoOrBuilder>(
                getInterpolationSettings(),
                getParentForChildren(),
                isClean());
        interpolationSettings_ = null;
      }
      return interpolationSettingsBuilder_;
    }
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }

    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return this;
    }


    // @@protoc_insertion_point(builder_scope:com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto)
  }

  // @@protoc_insertion_point(class_scope:com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto)
  private static final com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto();
  }

  public static com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<GetHistoricalPricesRequestDto>
      PARSER = new com.google.protobuf.AbstractParser<GetHistoricalPricesRequestDto>() {
    public GetHistoricalPricesRequestDto parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
        return new GetHistoricalPricesRequestDto(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<GetHistoricalPricesRequestDto> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<GetHistoricalPricesRequestDto> getParserForType() {
    return PARSER;
  }

  public com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

