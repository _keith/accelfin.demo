// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

public interface FxSpotPriceDtoOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.accelfin.server.demo.fx.dtos.FxSpotPriceDto)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>optional string id = 1;</code>
   */
  java.lang.String getId();
  /**
   * <code>optional string id = 1;</code>
   */
  com.google.protobuf.ByteString
      getIdBytes();

  /**
   * <code>optional string symbol = 2;</code>
   */
  java.lang.String getSymbol();
  /**
   * <code>optional string symbol = 2;</code>
   */
  com.google.protobuf.ByteString
      getSymbolBytes();

  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto spot_date = 3;</code>
   */
  boolean hasSpotDate();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto spot_date = 3;</code>
   */
  com.accelfin.server.messaging.dtos.TimestampDto getSpotDate();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto spot_date = 3;</code>
   */
  com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder getSpotDateOrBuilder();

  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto mid = 4;</code>
   */
  boolean hasMid();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto mid = 4;</code>
   */
  com.accelfin.server.messaging.dtos.DecimalDto getMid();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto mid = 4;</code>
   */
  com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getMidOrBuilder();

  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto ask = 5;</code>
   */
  boolean hasAsk();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto ask = 5;</code>
   */
  com.accelfin.server.messaging.dtos.DecimalDto getAsk();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto ask = 5;</code>
   */
  com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getAskOrBuilder();

  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto bid = 6;</code>
   */
  boolean hasBid();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto bid = 6;</code>
   */
  com.accelfin.server.messaging.dtos.DecimalDto getBid();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto bid = 6;</code>
   */
  com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getBidOrBuilder();

  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto spread = 7;</code>
   */
  boolean hasSpread();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto spread = 7;</code>
   */
  com.accelfin.server.messaging.dtos.DecimalDto getSpread();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.DecimalDto spread = 7;</code>
   */
  com.accelfin.server.messaging.dtos.DecimalDtoOrBuilder getSpreadOrBuilder();

  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto creation_date = 8;</code>
   */
  boolean hasCreationDate();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto creation_date = 8;</code>
   */
  com.accelfin.server.messaging.dtos.TimestampDto getCreationDate();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto creation_date = 8;</code>
   */
  com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder getCreationDateOrBuilder();

  /**
   * <code>optional bool is_tradable = 9;</code>
   */
  boolean getIsTradable();
}
