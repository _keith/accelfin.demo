// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

public interface GetHistoricalPricesResponseDtoOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.accelfin.server.demo.fx.dtos.GetHistoricalPricesResponseDto)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto prices = 1;</code>
   */
  java.util.List<com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto> 
      getPricesList();
  /**
   * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto prices = 1;</code>
   */
  com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto getPrices(int index);
  /**
   * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto prices = 1;</code>
   */
  int getPricesCount();
  /**
   * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto prices = 1;</code>
   */
  java.util.List<? extends com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDtoOrBuilder> 
      getPricesOrBuilderList();
  /**
   * <code>repeated .com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDto prices = 1;</code>
   */
  com.accelfin.server.demo.fx.dtos.FxHistoricalPriceDtoOrBuilder getPricesOrBuilder(
      int index);
}
