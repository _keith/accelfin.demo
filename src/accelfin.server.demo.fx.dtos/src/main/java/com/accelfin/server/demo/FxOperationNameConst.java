package com.accelfin.server.demo;

import com.accelfin.server.messaging.OperationNames;

public class FxOperationNameConst extends OperationNames {
    public static final String Login = "login";
    public static final String ClientHeartbeat = "clientHeartbeat";
    public static final String PriceSubscriptionUpdate = "priceSubscriptionUpdate";
    public static final String PriceUpdate = "priceUpdate";
    public static final String CashBlotterSubscribe = "cashBlotterSubscribe";
    public static final String CashBlotterUpdate = "cashBlotterUpdate";
    public static final String ExecuteSpot = "executeSpot";
    public static final String ExecuteSimpleOrder = "executeSimpleOrder";
    public static final String GetStartingPrices = "getStartingPrices";
    public static final String GetHistoricalPrices = "getHistoricalPrices";
    public static final String GetHistoricalTrades = "getHistoricalTrades";
    public static final String OrderBlotterSubscribe = "orderBlotterSubscribe";
    public static final String OrderBlotterUpdate = "orderBlotterUpdate";
    public static final String OrderBookSubscribe = "orderBookSubscribe";
    public static final String OrderBookUpdate = "orderBookUpdate";
}
