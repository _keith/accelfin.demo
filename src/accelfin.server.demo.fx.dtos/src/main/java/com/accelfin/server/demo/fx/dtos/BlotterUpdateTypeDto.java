// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

/**
 * Protobuf enum {@code com.accelfin.server.demo.fx.dtos.BlotterUpdateTypeDto}
 */
public enum BlotterUpdateTypeDto
    implements com.google.protobuf.ProtocolMessageEnum {
  /**
   * <code>NORMAL = 0;</code>
   */
  NORMAL(0),
  /**
   * <code>STATE_OF_THE_WORLD = 1;</code>
   */
  STATE_OF_THE_WORLD(1),
  UNRECOGNIZED(-1),
  ;

  /**
   * <code>NORMAL = 0;</code>
   */
  public static final int NORMAL_VALUE = 0;
  /**
   * <code>STATE_OF_THE_WORLD = 1;</code>
   */
  public static final int STATE_OF_THE_WORLD_VALUE = 1;


  public final int getNumber() {
    if (this == UNRECOGNIZED) {
      throw new java.lang.IllegalArgumentException(
          "Can't get the number of an unknown enum value.");
    }
    return value;
  }

  /**
   * @deprecated Use {@link #forNumber(int)} instead.
   */
  @java.lang.Deprecated
  public static BlotterUpdateTypeDto valueOf(int value) {
    return forNumber(value);
  }

  public static BlotterUpdateTypeDto forNumber(int value) {
    switch (value) {
      case 0: return NORMAL;
      case 1: return STATE_OF_THE_WORLD;
      default: return null;
    }
  }

  public static com.google.protobuf.Internal.EnumLiteMap<BlotterUpdateTypeDto>
      internalGetValueMap() {
    return internalValueMap;
  }
  private static final com.google.protobuf.Internal.EnumLiteMap<
      BlotterUpdateTypeDto> internalValueMap =
        new com.google.protobuf.Internal.EnumLiteMap<BlotterUpdateTypeDto>() {
          public BlotterUpdateTypeDto findValueByNumber(int number) {
            return BlotterUpdateTypeDto.forNumber(number);
          }
        };

  public final com.google.protobuf.Descriptors.EnumValueDescriptor
      getValueDescriptor() {
    return getDescriptor().getValues().get(ordinal());
  }
  public final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptorForType() {
    return getDescriptor();
  }
  public static final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptor() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.getDescriptor()
        .getEnumTypes().get(2);
  }

  private static final BlotterUpdateTypeDto[] VALUES = values();

  public static BlotterUpdateTypeDto valueOf(
      com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
    if (desc.getType() != getDescriptor()) {
      throw new java.lang.IllegalArgumentException(
        "EnumValueDescriptor is not for this type.");
    }
    if (desc.getIndex() == -1) {
      return UNRECOGNIZED;
    }
    return VALUES[desc.getIndex()];
  }

  private final int value;

  private BlotterUpdateTypeDto(int value) {
    this.value = value;
  }

  // @@protoc_insertion_point(enum_scope:com.accelfin.server.demo.fx.dtos.BlotterUpdateTypeDto)
}

