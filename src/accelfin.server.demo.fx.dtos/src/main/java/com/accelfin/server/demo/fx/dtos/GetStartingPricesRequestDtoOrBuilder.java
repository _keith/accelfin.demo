// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

public interface GetStartingPricesRequestDtoOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
   */
  boolean hasStart();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
   */
  com.accelfin.server.messaging.dtos.TimestampDto getStart();
  /**
   * <code>optional .com.accelfin.server.messaging.dtos.TimestampDto start = 1;</code>
   */
  com.accelfin.server.messaging.dtos.TimestampDtoOrBuilder getStartOrBuilder();
}
