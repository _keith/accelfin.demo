// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: accelfin-server-demo-fx-dtos.proto

package com.accelfin.server.demo.fx.dtos;

/**
 * Protobuf enum {@code com.accelfin.server.demo.fx.dtos.OrderTypeDto}
 */
public enum OrderTypeDto
    implements com.google.protobuf.ProtocolMessageEnum {
  /**
   * <code>TAKE_PROFIT = 0;</code>
   */
  TAKE_PROFIT(0),
  /**
   * <code>STOP_LOSS = 1;</code>
   */
  STOP_LOSS(1),
  /**
   * <code>LIMIT = 2;</code>
   */
  LIMIT(2),
  UNRECOGNIZED(-1),
  ;

  /**
   * <code>TAKE_PROFIT = 0;</code>
   */
  public static final int TAKE_PROFIT_VALUE = 0;
  /**
   * <code>STOP_LOSS = 1;</code>
   */
  public static final int STOP_LOSS_VALUE = 1;
  /**
   * <code>LIMIT = 2;</code>
   */
  public static final int LIMIT_VALUE = 2;


  public final int getNumber() {
    if (this == UNRECOGNIZED) {
      throw new java.lang.IllegalArgumentException(
          "Can't get the number of an unknown enum value.");
    }
    return value;
  }

  /**
   * @deprecated Use {@link #forNumber(int)} instead.
   */
  @java.lang.Deprecated
  public static OrderTypeDto valueOf(int value) {
    return forNumber(value);
  }

  public static OrderTypeDto forNumber(int value) {
    switch (value) {
      case 0: return TAKE_PROFIT;
      case 1: return STOP_LOSS;
      case 2: return LIMIT;
      default: return null;
    }
  }

  public static com.google.protobuf.Internal.EnumLiteMap<OrderTypeDto>
      internalGetValueMap() {
    return internalValueMap;
  }
  private static final com.google.protobuf.Internal.EnumLiteMap<
      OrderTypeDto> internalValueMap =
        new com.google.protobuf.Internal.EnumLiteMap<OrderTypeDto>() {
          public OrderTypeDto findValueByNumber(int number) {
            return OrderTypeDto.forNumber(number);
          }
        };

  public final com.google.protobuf.Descriptors.EnumValueDescriptor
      getValueDescriptor() {
    return getDescriptor().getValues().get(ordinal());
  }
  public final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptorForType() {
    return getDescriptor();
  }
  public static final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptor() {
    return com.accelfin.server.demo.fx.dtos.AccelfinServerDemoFxDtos.getDescriptor()
        .getEnumTypes().get(3);
  }

  private static final OrderTypeDto[] VALUES = values();

  public static OrderTypeDto valueOf(
      com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
    if (desc.getType() != getDescriptor()) {
      throw new java.lang.IllegalArgumentException(
        "EnumValueDescriptor is not for this type.");
    }
    if (desc.getIndex() == -1) {
      return UNRECOGNIZED;
    }
    return VALUES[desc.getIndex()];
  }

  private final int value;

  private OrderTypeDto(int value) {
    this.value = value;
  }

  // @@protoc_insertion_point(enum_scope:com.accelfin.server.demo.fx.dtos.OrderTypeDto)
}

