package com.accelfin.server.demo.analytics.model.reports;

import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.SessionToken;
import com.accelfin.server.demo.analytics.mappers.HistoricalDataRequestMapper;
import com.accelfin.server.demo.analytics.mappers.FxAlertsReportResponseMapper;
import com.accelfin.server.demo.analytics.model.AnalyticsService;
import com.accelfin.server.demo.analytics.model.events.operationEvents.FxAlertReportUpdate;
import com.accelfin.server.demo.analytics.model.events.operationEvents.UpdateReportSubscriptionRequestOperationEvent;
import com.esp.Router;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages all instances of FxAlertsReport users may have running
 */
public class FxAlertsReports {
    private static Logger logger = LoggerFactory.getLogger(FxAlertsReports.class);
    private ConnectionBroker _connectionBroker;
    private Router<AnalyticsService> _router;
    private HistoricalDataRequestMapper _historicalDataRequestMapper;
    private FxAlertsReportResponseMapper _fxAlertsReportResponseMapper;
    private Map<String, FxAlertsReport> _reportsByReportId = new HashMap<>();

    @Inject
    public FxAlertsReports(
            ConnectionBroker connectionBroker,
            Router<AnalyticsService> router,
            HistoricalDataRequestMapper historicalDataRequestMapper,
            FxAlertsReportResponseMapper fxAlertsReportResponseMapper
    ) {
        _connectionBroker = connectionBroker;
        _router = router;
        _historicalDataRequestMapper = historicalDataRequestMapper;
        _fxAlertsReportResponseMapper = fxAlertsReportResponseMapper;
    }

    public void processReportUpdate(
            UpdateReportSubscriptionRequestOperationEvent event,
            SessionToken sessionToken,
            FxAlertReportUpdate reportUpdate
    ) {
        FxAlertsReport report = _reportsByReportId.computeIfAbsent(
                reportUpdate.getReportId(),
                reportId -> new FxAlertsReport(reportId, sessionToken, _connectionBroker, _historicalDataRequestMapper, _fxAlertsReportResponseMapper)
        );
        report.update(
                event,
                reportUpdate.getSymbols(),
                reportUpdate.getRangeViewStart(),
                reportUpdate.getRangeViewEnd(),
                reportUpdate.getMainViewStart(),
                reportUpdate.getMainViewEnd()
        );
    }
}
