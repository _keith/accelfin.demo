package com.accelfin.server.demo.analytics.container;

import com.accelfin.server.Clock;
import com.accelfin.server.Scheduler;
import com.accelfin.server.ServiceModelBase;
import com.accelfin.server.demo.AnalyicsOperationNameConst;
import com.accelfin.server.demo.analytics.mappers.AnalyticsServiceInboundMessageEventMapper;
import com.accelfin.server.messaging.Connections;
import com.accelfin.server.messaging.ConnectionsBuilder;
import com.accelfin.server.messaging.config.SerializationType;
import com.accelfin.server.messaging.config.operations.OperationOwnership;
import com.accelfin.server.messaging.config.operations.OperationType;
import com.esp.Router;
import com.google.inject.Inject;
import com.google.inject.Provider;

public class ConnectionsProvider implements Provider<Connections> {
    private Router<ServiceModelBase> _router;
    private Clock _clock;
    private Scheduler _scheduler;
    private AnalyticsServiceInboundMessageEventMapper _eventMapper;

    @Inject
    public ConnectionsProvider(
            Router<ServiceModelBase> router,
            Clock clock,
            Scheduler scheduler,
            AnalyticsServiceInboundMessageEventMapper eventMapper
    ) {
        _router = router;
        _clock = clock;
        _scheduler = scheduler;
        _eventMapper = eventMapper;
    }

    @Override
    public Connections get() {
        return ConnectionsBuilder.create()
                .withConfiguration("messaging.properties")
                .withClock(_clock)
                .withSchedule(_scheduler)
                .withRouter(_router)
                .withConnection("web", cb -> cb
                        .withSerializationType(SerializationType.Json)
                        .withInboundMessageEventMapper(_eventMapper)
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.RPC,
                                AnalyicsOperationNameConst.ReportSubscriptionUpdate,
                                false) // TODO this should be true at some point
                        .withOperation(
                                OperationOwnership.OwnedByThisService,
                                OperationType.Stream,
                                AnalyicsOperationNameConst.ReportUpdate,
                                false)) // TODO this should be true at some point
                .withConnection("internal", cb -> cb
                        .withSerializationType(SerializationType.Binary)
                        .withInboundMessageEventMapper(_eventMapper))
                .build();
    }
}