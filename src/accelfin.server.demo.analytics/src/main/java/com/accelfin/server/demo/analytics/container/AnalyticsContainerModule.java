package com.accelfin.server.demo.analytics.container;

import com.accelfin.server.Clock;
import com.accelfin.server.Scheduler;
import com.accelfin.server.ServiceModelBase;
import com.accelfin.server.demo.analytics.mappers.AnalyticsServiceInboundMessageEventMapper;
import com.accelfin.server.demo.analytics.model.AnalyticsService;
import com.accelfin.server.demo.analytics.model.reports.Reports;
import com.accelfin.server.esp.DefaultTerminalErrorHandler;
import com.accelfin.server.messaging.Connections;
import com.accelfin.server.messaging.mappers.InboundMessageEventMapper;
import com.esp.DefaultRouter;
import com.esp.Router;
import com.esp.RouterDispatcher;
import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;

public class AnalyticsContainerModule extends AbstractModule {

    private RouterDispatcher _routerDispatcher;
    private Scheduler _scheduler;

    public AnalyticsContainerModule(RouterDispatcher routerDispatcher, Scheduler scheduler) {
        _routerDispatcher = routerDispatcher;
        _scheduler = scheduler;
    }

    @Override
    protected void configure() {
        try {
            configureRouter();
            bind(Scheduler.class).toInstance(_scheduler);
            bind(AnalyticsService.class).in(Scopes.SINGLETON);
            bind(Reports.class).in(Scopes.SINGLETON);
            bind(InboundMessageEventMapper.class).to(AnalyticsServiceInboundMessageEventMapper.class).in(Scopes.SINGLETON);
            bind(Clock.class).toConstructor(Clock.class.getConstructor(Router.class, Scheduler.class));
            bind(Connections.class).toProvider(ConnectionsProvider.class).in(Scopes.SINGLETON);
        } catch (NoSuchMethodException e) {
            addError(e);
        }
    }

    private void configureRouter() {
        // HACK need to register the router against Router<ServiceModelBase>, Router<AnalyticsService> and DefaultRouter<AnalyticsService>
        // however I can't see a good way to do this in Guice hence I have to create it and register
        // the instance against the relevant interfaces, using a cast HACK below.
        // The binding syntax doesn't seem to support this and the covariance support in java doesn't recognise
        // they are in fact the same
        DefaultTerminalErrorHandler errorHandler = new DefaultTerminalErrorHandler();
        DefaultRouter<AnalyticsService> router = new DefaultRouter<>(_routerDispatcher, errorHandler);
        bind(new TypeLiteral<Router<AnalyticsService>>() {}).toInstance(router);
        bind(new TypeLiteral<DefaultRouter<AnalyticsService>>() {}).toInstance(router);
        bind(new TypeLiteral<Router<ServiceModelBase>>() {}).toInstance((Router<ServiceModelBase>)(Router)router);
    }
}