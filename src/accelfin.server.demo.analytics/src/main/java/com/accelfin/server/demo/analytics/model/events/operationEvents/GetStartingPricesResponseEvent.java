package com.accelfin.server.demo.analytics.model.events.operationEvents;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.analytics.model.reports.FxHistoricalSpotPrice;

import java.util.Map;

public class GetStartingPricesResponseEvent extends InboundMessageEvent {

    private Map<String, FxHistoricalSpotPrice> _startPricesBySymbol;

    public GetStartingPricesResponseEvent(Map<String, FxHistoricalSpotPrice> startPricesBySymbol) {
        _startPricesBySymbol = startPricesBySymbol;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Unauthenticated;
    }

    public Map<String, FxHistoricalSpotPrice> getStartPricesBySymbol() {
        return _startPricesBySymbol;
    }
}

