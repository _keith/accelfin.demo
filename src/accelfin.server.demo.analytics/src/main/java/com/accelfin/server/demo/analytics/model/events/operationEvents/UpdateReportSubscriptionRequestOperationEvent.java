package com.accelfin.server.demo.analytics.model.events.operationEvents;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;

public class UpdateReportSubscriptionRequestOperationEvent extends InboundMessageEvent {
    private  ReportBase[] _reports;

    public UpdateReportSubscriptionRequestOperationEvent(ReportBase[] reports) {
        _reports = reports;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Unauthenticated;
    }

    public ReportBase[] getReports() {
        return _reports;
    }
}