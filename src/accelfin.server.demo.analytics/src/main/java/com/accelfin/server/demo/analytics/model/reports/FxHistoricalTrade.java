package com.accelfin.server.demo.analytics.model.reports;

import com.accelfin.server.demo.common.model.Side;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class FxHistoricalTrade {
    private String _tradeId;
    private String _currencyPair;
    private BigDecimal _notional;
    private Side _side;
    private BigDecimal _executedRate;
    private BigDecimal _normalisedRate;
    private Instant _executedAtTime;
    private boolean _hasAlert;
    private ArrayList<String> _alertCodes = new ArrayList<>();
    private FxHistoricalSpotPrice _spotPrice;

    public FxHistoricalTrade(
            String tradeId,
            String currencyPair,
            BigDecimal notional,
            Side side,
            BigDecimal executedRate,
            Instant executedAtTime,
            FxHistoricalSpotPrice spotPrice) {
        _tradeId = tradeId;
        _currencyPair = currencyPair;
        _notional = notional;
        _side = side;
        _executedRate = executedRate;
        _executedAtTime = executedAtTime;
        _spotPrice = spotPrice;
    }

    public String getTradeId() {
        return _tradeId;
    }

    public String getCurrencyPair() {
        return _currencyPair;
    }

    public BigDecimal getNotional() {
        return _notional;
    }

    public Side getSide() {
        return _side;
    }

    public BigDecimal getExecutedRate() {
        return _executedRate;
    }

    public BigDecimal getNormalisedRate() {
        return _normalisedRate;
    }

    public void setNormalisedRate(BigDecimal normalisedRate) {
        _normalisedRate = normalisedRate;
    }

    public Instant getExecutedAtTime() {
        return _executedAtTime;
    }

    public List<String> getAlertCodes() {
        return _alertCodes;
    }

    public boolean getHasAlert() {
        return _alertCodes.size() > 0;
    }

    public void addAlertCode(String alertCode) {
        _alertCodes.add(alertCode);
    }

    public FxHistoricalSpotPrice getSpotPrice() {
        return _spotPrice;
    }
}
