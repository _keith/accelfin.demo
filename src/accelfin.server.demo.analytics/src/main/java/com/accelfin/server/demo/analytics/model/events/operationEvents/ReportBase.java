package com.accelfin.server.demo.analytics.model.events.operationEvents;

import com.accelfin.server.demo.analytics.model.reports.ReportType;

public abstract class ReportBase {
    private String _reportId;

    public ReportBase(String reportId) {
        _reportId = reportId;
    }

    public String getReportId() {
        return _reportId;
    }

    public abstract ReportType getReportType();
}
