package com.accelfin.server.demo.analytics.model.reports;

import com.accelfin.server.demo.common.model.MathUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;

public class FxHistoricalSpotPrice {
    private String _id;
    private String _symbol;
    private Instant _spotDate;
    private BigDecimal _mid;
    private BigDecimal _ask;
    private BigDecimal _bid;
    private BigDecimal _spread;
    private Instant _creationDate;

    public FxHistoricalSpotPrice(String id, String symbol, Instant spotDate, BigDecimal mid, BigDecimal ask, BigDecimal bid, BigDecimal spread, Instant creationDate) {
        _id = id;
        _symbol = symbol;
        _spotDate = spotDate;
        _mid = mid;
        _ask = ask;
        _bid = bid;
        _spread = spread;
        _creationDate = creationDate;
    }

    public String getId() {
        return _id;
    }

    public String getSymbol() {
        return _symbol;
    }

    public Instant getSpotDate() {
        return _spotDate;
    }

    public BigDecimal getMid() {
        return _mid;
    }

    public BigDecimal getAsk() {
        return _ask;
    }

    public BigDecimal getBid() {
        return _bid;
    }

    public BigDecimal getSpread() {
        return _spread;
    }

    public Instant getCreationDate() {
        return _creationDate;
    }

    public FxHistoricalSpotPrice convertToPercentages(FxHistoricalSpotPrice basePrice) {
        BigDecimal midChangePercentage = MathUtils.getChangePercent(basePrice.getMid(), _mid);
        BigDecimal askChangePercentage = MathUtils.getChangePercent(basePrice.getAsk(), _ask);
        BigDecimal bidChangePercentage = MathUtils.getChangePercent(basePrice.getBid(), _bid);
        BigDecimal spread = askChangePercentage.subtract(bidChangePercentage);
        return new FxHistoricalSpotPrice(
                getId(),
                getSymbol(),
                getSpotDate(),
                midChangePercentage,
                askChangePercentage,
                bidChangePercentage,
                spread,
                getCreationDate()
        );
    }
}
