package com.accelfin.server.demo.analytics.model.reports;

import com.esp.disposables.DisposableBase;
import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.SessionToken;
import com.accelfin.server.demo.analytics.mappers.FxAlertsReportResponseMapper;
import com.accelfin.server.demo.analytics.mappers.HistoricalDataRequestMapper;
import com.accelfin.server.demo.analytics.model.events.operationEvents.GetHistoricalTradesResponseEvent;
import com.accelfin.server.demo.analytics.model.events.operationEvents.GetStartingPricesResponseEvent;
import com.accelfin.server.demo.analytics.model.events.operationEvents.UpdateReportSubscriptionRequestOperationEvent;
import com.accelfin.server.demo.analytics.model.events.operationEvents.GetHistoricalPricesResponseEvent;
import com.accelfin.server.demo.AnalyicsOperationNameConst;
import com.accelfin.server.demo.FxOperationNameConst;
import com.accelfin.server.demo.ServiceTypesConst;
import com.accelfin.server.demo.analytics.dtos.UpdateReportSubscriptionResponseAckDto;
import com.accelfin.server.demo.analytics.dtos.FxAlertsReportResponseDto;
import com.accelfin.server.demo.fx.dtos.GetHistoricalPricesRequestDto;
import com.accelfin.server.demo.fx.dtos.GetHistoricalTradesRequestDto;
import com.accelfin.server.demo.fx.dtos.GetStartingPricesRequestDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.*;

/**
 * Models a report instance as configured on the users screen
 */
public class FxAlertsReport extends DisposableBase {

    private static Logger logger = LoggerFactory.getLogger(FxAlertsReport.class);

    private String _reportId;
    private SessionToken _sessionToken;
    private ConnectionBroker _connectionBroker;
    private HistoricalDataRequestMapper _historicalDataRequestMapper;
    private FxAlertsReportResponseMapper _fxAlertsReportResponseMapper;
    private Instant _rangeViewStart;
    private Instant _rangeViewEnd;
    private Instant _mainViewStart;
    private Instant _mainViewEnd;
    private ArrayList<FxHistoricalSpotPrice> _mainViewPrices = new ArrayList<>();
    private ArrayList<FxHistoricalSpotPrice> _rangeViewPrices = new ArrayList<>();
    private ArrayList<FxHistoricalTrade> _mainViewTrades = new ArrayList<>();
    private ArrayList<String> _subscribedSymbols = new ArrayList<>();
    private FxAlertReportUpdateUnitOfWork _currentStateUpdateUnitOfWork;

    public FxAlertsReport(
            String reportId,
            SessionToken sessionToken,
            ConnectionBroker connectionBroker,
            HistoricalDataRequestMapper historicalDataRequestMapper,
            FxAlertsReportResponseMapper fxAlertsReportResponseMapper
    ) {
        _reportId = reportId;
        _sessionToken = sessionToken;
        _connectionBroker = connectionBroker;
        _historicalDataRequestMapper = historicalDataRequestMapper;
        _fxAlertsReportResponseMapper = fxAlertsReportResponseMapper;
    }

    public String getReportId() {
        return _reportId;
    }

    public List<FxHistoricalSpotPrice> getMainViewPrices() {
        return _mainViewPrices;
    }

    public List<FxHistoricalSpotPrice> getRangeViewPrices() {
        return _rangeViewPrices;
    }

    public List<FxHistoricalTrade> getMainViewTrades() {
        return _mainViewTrades;
    }

    public void update(
            UpdateReportSubscriptionRequestOperationEvent event,
            String[] symbols,
            Instant rangeViewStart,
            Instant rangeViewEnd,
            Instant mainViewStart,
            Instant mainViewEnd
    ) {
        logger.debug("Update request received [id:{},sessionId:{}]", _reportId, _sessionToken);

        reset();

        if (_currentStateUpdateUnitOfWork != null) {
            _currentStateUpdateUnitOfWork.dispose();
        }
        FxAlertReportUpdateUnitOfWork unitOfWork = new FxAlertReportUpdateUnitOfWork(
                UUID.randomUUID().toString(),
                rangeViewStart,
                rangeViewEnd,
                mainViewStart,
                mainViewEnd,
                new ArrayList<>(Arrays.asList(symbols))
        );
        _currentStateUpdateUnitOfWork = unitOfWork;
        logger.debug("Unit of work for report update [id:{},sessionId:{}][{}]", _reportId, _sessionToken, unitOfWork.toString());

        logger.debug("Acking update request [id:{},sessionId:{}]", _reportId, _sessionToken);
        UpdateReportSubscriptionResponseAckDto response = UpdateReportSubscriptionResponseAckDto.newBuilder()
                .setIsSuccess(true)
                .build();
        _connectionBroker.sendRpcResponse(event, response);

        if (unitOfWork.getWillFetchPrices()) {
            logger.debug("Getting historical prices[id:{},sessionId:{}]", _reportId, _sessionToken);
            GetHistoricalPricesRequestDto getHistoricalPricesRequestDto = _historicalDataRequestMapper.mapToHistoricalPricesRequestDto(
                    unitOfWork.getSubscribedSymbols(),
                    unitOfWork.getRangeViewStart(),
                    unitOfWork.getRangeViewEnd()
            );
            _connectionBroker
                    .sendRequest(
                            GetHistoricalPricesResponseEvent.class,
                            ServiceTypesConst.Fx,
                            FxOperationNameConst.GetHistoricalPrices,
                            unitOfWork.getCorrelationId(),
                            getHistoricalPricesRequestDto)
                    .observe((e, c, m) -> {
                        if (!unitOfWork.isDisposed()) {
                            unitOfWork.setIsPricesReceived(true);
                            unitOfWork.setAllPrices(e.getPrices());
                            tryCompleteUnitOfWork(unitOfWork);
                        }
                    });
        }

        logger.debug("Getting starting prices [id:{},sessionId:{}]", _reportId, _sessionToken);
        GetStartingPricesRequestDto getStartingPricesRequestDto = _historicalDataRequestMapper.mapGetStartingPricesRequestDto(
                unitOfWork.getMainViewStart()
        );
        _connectionBroker
                .sendRequest(
                        GetStartingPricesResponseEvent.class,
                        ServiceTypesConst.Fx,
                        FxOperationNameConst.GetStartingPrices,
                        unitOfWork.getCorrelationId(),
                        getStartingPricesRequestDto)
                .observe((e, c, m) -> {
                    if (!unitOfWork.isDisposed()) {
                        unitOfWork.setIStartingPricesReceived(true);
                        unitOfWork.setStartPricesBySymbol(e.getStartPricesBySymbol());
                        tryCompleteUnitOfWork(unitOfWork);
                    }
                });

        logger.debug("Getting historical trades[id:{},sessionId:{}]", _reportId, _sessionToken);
        GetHistoricalTradesRequestDto getHistoricalTradesRequestDto = _historicalDataRequestMapper.mapToHistoricalTradesRequestDto(
                unitOfWork.getMainViewStart(),
                unitOfWork.getMainViewEnd()
        );
        _connectionBroker
                .sendRequest(
                        GetHistoricalTradesResponseEvent.class,
                        ServiceTypesConst.Fx,
                        FxOperationNameConst.GetHistoricalTrades,
                        unitOfWork.getCorrelationId(),
                        getHistoricalTradesRequestDto)
                .observe((e, c, m) -> {
                    if (!unitOfWork.isDisposed()) {
                        unitOfWork.setIsTradesReceived(true);
                        unitOfWork.setTrades(e.getTrades());
                        tryCompleteUnitOfWork(unitOfWork);
                    }
                });
    }

    private void reset() {
        _mainViewPrices.clear();
        _mainViewTrades.clear();
        _rangeViewPrices.clear();
        _rangeViewStart = Instant.EPOCH;
        _rangeViewEnd = Instant.EPOCH;
        _mainViewStart = Instant.EPOCH;
        _mainViewEnd = Instant.EPOCH;
        _subscribedSymbols.clear();
    }

    private void tryCompleteUnitOfWork(FxAlertReportUpdateUnitOfWork unitOfWork) {
        unitOfWork.tryComplete();
        if (unitOfWork.getHasCompleted()) {
            _mainViewPrices = unitOfWork.getMainViewPrices();
            _rangeViewPrices = unitOfWork.getRangeViewPrices();
            _rangeViewStart = unitOfWork.getRangeViewStart();
            _rangeViewEnd = unitOfWork.getRangeViewEnd();
            _mainViewStart = unitOfWork.getMainViewStart();
            _mainViewEnd = unitOfWork.getMainViewEnd();
            _mainViewTrades = unitOfWork.getTrades();
            _subscribedSymbols = unitOfWork.getSubscribedSymbols();
            logStateUpdate(unitOfWork.getAllPrices().size());
            dispatchUpdate();
        }
    }

    private void dispatchUpdate() {
        FxAlertsReportResponseDto responseDto = _fxAlertsReportResponseMapper.mapToDto(this);
        _connectionBroker.sendStreamResponseMessage(
                AnalyicsOperationNameConst.ReportUpdate,
                responseDto,
                false,
                _sessionToken
        );
    }

    private void logStateUpdate(int size) {
        logger.info(
                "[Report:{}]: Sampled {} range view prices down to {}, Sampled main view prices down to {}. Dispatching to client [id:{},sessionId:{}]",
                _reportId,
                size,
                _rangeViewPrices.size(),
                _mainViewPrices.size(),
                _reportId,
                _sessionToken
        );
        logger.info("Updating report [id:{},sessionId:{}]. rangeViewStart [{}] rangeViewEnd [{}] mainViewStart [{}] mainViewEnd [{}] rangeViewPriceCount [{}] mainViewPriceCount [{}] mainViewTradeCount [{}]",
                _reportId,
                _sessionToken,
                _rangeViewStart,
                _rangeViewEnd,
                _mainViewStart,
                _mainViewEnd,
                _rangeViewPrices.size(),
                _mainViewPrices.size(),
                _mainViewTrades.size()
        );
    }
}

