package com.accelfin.server.demo.analytics.mappers;

import com.accelfin.server.demo.fx.dtos.*;
import com.accelfin.server.messaging.dtos.AnyDto;
import com.accelfin.server.messaging.mappers.InboundMessageEventMapperBase;
import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.gateways.AnyDtoUtils;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.analytics.model.events.operationEvents.*;
import com.accelfin.server.demo.analytics.model.reports.FxHistoricalSpotPrice;
import com.accelfin.server.demo.analytics.model.reports.FxHistoricalTrade;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.AnalyicsOperationNameConst;
import com.accelfin.server.demo.FxOperationNameConst;
import com.accelfin.server.demo.analytics.dtos.UpdateReportSubscriptionRequestDto;
import com.accelfin.server.demo.analytics.dtos.FxAlertReportUpdateRequestDto;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.protobuf.Message;

import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapBigDecimalFromDto;
import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapInstantFromDto;

public class AnalyticsServiceInboundMessageEventMapper extends InboundMessageEventMapperBase {

    @Override
    protected InboundMessageEvent tryMapToEvent(MessageEnvelope envelope) {
        InboundMessageEvent event = null;
        switch (envelope.getOperationName()) {
            case AnalyicsOperationNameConst.ReportSubscriptionUpdate:
                event = mapReportSubscriptionUpdate(envelope);
                break;
            case FxOperationNameConst.GetHistoricalPrices:
                event = mapGetHistoricalPrices(envelope);
                break;
            case FxOperationNameConst.GetHistoricalTrades:
                event = mapGetHistoricalTrades(envelope);
                break;
            case FxOperationNameConst.GetStartingPrices:
                event = mapGetStartingPrices(envelope);
                break;
        }
        return event;
    }

    private InboundMessageEvent mapReportSubscriptionUpdate(MessageEnvelope envelope) {
        try {
            UpdateReportSubscriptionRequestDto dto = envelope.getPayload();
            ArrayList<ReportBase> reports = new ArrayList<>();
            for (AnyDto anyDto : dto.getReportsList()) {
                Message innerDto = AnyDtoUtils.deserializePayloadMessage(anyDto);
                if (FxAlertReportUpdateRequestDto.class.isInstance(innerDto)) {
                    reports.add(mapFxAlertReportUpdate((FxAlertReportUpdateRequestDto)innerDto));
                } else {
                    throw new RuntimeException("Unknown report type");
                }
            }
            return new UpdateReportSubscriptionRequestOperationEvent(reports.toArray(new ReportBase[reports.size()]));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private ReportBase mapFxAlertReportUpdate(FxAlertReportUpdateRequestDto updatedto) {
        return new FxAlertReportUpdate(
                updatedto.getReportId(),
                updatedto.getSymbolsList().toArray(new String[updatedto.getSymbolsCount()]),
                mapInstantFromDto(updatedto.getRangeViewStart()),
                mapInstantFromDto(updatedto.getRangeViewEnd()),
                mapInstantFromDto(updatedto.getMainViewStart()),
                mapInstantFromDto(updatedto.getMainViewEnd())
        );
    }

    private InboundMessageEvent mapGetHistoricalPrices(MessageEnvelope envelope) {
        GetHistoricalPricesResponseDto dto = envelope.getPayload();
        ArrayList<FxHistoricalSpotPrice> prices = dto
                .getPricesList()
                .stream()
                .map(this::mapHistoricalPrice)
                .collect(Collectors.toCollection(ArrayList::new));
        return new GetHistoricalPricesResponseEvent(prices);
    }

    private FxHistoricalSpotPrice mapHistoricalPrice(FxHistoricalPriceDto priceDto) {
        return new FxHistoricalSpotPrice(
                priceDto.getId(),
                priceDto.getSymbol(),
                Instant.ofEpochSecond(priceDto.getSpotDate().getSeconds()),
                mapBigDecimalFromDto(priceDto.getMid()),
                mapBigDecimalFromDto(priceDto.getAsk()),
                mapBigDecimalFromDto(priceDto.getBid()),
                mapBigDecimalFromDto(priceDto.getSpread()),
                Instant.ofEpochSecond(priceDto.getCreationDate().getSeconds())
        );
    }

    private InboundMessageEvent mapGetHistoricalTrades(MessageEnvelope envelope) {
        GetHistoricalTradeResponseDto dto = envelope.getPayload();
        ArrayList<FxHistoricalTrade> trades = dto
                .getTradesList()
                .stream()
                .map(this::mapHistoricalTrade).collect(Collectors.toCollection(ArrayList::new));
        return new GetHistoricalTradesResponseEvent(trades);
    }

    private InboundMessageEvent mapGetStartingPrices(MessageEnvelope envelope) {
        GetStartingPricesResponseDto dto = envelope.getPayload();
        Map<String, FxHistoricalSpotPrice> startPricesBySymbol = dto
                .getStartPricesList()
                .stream()
                .map(this::mapHistoricalPrice)
                .collect(Collectors.toMap(FxHistoricalSpotPrice::getSymbol, (p) -> p));
        return new GetStartingPricesResponseEvent(Collections.unmodifiableMap(startPricesBySymbol));
    }

    private FxHistoricalTrade mapHistoricalTrade(FxHistoricalTradeDto tradeDto) {
        return new FxHistoricalTrade(
                tradeDto.getTradeId(),
                tradeDto.getCurrencyPair(),
                mapBigDecimalFromDto(tradeDto.getNotional()),
                tradeDto.getSide() == SideDto.BUY ? Side.Buy : Side.Sell,
                mapBigDecimalFromDto(tradeDto.getExecutedRate()),
                Instant.ofEpochSecond(tradeDto.getExecutedAt().getSeconds()),
                mapHistoricalPrice(tradeDto.getPrice())
        );
    }
}
