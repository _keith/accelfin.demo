package com.accelfin.server.demo.analytics.model;

import com.accelfin.server.Clock;
import com.accelfin.server.ServiceModelBase;
import com.accelfin.server.demo.analytics.model.events.operationEvents.UpdateReportSubscriptionRequestOperationEvent;
import com.accelfin.server.demo.analytics.model.reports.Reports;
import com.accelfin.server.messaging.Connections;
import com.accelfin.server.messaging.events.InitEvent;
import com.esp.EventContext;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AnalyticsService extends ServiceModelBase {

    private static Logger logger = LoggerFactory.getLogger(AnalyticsService.class);

    private Router<AnalyticsService> _router;
    private Reports _reports;

    @Inject
    public AnalyticsService(
            Router<AnalyticsService> router,
            Reports reports,
            Connections connections,
            Clock clock
    ) {
        super(clock, connections);
        _router = router;
        _reports = reports;
    }

    public Reports getReports() {
        return _reports;
    }

    @Override
    public void observeEvents() {
        super.observeEvents();
        addDisposable(_router.observeEventsOn(this));
        _reports.observeEvents();
    }

    @ObserveEvent(eventClass = UpdateReportSubscriptionRequestOperationEvent.class)
    public void onUpdateReportRequestOperationEvent(UpdateReportSubscriptionRequestOperationEvent event, EventContext context) {
        logger.debug("Processing report update request");
    }

    @ObserveEvent(eventClass = InitEvent.class)
    public void onInitEvent() {
        logger.info("Init event received. Connecting Mq");
        getConnections().connectAll();
    }
}
