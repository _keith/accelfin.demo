package com.accelfin.server.demo.analytics.model.events.operationEvents;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.analytics.model.reports.FxHistoricalSpotPrice;

import java.util.ArrayList;

public class GetHistoricalPricesResponseEvent extends InboundMessageEvent {
    private ArrayList<FxHistoricalSpotPrice> _prices;

    public GetHistoricalPricesResponseEvent(ArrayList<FxHistoricalSpotPrice> prices) {
        _prices = prices;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Unauthenticated;
    }

    public ArrayList<FxHistoricalSpotPrice> getPrices() {
        return _prices;
    }
}

