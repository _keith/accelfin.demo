package com.accelfin.server.demo.fx.model.cash.pricing;

public enum  TemporalUnit {
    Seconds,
    Minute,
    Hour
}