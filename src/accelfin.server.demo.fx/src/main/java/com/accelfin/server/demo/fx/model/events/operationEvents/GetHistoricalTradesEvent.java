package com.accelfin.server.demo.fx.model.events.operationEvents;


import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;

import java.time.Instant;

public class GetHistoricalTradesEvent extends InboundMessageEvent {

    private Instant _start;
    private Instant _end;

    public GetHistoricalTradesEvent(Instant start, Instant end) {
        _start = start;
        _end = end;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Unauthenticated;
    }

    public Instant getStart() {
        return _start;
    }

    public Instant getEnd() {
        return _end;
    }
}
