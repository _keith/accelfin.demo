package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderRequestDto;
import com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderResponseDto;
import com.accelfin.server.demo.fx.model.events.operationEvents.ExecuteSimpleOrderOperationEvent;

import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapBigDecimalFromDto;

public class OrderExecutionMapper {

    public InboundMessageEvent mapExecuteOrderOperationEvent(MessageEnvelope envelope) {
        ExecuteSimpleOrderRequestDto dto = envelope.getPayload();
        return new ExecuteSimpleOrderOperationEvent(
                MinorTypesMapper.mapSideFromDto(dto.getSide()),
                MinorTypesMapper.mapOrderTypeFromDto(dto.getType()),
                mapBigDecimalFromDto(dto.getNotional()),
                mapBigDecimalFromDto(dto.getRate()),
                dto.getCurrencyPair()
        );
    }

    public ExecuteSimpleOrderResponseDto mapExecuteSimpleOrderSuccess() {
        return ExecuteSimpleOrderResponseDto.newBuilder()
                .setIsSuccess(true)
                .build();
    }

    public ExecuteSimpleOrderResponseDto mapExecuteSimpleOrderFailure(String errorMessage) {
        return ExecuteSimpleOrderResponseDto.newBuilder()
                .setErrorMessage(errorMessage)
                .build();
    }
}
