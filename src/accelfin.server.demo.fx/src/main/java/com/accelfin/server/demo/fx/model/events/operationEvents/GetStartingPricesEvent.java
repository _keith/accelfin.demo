package com.accelfin.server.demo.fx.model.events.operationEvents;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;

import java.time.Instant;

public class GetStartingPricesEvent extends InboundMessageEvent {

    private Instant _start;

    public GetStartingPricesEvent(Instant start) {
        _start = start;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Unauthenticated;
    }

    public Instant getStart() {
        return _start;
    }
}
