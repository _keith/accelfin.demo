package com.accelfin.server.demo.fx.model.session;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.Clock;
import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.SessionToken;
import com.accelfin.server.messaging.events.ClockTickEvent;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.messaging.events.InitEvent;

import com.accelfin.server.demo.fx.dtos.LoginResponseDto;
import com.accelfin.server.demo.fx.dtos.mappers.SessionMapper;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.User;
import com.accelfin.server.demo.fx.model.events.SessionsExpiredEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.ClientHeartbeatOperationEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.LoginOperationEvent;
import com.accelfin.server.demo.fx.model.staticData.ReferenceData;
import com.accelfin.server.demo.FxOperationNameConst;
import com.esp.EventContext;
import com.esp.ObservationStage;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Sessions extends DisposableBase {
    private static Logger logger = LoggerFactory.getLogger(Sessions.class);
    private static final int SESSION_TIMEOUT_CHECK_MS = 5000; // check session every 5sec

    private HashMap<SessionToken, Session> _sessionsBySessionToken = new HashMap<>();
    private HashMap<String, ArrayList<Session>> _sessionsByUserId = new HashMap<>();
    private Router<FxService> _router;
    private ConnectionBroker _connectionBroker;
    private ReferenceData _referenceData;
    private SessionMapper _sessionMapper;
    private Clock _clock;
    private Instant _sessionTimeoutLastCheckTime;

    @Inject
    public Sessions(
            Router<FxService> router,
            ConnectionBroker connectionBroker,
            ReferenceData referenceData,
            SessionMapper sessionMapper,
            Clock clock
    ) {
        _router = router;
        _connectionBroker = connectionBroker;
        _referenceData = referenceData;
        _sessionMapper = sessionMapper;
        _clock = clock;
        _sessionTimeoutLastCheckTime = _clock.now();
    }

    public User getUserForSession(SessionToken sessionToken) {
        Session session = _sessionsBySessionToken.get(sessionToken);
        return session.getUser();
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    @ObserveEvent(eventClass = InitEvent.class)
    public void onInitEvent() {
        _connectionBroker.setOperationStatus(FxOperationNameConst.Login, true);
        _connectionBroker.setOperationStatus(FxOperationNameConst.ClientHeartbeat, true);
    }

    @ObserveEvent(eventClass = InboundMessageEvent.class, stage = ObservationStage.Preview)
    public void setMessageAuthenticationStatus(InboundMessageEvent event, EventContext context) {
        if(_sessionsBySessionToken.containsKey(event.getSessionToken())) {
            event.setAuthenticationStatus(AuthenticationStatus.Authenticated);
        }
        if (event.getRequiredAuthenticationStatus() != event.getAuthenticationStatus()) {
            logger.warn("Canceling event as it requires authentication but the request can't be authenticated. Event details: [{}]", event);
            context.cancel();
        }
    }

    // TODO disconnects, check out https://github.com/rabbitmq/rabbitmq-event-exchange, or user http://www.rabbitmq.com/blog/2011/02/10/introducing-publisher-confirms/
    @ObserveEvent(eventClass = LoginOperationEvent.class)
    public void observeLoginOperation(LoginOperationEvent event) {
        LoginResponseDto result;
        if (_referenceData.hasUser(event.getUserName())) {
            User user = _referenceData.getUser(event.getUserName());
            // this is special case where the SessionToken on the incomming event isn't
            // fully populated (as the users isn't yet logged in).
            // we need to fix that here
            // TODO we shouldn't just rely on the users client session id, it may not be unique enough
            SessionToken token = new SessionToken(event.getClientSessionId(), event.getSessionToken().getConnectionId());
            // reset the token now we have logged in
            event.setSessionToken(token);

            // if it's a re-login we should already have a reference to the client token id
            boolean isReLogin = _sessionsBySessionToken.containsKey(token);
            if (isReLogin) {
                logger.debug("Client logging in with token [{}] (RE-LOGIN)", token);
            } else {
                logger.debug("Client logging in with token  id [{}] (NEW-LOGIN)", token);
                Session session = new Session(user, token, _clock);
                ArrayList<Session> sessions = _sessionsByUserId.computeIfAbsent(event.getUserName(), username -> new ArrayList<>());
                sessions.add(session);
                _sessionsBySessionToken.put(token, session);
            }
            result = _sessionMapper.mapLoginSuccessReponseDto(token.getSessionId(), user);
        } else {
            logger.debug("Login failure. Username [{}] not known");
            result = _sessionMapper.mapFailedLoginReponseDto();
        }
        _connectionBroker.sendRpcResponse(event, result);
    }

    @ObserveEvent(eventClass = ClientHeartbeatOperationEvent.class)
    public void onHeartbeatOperationEvent(ClientHeartbeatOperationEvent event) {
        SessionToken token = event.getSessionToken();
        if (_sessionsBySessionToken.containsKey(token)) {
            Session session = _sessionsBySessionToken.get(token);
            session.updateLastSeenTime();
            logger.debug("[{}:{}] has sent a heartbeat", session.getUser().getUserName(), token);
            _connectionBroker.sendRpcResponse(
                    event,
                    _sessionMapper.mapClientHeartbeatResponse()
            );
        } else {
            logger.debug("Ignoring heartbeat for session [{}], session token not found", event.getSessionToken());
        }
    }

    @ObserveEvent(eventClass = ClockTickEvent.class)
    public void onClockTickEvent() {
        boolean shouldCheck = _sessionTimeoutLastCheckTime
                .plus(Duration.ofMillis(SESSION_TIMEOUT_CHECK_MS))
                .isBefore(_clock.now());
        if (shouldCheck) {
            ArrayList<Session> expiredSessions = new ArrayList<>();
            Iterator<Map.Entry<SessionToken, Session>> iter = _sessionsBySessionToken.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<SessionToken, Session> entry = iter.next();
                Session session = entry.getValue();
                session.updateHasExpired();
                if (session.getHasExpired()) {
                    SessionToken token = entry.getKey();
                    logger.debug("[{}:{}] session has expired", session.getUser().getUserName(), token);
                    iter.remove();
                    ArrayList<Session> usersSessions = _sessionsByUserId.get(session.getUser().getUserName());
                    usersSessions.remove(session);
                    expiredSessions.add(session);
                }
            }
            if(expiredSessions.size() > 0) {
                _router.executeEvent(new SessionsExpiredEvent(expiredSessions.toArray(new Session[expiredSessions.size()])));
            }
            _sessionTimeoutLastCheckTime = _clock.now();
        }
    }
}