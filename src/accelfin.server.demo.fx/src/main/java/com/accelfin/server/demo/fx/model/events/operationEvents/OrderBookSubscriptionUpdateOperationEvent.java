package com.accelfin.server.demo.fx.model.events.operationEvents;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;

public class OrderBookSubscriptionUpdateOperationEvent extends InboundMessageEvent {
    private String[] _symbols;

    public OrderBookSubscriptionUpdateOperationEvent(String... symbols) {
        _symbols = symbols;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Authenticated;
    }

    public String[] getSymbols() {
        return _symbols;
    }
}
