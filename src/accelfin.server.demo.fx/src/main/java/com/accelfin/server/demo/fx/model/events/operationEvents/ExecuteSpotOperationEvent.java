package com.accelfin.server.demo.fx.model.events.operationEvents;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.common.model.Side;

import java.math.BigDecimal;

public class ExecuteSpotOperationEvent extends InboundMessageEvent {

    private String _symbol;
    private Side _side;
    private BigDecimal _amount;
    private String _priceId;
    private BigDecimal _price;
    private boolean _isStateOfTheWorld;

    public ExecuteSpotOperationEvent(
            String symbol,
            Side side,
            BigDecimal amount,
            String priceId,
            BigDecimal price,
            boolean isStateOfTheWorld
    ) {
        _symbol = symbol;
        _side = side;
        _amount = amount;
        _priceId = priceId;
        _price = price;
        _isStateOfTheWorld = isStateOfTheWorld;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Authenticated;
    }

    public String getSymbol() {
        return _symbol;
    }

    public Side getSide() {
        return _side;
    }

    public BigDecimal getAmount() {
        return _amount;
    }

    public String getPriceId() {
        return _priceId;
    }

    public BigDecimal getPrice() {
        return _price;
    }

    public boolean getIsStateOfTheWorld() {
        return _isStateOfTheWorld;
    }
}
