package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.fx.dtos.FxSpotPriceDto;
import com.accelfin.server.demo.fx.dtos.PriceSubscriptionUpdateRequestDto;
import com.accelfin.server.demo.fx.dtos.PriceSubscriptionUpdateResponseDto;
import com.accelfin.server.demo.fx.dtos.PriceUpdateResponseDto;
import com.accelfin.server.demo.fx.model.events.operationEvents.PriceSubscriptionUpdateOperationEvent;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;

import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapBigDecimalToDto;
import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapInstantToDto;

public class PriceMapper {
    public PriceSubscriptionUpdateResponseDto mapSubscriptionSuccessResponse() {
        return PriceSubscriptionUpdateResponseDto.newBuilder()
                .setIsSuccess(true)
                .build();
    }

    public InboundMessageEvent mapPriceSubscriptionUpdateOperationEvent(MessageEnvelope envelope) {
        PriceSubscriptionUpdateRequestDto dto = envelope.getPayload();
        return new PriceSubscriptionUpdateOperationEvent(
                dto.getSymbolsList().stream().toArray(String[]::new)
        );
    }

    public PriceUpdateResponseDto mapPriceUpdateResponseDto(FxSpotPriceEvent price) {
        return PriceUpdateResponseDto.newBuilder()
                .setPrice(mapToDto(price))
                .build();
    }

    public static FxSpotPriceDto mapToDto(FxSpotPriceEvent price) {
        return FxSpotPriceDto.newBuilder()
                .setId(price.getId())
                .setSymbol(price.getSymbol())
                .setSpotDate(mapInstantToDto(price.getSpotDate()))
                .setMid(mapBigDecimalToDto(price.getMid()))
                .setAsk(mapBigDecimalToDto(price.getAsk()))
                .setBid(mapBigDecimalToDto(price.getBid()))
                .setSpread(mapBigDecimalToDto(price.getSpread()))
                .setCreationDate(mapInstantToDto(price.getCreationDate()))
                .setIsTradable(price.getIsTradable())
                .build();
    }
}
