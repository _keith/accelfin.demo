package com.accelfin.server.demo.fx.model.events;

import com.accelfin.server.demo.fx.model.User;

public class UsersResolvedEvent {
    private User[] _users;

    public UsersResolvedEvent(User... users) {
        _users = users;
    }

    public User[] getUsers() {
        return _users;
    }
}
