package com.accelfin.server.demo.fx.model.events;

import com.accelfin.server.demo.common.model.CurrencyPair;

public class CurrencyPairsResolvedEvent {
    private CurrencyPair[] _pairs;

    public CurrencyPairsResolvedEvent(CurrencyPair... pairs) {
        _pairs = pairs;
    }

    public CurrencyPair[] getPairs() {
        return _pairs;
    }
}
