package com.accelfin.server.demo.fx.model.events.operationEvents;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;

import java.time.Instant;

public class ClientHeartbeatOperationEvent extends InboundMessageEvent {
    private Instant _timeSent;

    public ClientHeartbeatOperationEvent(Instant timeSent) {
        _timeSent = timeSent;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Authenticated;
    }

    public Instant getTimeSent() {
        return _timeSent;
    }
}