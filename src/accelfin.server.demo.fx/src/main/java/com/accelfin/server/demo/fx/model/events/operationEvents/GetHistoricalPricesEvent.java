package com.accelfin.server.demo.fx.model.events.operationEvents;


import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.fx.model.cash.pricing.InterpolationSettings;

import java.time.Instant;

public class GetHistoricalPricesEvent extends InboundMessageEvent {

    private String[] _symbols;
    private Instant _start;
    private Instant _end;
    private InterpolationSettings _interpolationSettings;

    public GetHistoricalPricesEvent(String[] symbols, Instant start, Instant end, InterpolationSettings interpolationSettings) {
        _symbols = symbols;
        _start = start;
        _end = end;
        _interpolationSettings = interpolationSettings;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Unauthenticated;
    }

    public String[] getSymbols() {
        return _symbols;
    }

    public Instant getStart() {
        return _start;
    }

    public Instant getEnd() {
        return _end;
    }

    public InterpolationSettings getInterpolationSettings() {
        return _interpolationSettings;
    }
}
