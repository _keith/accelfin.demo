package com.accelfin.server.demo.fx.model.events.operationEvents;


import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;

public class PriceSubscriptionUpdateOperationEvent extends InboundMessageEvent {

    public PriceSubscriptionUpdateOperationEvent(String[] symbols) {
        _symbols = symbols;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Authenticated;
    }

    private String[] _symbols;

    public String[] getSymbols() {
        return _symbols;
    }
}
