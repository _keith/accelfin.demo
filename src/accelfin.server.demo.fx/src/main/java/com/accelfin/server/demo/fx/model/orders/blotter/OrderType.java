package com.accelfin.server.demo.fx.model.orders.blotter;

public enum OrderType {
    TAKE_PROFIT,
    STOP_LOSS,
    LIMIT
}
