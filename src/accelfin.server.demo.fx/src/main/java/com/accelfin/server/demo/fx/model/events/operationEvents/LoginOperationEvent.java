package com.accelfin.server.demo.fx.model.events.operationEvents;

import com.accelfin.server.AuthenticationStatus;
import com.accelfin.server.messaging.events.InboundMessageEvent;

public class LoginOperationEvent extends InboundMessageEvent {
    private String _userName;
    private String _password;
    private String _clientSessionId;

    public  LoginOperationEvent(String userName, String password, String clientSessionId) {
        _userName = userName;
        _password = password;
        _clientSessionId = clientSessionId;
    }

    @Override
    public AuthenticationStatus getRequiredAuthenticationStatus() {
        return AuthenticationStatus.Unauthenticated;
    }

    public String getUserName() {
        return _userName;
    }

    public void setUserName(String userName) {
        _userName = userName;
    }

    public String getPassword() {
        return _password;
    }

    public void setPassword(String password) {
        _password = password;
    }

    public String getClientSessionId() {
        return _clientSessionId;
    }

    public void setClientSessionId(String clientSessionId) {
        _clientSessionId = clientSessionId;
    }
}
