package com.accelfin.server.demo.fx.model.fakeData;

import java.math.BigDecimal;

public class BidAndAsk {
    private BigDecimal _bid;
    private BigDecimal _ask;

    public BidAndAsk(BigDecimal bid, BigDecimal ask) {
        _bid = bid;
        _ask = ask;
    }

    public BigDecimal getBid() {
        return _bid;
    }

    public BigDecimal getAsk() {
        return _ask;
    }

    @Override
    public String toString() {
        return "BidAndAsk{" +
                "_bid=" + _bid +
                ", _ask=" + _ask +
                '}';
    }
}
