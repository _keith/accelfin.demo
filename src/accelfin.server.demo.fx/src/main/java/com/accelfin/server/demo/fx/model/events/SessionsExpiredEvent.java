package com.accelfin.server.demo.fx.model.events;

import com.accelfin.server.SessionToken;
import com.accelfin.server.demo.fx.model.session.Session;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SessionsExpiredEvent {
    private final Set<SessionToken> _sessionTokens;
    private final Session[] _sessions;

    public SessionsExpiredEvent(Session[] sessions) {
        _sessions = sessions;
        List<SessionToken> sessionIds = Arrays.stream(sessions)
                .map(Session::getSessionToken)
                .collect(Collectors.toList());
        _sessionTokens = new HashSet<>(sessionIds);
    }

    public Session[] getSessions() {
        return _sessions;
    }

    public Set<SessionToken> getSessionTokens() {
        return _sessionTokens;
    }
}
