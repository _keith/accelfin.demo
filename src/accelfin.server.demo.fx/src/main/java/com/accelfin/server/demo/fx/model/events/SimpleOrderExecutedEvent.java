package com.accelfin.server.demo.fx.model.events;

import com.accelfin.server.demo.fx.model.orders.blotter.SimpleOrder;

public class SimpleOrderExecutedEvent extends TradeExecutedEventBase<SimpleOrder> {
    public SimpleOrderExecutedEvent(SimpleOrder simpleOrder) {
        super(simpleOrder);
    }
}
