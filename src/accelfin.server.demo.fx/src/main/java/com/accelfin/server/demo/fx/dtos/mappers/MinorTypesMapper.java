package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.dtos.*;
import com.accelfin.server.demo.fx.model.orders.blotter.OrderStatus;
import com.accelfin.server.demo.fx.model.orders.blotter.OrderType;
import com.accelfin.server.demo.fx.model.orders.execution.OrderBookRungType;
import com.accelfin.server.demo.fx.model.cash.pricing.TemporalUnit;

public class MinorTypesMapper {
    public static SideDto mapSideToDto(Side side) {
        switch (side) {
            case Buy:
                return SideDto.BUY;
            case Sell:
                return SideDto.SELL;
        }
        throw new RuntimeException("Unknown side " + side);
    }

    public static Side mapSideFromDto(SideDto dto) {
        switch (dto) {
            case BUY:
                return Side.Buy;
            case SELL:
                return Side.Sell;
            default:
                throw new RuntimeException("Unknown side " + dto);
        }
    }

    public static OrderTypeDto mapOrderTypeToDto(OrderType side) {
        switch (side) {
            case LIMIT:
                return OrderTypeDto.LIMIT;
            case TAKE_PROFIT:
                return OrderTypeDto.TAKE_PROFIT;
            case STOP_LOSS:
                return OrderTypeDto.STOP_LOSS;
        }
        throw new RuntimeException("Unknown OrderType " + side);
    }

    public static OrderType mapOrderTypeFromDto(OrderTypeDto side) {
        switch (side) {
            case LIMIT:
                return OrderType.LIMIT;
            case TAKE_PROFIT:
                return OrderType.TAKE_PROFIT;
            case STOP_LOSS:
                return OrderType.STOP_LOSS;
        }
        throw new RuntimeException("Unknown OrderTypeDto " + side);
    }


    public static OrderStatusDto mapOrderStatusToDto(OrderStatus status) {
        switch (status) {
            case ACTIVE:
                return OrderStatusDto.ACTIVE;
            case EXECUTED:
                return OrderStatusDto.EXECUTED;
            case CANCELED:
                return OrderStatusDto.CANCELED;
        }
        throw new RuntimeException("Unknown OrderStatus " + status);
    }

    public static TemporalUnit mapTemporalUnitFromDto(TemporalUnitDto dto) {
        switch (dto) {
            case SECONDS:
                return TemporalUnit.Seconds;
            case MINUTE:
                return TemporalUnit.Minute;
            case HOUR:
                return TemporalUnit.Hour;
            default:
                throw new RuntimeException("Unknown TemporalUnit " + dto);
        }
    }

    public static OrderBookRungDto.OrderBookRungTypeDto mapOrderBookRungTypeDtoToDto(OrderBookRungType type) {
        switch (type) {
            case BUY:
                return OrderBookRungDto.OrderBookRungTypeDto.BUY;
            case SELL:
                return OrderBookRungDto.OrderBookRungTypeDto.SELL;
            case MID:
                return OrderBookRungDto.OrderBookRungTypeDto.MID;
        }
        throw new RuntimeException("Unknown OrderBookRungType " + type);
    }
}
