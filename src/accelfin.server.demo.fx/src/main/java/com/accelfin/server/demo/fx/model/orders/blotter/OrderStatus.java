package com.accelfin.server.demo.fx.model.orders.blotter;

public enum OrderStatus {
    ACTIVE,
    EXECUTED, // not doing partial execution for now, if it's filled it's all of it
    CANCELED
}
