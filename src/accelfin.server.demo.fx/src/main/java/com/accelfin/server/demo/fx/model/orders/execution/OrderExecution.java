package com.accelfin.server.demo.fx.model.orders.execution;

import com.accelfin.server.Clock;
import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.messaging.events.InitEvent;
import com.accelfin.server.demo.FxOperationNameConst;
import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.fx.dtos.mappers.OrderExecutionMapper;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.User;
import com.accelfin.server.demo.fx.model.orders.blotter.OrderStatus;
import com.accelfin.server.demo.fx.model.orders.blotter.SimpleOrder;
import com.accelfin.server.demo.fx.model.events.SimpleOrderExecutedEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.ExecuteSimpleOrderOperationEvent;
import com.accelfin.server.demo.fx.model.session.Sessions;
import com.accelfin.server.demo.fx.model.staticData.ReferenceData;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.UUID;

public class OrderExecution extends DisposableBase {

    private static Logger logger = LoggerFactory.getLogger(OrderExecution.class);

    private Router<FxService> _router;
    private ConnectionBroker _connectionBroker;
    private ReferenceData _referenceData;
    private OrderExecutionMapper _orderExecutionMapper;
    private Sessions _sessions;
    private Clock _clock;

    @Inject
    public OrderExecution(
            Router<FxService> router,
            ConnectionBroker connectionBroker,
            ReferenceData referenceData,
            OrderExecutionMapper orderExecutionMapper,
            Sessions sessions,
            Clock clock
    ) {
        _router = router;
        _connectionBroker = connectionBroker;
        _referenceData = referenceData;
        _orderExecutionMapper = orderExecutionMapper;
        _sessions = sessions;
        _clock = clock;
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    @ObserveEvent(eventClass = InitEvent.class)
    public void onInitEvent() {
        _connectionBroker.setOperationStatus(FxOperationNameConst.ExecuteSimpleOrder, true);
    }

    @ObserveEvent(eventClass = ExecuteSimpleOrderOperationEvent.class)
    public void onExecuteSimpleOrderOperationEvent(ExecuteSimpleOrderOperationEvent event) {
        logger.debug("Simple order execution event received");
        User user = _sessions.getUserForSession(event.getSessionToken());
        CurrencyPair currencyPair = _referenceData.getCurrencyPair(event.getCurrencyPair());
        if(event.getSubmittedRate().scale() <= currencyPair.getRatePrecision()) {
            BigDecimal submittedRate = event.getSubmittedRate().scale() < currencyPair.getRatePrecision()
                    ? event.getSubmittedRate().setScale(currencyPair.getRatePrecision(), BigDecimal.ROUND_UNNECESSARY)
                    : event.getSubmittedRate();

            SimpleOrder simpleOrder = new SimpleOrder(
                    UUID.randomUUID().toString(),
                    currencyPair,
                    user,
                    event.getSide(),
                    event.getType(),
                    event.getNotional(),
                    submittedRate,
                    _clock.now(),
                    OrderStatus.ACTIVE,
                    null,
                    null
            );
            SimpleOrderExecutedEvent executionEvent = new SimpleOrderExecutedEvent(simpleOrder);
            _router.publishEvent(executionEvent);
            _connectionBroker.sendRpcResponse(
                    event,
                    _orderExecutionMapper.mapExecuteSimpleOrderSuccess()
            );
        } else {
            _connectionBroker.sendRpcResponse(
                    event,
                    _orderExecutionMapper.mapExecuteSimpleOrderFailure("submitted rate precision too large")
            );
        }
    }
}
