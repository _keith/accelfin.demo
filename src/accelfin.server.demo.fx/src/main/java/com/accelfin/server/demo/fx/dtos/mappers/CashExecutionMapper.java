package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.fx.dtos.ExecuteSpotRequestDto;
import com.accelfin.server.demo.fx.dtos.ExecuteSpotResponseDto;
import com.accelfin.server.demo.fx.model.events.operationEvents.ExecuteSpotOperationEvent;

import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapBigDecimalFromDto;

public class CashExecutionMapper {

    public InboundMessageEvent mapExecuteSpotOperationEvent(MessageEnvelope envelope) {
        ExecuteSpotRequestDto dto = envelope.getPayload();
        return new ExecuteSpotOperationEvent(
                dto.getSymbol(),
                MinorTypesMapper.mapSideFromDto(dto.getSide()),
                mapBigDecimalFromDto(dto.getAmount()),
                dto.getPriceId(),
                mapBigDecimalFromDto(dto.getPrice()),
                false
        );
    }

    public ExecuteSpotResponseDto mapExecuteSpotSuccess() {
        return ExecuteSpotResponseDto.newBuilder()
                .setIsSuccess(true)
                .build();
    }

    public ExecuteSpotResponseDto mapExecuteSpotFailure(String errorMessage) {
        return ExecuteSpotResponseDto.newBuilder()
                .setErrorMessage(errorMessage)
                .build();
    }
}
