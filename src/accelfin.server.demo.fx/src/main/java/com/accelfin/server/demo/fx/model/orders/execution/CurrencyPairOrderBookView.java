package com.accelfin.server.demo.fx.model.orders.execution;

import java.util.List;

public class CurrencyPairOrderBookView {
    private String _symbol;
    private List<OrderBookRung> _rungs;

    public CurrencyPairOrderBookView(String symbol, List<OrderBookRung> rungs) {
        _symbol = symbol;
        _rungs = rungs;
    }

    public String getSymbol() {
        return _symbol;
    }

    public List<OrderBookRung> getRungs() {
        return _rungs;
    }
}
