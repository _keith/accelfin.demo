package com.accelfin.server.demo.fx;

import com.accelfin.server.DefaultScheduler;
import com.accelfin.server.demo.common.ServiceBootstrapper;
import com.accelfin.server.demo.fx.container.ConnectionsProvider;
import com.accelfin.server.esp.BackingQueueRouterDispatcher;
import com.accelfin.server.messaging.events.InitEvent;
import com.accelfin.server.demo.fx.container.FxContainerModule;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.fakeData.FakeActivities;
import com.esp.DefaultRouter;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Bootstrapper extends ServiceBootstrapper {
    private static Logger logger = LoggerFactory.getLogger(Bootstrapper.class);

    public static void main(String[] args) throws Exception {
        new Bootstrapper().run();
    }

    @Override
    protected List<AbstractModule> getContainerModules() {
        ArrayList<AbstractModule> modules = new ArrayList<>();
        modules.add(
                new FxContainerModule(
                        new BackingQueueRouterDispatcher(),
                        new DefaultScheduler(),
                        ConnectionsProvider.class
                )
        );
        return modules;
    }

    @Override
    protected void startModel() {
        logger.debug("Creating model");
        Injector injector = getInjector();
        FxService model = injector.getInstance(FxService.class);

        FakeActivities fakeActivities = injector.getInstance(FakeActivities.class);
        fakeActivities.preLoadModel();

        DefaultRouter<FxService> router = injector.getInstance(Key.get(new TypeLiteral<DefaultRouter<FxService>>(){}));
        router.setModel(model);
        model.observeEvents();
        router.publishEvent(new InitEvent());
    }
}