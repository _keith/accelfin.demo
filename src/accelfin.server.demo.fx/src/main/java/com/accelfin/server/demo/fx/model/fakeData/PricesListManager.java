package com.accelfin.server.demo.fx.model.fakeData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Loads prices from the resource zip prices.zip (created using the PriceParser).
 */
public class PricesListManager {
    private static Logger logger = LoggerFactory.getLogger(PricesListManager.class);
    private static HashMap<String, PriceList> _priceListsByCcyPair = new HashMap<>();

    static {

        try {
            logger.debug("Loading properties");
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            ZipInputStream zipStream = new ZipInputStream(classLoader.getResourceAsStream("prices.zip"));

            ZipEntry entry = null;
            int count = 0;
            while ((entry = zipStream.getNextEntry()) != null) {
                logger.info("Processing file {}, size {} ", entry.getName(), entry.getSize());
                PriceList priceList = new PriceList();
                Scanner sc = new Scanner(zipStream);
                while (sc.hasNextLine()) {
                    count++;
                    String[] bidAndAsk = sc.nextLine().split(",");
                    priceList.add(new BigDecimal(bidAndAsk[0]), new BigDecimal(bidAndAsk[1]));
                }
                String currencyPair = entry.getName().substring(0, 6);
                _priceListsByCcyPair.put(currencyPair, priceList);
            }

        } catch (Exception e) {
            logger.error("Error loading prices", e);
            throw new RuntimeException("Could not load prices");
        }
    }

    public static PriceList getPriceList(String currencyPair) {
        return _priceListsByCcyPair.get(currencyPair);
    }
}
