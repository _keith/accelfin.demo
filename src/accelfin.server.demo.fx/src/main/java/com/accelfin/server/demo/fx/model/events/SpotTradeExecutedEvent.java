package com.accelfin.server.demo.fx.model.events;

import com.accelfin.server.demo.fx.model.cash.blotter.SpotTrade;

public class SpotTradeExecutedEvent extends TradeExecutedEventBase<SpotTrade> {
    public SpotTradeExecutedEvent(SpotTrade spotTrade) {
        super(spotTrade);
    }
}
