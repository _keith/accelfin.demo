package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.messaging.mappers.CommonTypesMapper;
import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.fx.dtos.*;
import com.accelfin.server.demo.fx.model.events.operationEvents.OrderBookSubscriptionUpdateOperationEvent;
import com.accelfin.server.demo.fx.model.orders.execution.CurrencyPairOrderBookView;
import com.accelfin.server.demo.fx.model.orders.execution.OrderBookRung;
import com.google.protobuf.Message;

import java.util.List;
import java.util.stream.Collectors;

public class OrderBookMapper {

    public InboundMessageEvent mapOrderBookSubscriptionUpdateRequestDtoToOperationEvent(MessageEnvelope envelope) {
        OrderBookSubscriptionUpdateRequestDto dto = envelope.getPayload();
        return new OrderBookSubscriptionUpdateOperationEvent(
                dto.getSymbolsList().stream().toArray(String[]::new)
        );
    }

    public Message mapToOrderBookSubscriptionUpdateResponseDto(boolean success) {
        return OrderBookSubscriptionUpdateResponseDto
                .newBuilder()
                .setIsSuccess(success)
                .build();
    }

    public Message mapToOrderBookUpdateResponseDto(CurrencyPairOrderBookView currencyPairOrderBookView) {
        return OrderBookUpdateResponseDto.newBuilder()
                .setCurrencyPair(currencyPairOrderBookView.getSymbol())
                .addAllRungs(mapRungsToDto(currencyPairOrderBookView.getRungs()))
                .build();
    }

    private List<OrderBookRungDto> mapRungsToDto(List<OrderBookRung> rungs) {
        return rungs.stream().map(rung -> OrderBookRungDto
                    .newBuilder()
                    .setMarketSizeNotional(CommonTypesMapper.mapBigDecimalToDto(rung.getMarketSizeNotional()))
                    .setRate(CommonTypesMapper.mapBigDecimalToDto(rung.getRate()))
                    .setType(MinorTypesMapper.mapOrderBookRungTypeDtoToDto(rung.getType()))
                    .build())
                    .collect(Collectors.toList());
    }
}
