package com.accelfin.server.demo.fx.model.common;

import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.SessionToken;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.messaging.events.InitEvent;
import com.accelfin.server.demo.fx.dtos.mappers.BlotterMapper;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.events.SessionsExpiredEvent;
import com.accelfin.server.demo.fx.model.events.TradeExecutedEventBase;
import com.accelfin.server.demo.fx.model.session.Session;
import com.esp.EventContext;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;
import com.google.inject.Inject;
import com.google.protobuf.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public abstract class BlotterBase<TTrade extends TradeBase> extends DisposableBase {

    private static Logger logger = LoggerFactory.getLogger(BlotterBase.class);

    private HashSet<SessionToken> _blotterSubscriptions = new HashSet<>();

    private Router<FxService> _router;
    private ConnectionBroker _connectionBroker;
    private TradeRepository<TTrade> _tradeRepository;
    private BlotterMapper<TTrade> _blotterMapper;

    @Inject
    public BlotterBase(
            Router<FxService> router,
            ConnectionBroker connectionBroker,
            TradeRepository<TTrade> tradeRepository,
            BlotterMapper<TTrade> blotterMapper
    ) {
        _router = router;
        _connectionBroker = connectionBroker;
        _tradeRepository = tradeRepository;
        _blotterMapper = blotterMapper;
    }

    public final HashSet<SessionToken> getBlotterSubscriptions() {
        return _blotterSubscriptions;
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    @ObserveEvent(eventClass = InitEvent.class)
    public void onInitEvent() {
        setOperationStatus();
    }

    protected abstract void setOperationStatus();

    protected abstract String getBlotterOperationName();

    protected void processSubscriptionEvent(InboundMessageEvent event) {
        logger.debug("Blotter subscribe event received for [{}]. Sending state of the world of [{}] trades", event.getSessionToken(), _tradeRepository.getNumberOfTrades());
        _blotterSubscriptions.add(event.getSessionToken());
        _connectionBroker.sendRpcResponse(
                event,
                _blotterMapper.mapToSubscriptionResponseDto(true)
        );
        if (_tradeRepository.getNumberOfTrades() > 0) {
            dispatchUpdate(
                    BlotterUpdateType.StateOfTheWorld,
                    _tradeRepository.getAllTrades(),
                    event.getSessionToken()
            );
        }
    }

    protected void onTradeExecutionEvent(TradeExecutedEventBase<TTrade> event, EventContext eventContext) {
        TTrade trade = event.getTrade();
        // seems to make sense for the blotter to add the trade to the trade repository.
        // This is because the event that actually books the trade (via the Execution entities) is a user based event
        // this is more an after the fact event, i.e. trade is done, not please create me a trade
        _tradeRepository.add(trade);
        if (_blotterSubscriptions.size() > 0) {
            logger.debug("New trade dispatching to [{}] subscribers", _blotterSubscriptions.size());

            SessionToken[] subscribers = _blotterSubscriptions.toArray(new SessionToken[_blotterSubscriptions.size()]);
            dispatchUpdate(
                    BlotterUpdateType.Normal,
                    Collections.singletonList(trade),
                    subscribers
            );
        } else {
            logger.trace("Trade received. Ignoring as there are no blotter subscribers");
        }
        eventContext.commit();
    }

    @ObserveEvent(eventClass = SessionsExpiredEvent.class)
    public void onSessionsExpiredEvent(SessionsExpiredEvent event) {
        for (Session session : event.getSessions()) {
            if (_blotterSubscriptions.contains(session.getSessionToken())) {
                _blotterSubscriptions.remove(session.getSessionToken());
                logger.debug("Session [{}] expired removing from blotter.", session.getSessionToken());
            }
        }
    }

    protected void dispatchUpdate(
            BlotterUpdateType updateType,
            List<TTrade> trades,
            SessionToken... subscribers
    ) {

        Message responseDto = _blotterMapper.mapToResponseDto(updateType, trades);
        _connectionBroker.sendStreamResponseMessage(
                getBlotterOperationName(),
                responseDto,
                false,
                subscribers
        );
    }
}
