package com.accelfin.server.demo.fx.model.cash.pricing;

public class InterpolationSettings {
    private int _maxPointsPerUnit;
    private TemporalUnit _temporalUnit;

    public InterpolationSettings() {
    }

    public InterpolationSettings(int maxPointsPerUnit, TemporalUnit temporalUnit) {
        _maxPointsPerUnit = maxPointsPerUnit;
        _temporalUnit = temporalUnit;
    }

    public int getMaxPointsPerUnit() {
        return _maxPointsPerUnit;
    }

    public void setMaxPointsPerUnit(int maxPointsPerUnit) {
        _maxPointsPerUnit = maxPointsPerUnit;
    }

    public TemporalUnit getTemporalUnit() {
        return _temporalUnit;
    }

    public void setTemporalUnit(TemporalUnit temporalUnit) {
        _temporalUnit = temporalUnit;
    }
}