package com.accelfin.server.demo.fx.model.staticData;

import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.fx.gateways.ReferenceDataGateway;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.User;
import com.accelfin.server.demo.fx.model.events.CurrencyPairsResolvedEvent;
import com.accelfin.server.demo.fx.model.events.UsersResolvedEvent;
import com.esp.EventContext;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class ReferenceData extends DisposableBase {
    private static Logger logger = LoggerFactory.getLogger(ReferenceData.class);

    private Router<FxService> _router;
    private ReferenceDataGateway _referenceDataGateway;
    private CurrencyPair[] _currencyPairs;
    private HashMap<String, User> _users = new HashMap<>();
    private Map<String, CurrencyPair> _currencyPairsBySymbol;

    @Inject
    public ReferenceData(
            Router<FxService> router,
            ReferenceDataGateway referenceDataGateway) {
        _router = router;
        _referenceDataGateway = referenceDataGateway;
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    public CurrencyPair[] currencyPairs() {
        return _currencyPairs;
    }

    public boolean hasUser(String userName) {
        return _users.containsKey(userName);
    }

    public User getUser(String userName) {
        return _users.get(userName);
    }

    public List<User> getUsers() {
        return new ArrayList<>(_users.values());
    }

    public CurrencyPair getCurrencyPair(String symbol) {
        return _currencyPairsBySymbol.get(symbol);
    }

    public void beginLoadData() {
        _referenceDataGateway.beginGetUsers();
        _referenceDataGateway.beginGetCurrencyPairs();
    }

    @ObserveEvent(eventClass = CurrencyPairsResolvedEvent.class)
    public void onCurrencyPairsResolvedEvent(CurrencyPairsResolvedEvent event, EventContext context) {
        _currencyPairs = event.getPairs();
        _currencyPairsBySymbol = Arrays
                .stream(_currencyPairs)
                .collect(Collectors.toMap(CurrencyPair::getSymbol, (p) -> p));
        context.commit();
    }

    @ObserveEvent(eventClass = UsersResolvedEvent.class)
    public void onUsersResolvedEvent(UsersResolvedEvent event, EventContext context) {
        for (User user : event.getUsers()) {
            _users.putIfAbsent(user.getUserName(), user);
        }
        context.commit();
    }
}
