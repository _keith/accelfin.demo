package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.messaging.mappers.CommonTypesMapper;
import com.accelfin.server.demo.fx.dtos.OrderDto;
import com.accelfin.server.demo.fx.model.orders.blotter.SimpleOrder;

import java.util.Arrays;

public class OrderMapper {

    public OrderDto[] mapToDtos(SimpleOrder[] trades) {
        OrderDto[] tradDtos = Arrays.stream(trades)
                .map(this::mapToDto)
                .toArray(OrderDto[]::new);
        return tradDtos;
    }

    public OrderDto mapToDto(SimpleOrder trade) {
        OrderDto.Builder builder = OrderDto.newBuilder()
                .setOrderId(trade.getTradeId())
                .setCurrencyPair(trade.getCurrencyPair().getSymbol())
                .setUserName(trade.getUser().getUserName())
                .setSide(MinorTypesMapper.mapSideToDto(trade.getSide()))
                .setType(MinorTypesMapper.mapOrderTypeToDto(trade.getType()))
                .setNotional(CommonTypesMapper.mapBigDecimalToDto(trade.getNotional()))
                .setSubmittedRate(CommonTypesMapper.mapBigDecimalToDto(trade.getSubmittedRate()))
                .setSubmittedAt(CommonTypesMapper.mapInstantToDto(trade.getSubmittedAt()))
                .setStatus(MinorTypesMapper.mapOrderStatusToDto(trade.getStatus()));
        if (trade.getExecutedRate() != null) {
            builder.setExecutedRate(CommonTypesMapper.mapBigDecimalToDto(trade.getExecutedRate()));
        }
        if (trade.getExecutedRate() != null) {
            builder.setExecutedAt(CommonTypesMapper.mapInstantToDto(trade.getSubmittedAt()));
        }
        return builder.build();
    }
}
