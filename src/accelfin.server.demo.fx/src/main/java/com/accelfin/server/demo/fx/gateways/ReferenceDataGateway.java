package com.accelfin.server.demo.fx.gateways;

import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.events.CurrencyPairsResolvedEvent;
import com.accelfin.server.demo.fx.model.events.UsersResolvedEvent;
import com.accelfin.server.demo.fx.model.fakeData.CurrencyPairsMetadata;
import com.accelfin.server.demo.fx.model.fakeData.FakeUsers;
import com.esp.Router;
import com.google.inject.Inject;

public class ReferenceDataGateway {

    private Router<FxService> _router;

    @Inject
    public ReferenceDataGateway(Router<FxService> router) {
        _router = router;
    }

    public void beginGetCurrencyPairs() {
        _router.publishEvent(new CurrencyPairsResolvedEvent(CurrencyPairsMetadata.Instance.getPairs()));
    }

    public void beginGetUsers() {
        _router.publishEvent(new UsersResolvedEvent(FakeUsers.Instance.getUsers()));
    }
}
