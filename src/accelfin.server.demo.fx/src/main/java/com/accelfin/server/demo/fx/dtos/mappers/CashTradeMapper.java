package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.messaging.mappers.CommonTypesMapper;
import com.accelfin.server.demo.fx.dtos.TradeDto;
import com.accelfin.server.demo.fx.model.cash.blotter.SpotTrade;

import java.util.Arrays;

import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapBigDecimalToDto;

public class CashTradeMapper {

    public TradeDto[] mapToDtos(SpotTrade[] trades) {
        TradeDto[] tradDtos = Arrays.stream(trades)
                .map(this::mapToDto)
                .toArray(TradeDto[]::new);
        return tradDtos;
    }

    public TradeDto mapToDto(SpotTrade trade) {
        return TradeDto.newBuilder()
                .setTradeId(trade.getTradeId())
                .setCurrencyPair(trade.getCurrencyPair().getSymbol())
                .setNotional(mapBigDecimalToDto(trade.getNotional()))
                .setSide(MinorTypesMapper.mapSideToDto(trade.getSide()))
                .setRate(mapBigDecimalToDto(trade.getRate()))
                .setExecutedAt(CommonTypesMapper.mapInstantToDto(trade.getSubmittedAt()))
                .setUserName(String.format("%s %s", trade.user().getFirstName(), trade.user().getLastName()))
                .build();
    }
}
