package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.fx.dtos.BlotterUpdateTypeDto;
import com.accelfin.server.demo.fx.dtos.OrderBlotterSubscribeResponseDto;
import com.accelfin.server.demo.fx.dtos.OrderBlotterUpdateResponseDto;
import com.accelfin.server.demo.fx.model.common.BlotterUpdateType;
import com.accelfin.server.demo.fx.model.orders.blotter.SimpleOrder;
import com.accelfin.server.demo.fx.model.events.operationEvents.OrderBlotterSubscribeOperationEvent;
import com.google.inject.Inject;
import com.google.protobuf.Message;

import java.util.List;

public class OrderBlotterMapper implements BlotterMapper<SimpleOrder> {

    private OrderMapper _tradeMapper;

    @Inject
    public OrderBlotterMapper(OrderMapper orderMapper) {
        _tradeMapper = orderMapper;
    }

    public InboundMessageEvent mapOrderBlotterSubscribeOperationEvent(MessageEnvelope envelope) {
        return new OrderBlotterSubscribeOperationEvent();
    }

    @Override
    public Message mapToSubscriptionResponseDto(boolean success) {
        return OrderBlotterSubscribeResponseDto
                .newBuilder()
                .setIsSuccess(success)
                .build();
    }

    public Message mapToResponseDto(BlotterUpdateType updateType, List<SimpleOrder> trades) {
        BlotterUpdateTypeDto updateTypeDto = BlotterUpdateTypeMapper.mapToDto(updateType);
        return OrderBlotterUpdateResponseDto.newBuilder()
                .addAllOrders(() -> trades.stream().map(_tradeMapper::mapToDto).iterator())
                .setUpdateType(updateTypeDto)
                .build();
    }
}
