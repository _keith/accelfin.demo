package com.accelfin.server.demo.fx.model.common;

import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.fx.model.User;

import java.time.Instant;

public class TradeBase {
    private String _tradeId;
    private User _user;
    private CurrencyPair _currencyPair;
    private Instant _submittedAt;

    public TradeBase(String tradeId, CurrencyPair currencyPair, Instant submittedAt, User user) {
        _tradeId = tradeId;
        _currencyPair = currencyPair;
        _submittedAt = submittedAt;
        _user = user;
    }

    public Instant getSubmittedAt() {
        return _submittedAt;
    }

    public CurrencyPair getCurrencyPair() {
        return _currencyPair;
    }

    public String getTradeId() {
        return _tradeId;
    }

    public User user() {
        return _user;
    }

    @Override
    public String toString() {
        return "TradeBase{" +
                "_tradeId='" + _tradeId + '\'' +
                ", _currencyPair=" + _currencyPair +
                ", _submittedAt=" + _submittedAt +
                ", _user=" + _user +
                '}';
    }
}
