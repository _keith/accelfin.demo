package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.messaging.dtos.TimestampDto;
import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.fx.dtos.FxHistoricalTradeDto;
import com.accelfin.server.demo.fx.dtos.GetHistoricalTradeResponseDto;
import com.accelfin.server.demo.fx.dtos.GetHistoricalTradesRequestDto;
import com.accelfin.server.demo.fx.model.cash.blotter.SpotTrade;
import com.accelfin.server.demo.fx.model.events.operationEvents.GetHistoricalTradesEvent;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.accelfin.server.demo.fx.model.cash.pricing.PriceRepository;
import com.google.inject.Inject;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.accelfin.server.messaging.mappers.CommonTypesMapper.mapBigDecimalToDto;

public class HistoricalTradeMapper {
    private PriceRepository _priceRepository;
    private HistoricalPriceMapper _priceMapper;

    @Inject
    public HistoricalTradeMapper(PriceRepository priceRepository, HistoricalPriceMapper priceMapper) {
        _priceRepository = priceRepository;
        _priceMapper = priceMapper;
    }

    public InboundMessageEvent mapGetHistoricalTradesEvent(MessageEnvelope envelope) {
        GetHistoricalTradesRequestDto dto = envelope.getPayload();
        return new GetHistoricalTradesEvent(
                Instant.ofEpochSecond(dto.getStart().getSeconds()),
                Instant.ofEpochSecond(dto.getEnd().getSeconds())
        );
    }

    public GetHistoricalTradeResponseDto mapToDto(Collection<SpotTrade> trades) {
        List<FxHistoricalTradeDto> tradeDtos = trades
                .stream().map(trade ->
                {
                    FxSpotPriceEvent price = _priceRepository.getPrice(trade.getPriceId());
                    return FxHistoricalTradeDto.newBuilder()
                                .setTradeId(trade.getTradeId())
                                .setCurrencyPair(trade.getCurrencyPair().getSymbol())
                                .setNotional(mapBigDecimalToDto(trade.getNotional()))
                                .setSide(MinorTypesMapper.mapSideToDto(trade.getSide()))
                                .setExecutedRate(mapBigDecimalToDto(trade.getRate()))
                                .setPrice(_priceMapper.mapToDto(price))
                                .setExecutedAt(TimestampDto.newBuilder().setSeconds(trade.getSubmittedAt().getEpochSecond()))
                                .build();}
                )
                .collect(Collectors.toList());
        return GetHistoricalTradeResponseDto.newBuilder()
                .addAllTrades(tradeDtos)
                .build();
    }
}
