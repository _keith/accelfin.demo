package com.accelfin.server.demo.fx.container;

import com.accelfin.server.Clock;
import com.accelfin.server.Scheduler;
import com.accelfin.server.ServiceModelBase;
import com.accelfin.server.messaging.Connections;
import com.accelfin.server.esp.DefaultTerminalErrorHandler;
import com.accelfin.server.messaging.mappers.InboundMessageEventMapper;
import com.accelfin.server.demo.fx.dtos.mappers.*;
import com.accelfin.server.demo.fx.gateways.ReferenceDataGateway;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.cash.execution.CashExecution;
import com.accelfin.server.demo.fx.model.common.TradeRepository;
import com.accelfin.server.demo.fx.model.cash.blotter.CashBlotter;
import com.accelfin.server.demo.fx.model.cash.blotter.SpotTrade;
import com.accelfin.server.demo.fx.model.fakeData.FakeActivities;
import com.accelfin.server.demo.fx.model.orders.blotter.OrderBlotter;
import com.accelfin.server.demo.fx.model.orders.blotter.SimpleOrder;
import com.accelfin.server.demo.fx.model.orders.execution.*;
import com.accelfin.server.demo.fx.model.cash.pricing.PriceRepository;
import com.accelfin.server.demo.fx.model.cash.pricing.Pricer;
import com.accelfin.server.demo.fx.model.session.Sessions;
import com.accelfin.server.demo.fx.model.staticData.ReferenceData;
import com.esp.DefaultRouter;
import com.esp.Router;
import com.esp.RouterDispatcher;
import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;

public class FxContainerModule extends AbstractModule {

    private RouterDispatcher _routerDispatcher;
    private Scheduler _scheduler;
    private Class<? extends javax.inject.Provider<? extends Connections>> _connectionsProviderType;

    public FxContainerModule(
            RouterDispatcher routerDispatcher,
            Scheduler scheduler,
            Class<? extends javax.inject.Provider<? extends Connections>> connectionsProviderType
    ) {
        _routerDispatcher = routerDispatcher;
        _scheduler = scheduler;
        _connectionsProviderType = connectionsProviderType;
    }

    @Override
    protected void configure() {
        try {
            configureRouter();

            bind(Scheduler.class).toInstance(_scheduler);

            bind(FxService.class).in(Scopes.SINGLETON);
            bind(Sessions.class).in(Scopes.SINGLETON);
            bind(ReferenceData.class).in(Scopes.SINGLETON);
            bind(Pricer.class).in(Scopes.SINGLETON);
            bind(ReferenceDataGateway.class).in(Scopes.SINGLETON);
            bind(FakeActivities.class).in(Scopes.SINGLETON);
            bind(InboundMessageEventMapper.class).to(FxServiceInboundMessageEventMapper.class).in(Scopes.SINGLETON);
            bind(PriceRepository.class).in(Scopes.SINGLETON);

            bind(SessionMapper.class).in(Scopes.SINGLETON);
            bind(PriceMapper.class).in(Scopes.SINGLETON);
            bind(CashExecutionMapper.class).in(Scopes.SINGLETON);
            bind(HistoricalPriceMapper.class).in(Scopes.SINGLETON);
            bind(HistoricalTradeMapper.class).in(Scopes.SINGLETON);

            bind(CashExecution.class).in(Scopes.SINGLETON);
            bind(CashBlotter.class).in(Scopes.SINGLETON);
            bind(CashTradeMapper.class).in(Scopes.SINGLETON);
            bind(CashBlotterMapper.class).in(Scopes.SINGLETON);
            bind(new TypeLiteral<BlotterMapper<SpotTrade>>() {}).to(CashBlotterMapper.class).in(Scopes.SINGLETON);
            bind(new TypeLiteral<TradeRepository<SpotTrade>>() {}).in(Scopes.SINGLETON);

            bind(OrderExecution.class).in(Scopes.SINGLETON);
            bind(OrderBook.class).in(Scopes.SINGLETON);
            bind(OrderBlotter.class).in(Scopes.SINGLETON);
            bind(OrderMapper.class).in(Scopes.SINGLETON);
            bind(OrderBookMapper.class).in(Scopes.SINGLETON);
            bind(OrderBlotterMapper.class).in(Scopes.SINGLETON);
            bind(new TypeLiteral<BlotterMapper<SimpleOrder>>() {}).to(OrderBlotterMapper.class).in(Scopes.SINGLETON);
            bind(new TypeLiteral<TradeRepository<SimpleOrder>>() {}).in(Scopes.SINGLETON);
            install(new FactoryModuleBuilder().implement(OrderRepository.class, OrderRepository.class).build(OrderRepositoryFactory.class));

            bind(Clock.class).toConstructor(Clock.class.getConstructor(Router.class, Scheduler.class));
            bind(Connections.class).toProvider(_connectionsProviderType).in(Scopes.SINGLETON);
        } catch (NoSuchMethodException e) {
            addError(e);
        }
    }

    private void configureRouter() {
        // HACK need to register the router against Router<ServiceModelBase>, Router<FxService> and DefaultRouter<FxService>
        // however I can't see a good way to do this in Guice hence I have to create it and register
        // the instance against the relevant interfaces, using a cast HACK below.
        // The binding syntax doesn't seem to support this and the covariance support in java doesn't recognise
        // they are in fact the same
        DefaultTerminalErrorHandler errorHandler = new DefaultTerminalErrorHandler();
        DefaultRouter<FxService> router = new DefaultRouter<>(_routerDispatcher, errorHandler);
        bind(RouterDispatcher.class).toInstance(_routerDispatcher);
        bind(new TypeLiteral<Router<FxService>>() {}).toInstance(router);
        bind(new TypeLiteral<DefaultRouter<FxService>>() {}).toInstance(router);
        bind(new TypeLiteral<Router<ServiceModelBase>>() {}).toInstance((Router<ServiceModelBase>)(Router)router);
    }
}