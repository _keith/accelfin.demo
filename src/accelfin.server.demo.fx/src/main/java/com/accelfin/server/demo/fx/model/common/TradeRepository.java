package com.accelfin.server.demo.fx.model.common;

import java.time.Instant;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TradeRepository<TTrade extends TradeBase> {

    private HashMap<String, TTrade> _tradesById = new HashMap<>();
    private NavigableMap<Instant, ArrayList<TTrade>> _trades = new TreeMap<>();

    public void add(TTrade trade) {
        ArrayList<TTrade> trades = _trades.get(trade.getSubmittedAt());
        if(trades == null) {
            trades = new ArrayList<>();
            _trades.put(trade.getSubmittedAt(), trades);
        }
        trades.add(trade);
        _tradesById.put(trade.getTradeId(), trade);
    }

    public List<TTrade> getAllTrades() {
        ArrayList<TTrade> trades = _trades
                .values()
                .stream()
                .flatMap(List::stream)
                .collect(Collectors.toCollection(ArrayList::new));
        return trades;
    }

    public int getNumberOfTrades() {
        return _trades.size();
    }

    public TTrade getTrade(String id) {
        return _tradesById.get(id);
    }

    public List<TTrade> getTrades(HashSet<String> symbols, Instant start, Instant end) {
        return getTrades(start, end, trade -> symbols.contains(trade.getCurrencyPair().getSymbol()));
    }

    public List<TTrade> getTrades(Instant start, Instant end) {
        return getTrades(start, end, trade -> true);
    }

    private List<TTrade> getTrades(Instant start, Instant end, Predicate<TTrade> predicate) {
        SortedMap<Instant, ArrayList<TTrade>> range = _trades.subMap(
                start,
                true,
                end,
                true
        );
        return range
                .values()
                .stream()
                .flatMap(List::stream)
                .filter(predicate)
                .collect(Collectors.toList());
    }
}
