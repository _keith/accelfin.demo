package com.accelfin.server.demo.fx.model.fakeData;

import com.accelfin.server.demo.common.model.CurrencyPair;

import java.math.BigDecimal;
import java.util.HashMap;

public class CurrencyPairsMetadata {

    private HashMap<String, CurrencyPairMetadata> _pairs;

    public static CurrencyPairsMetadata Instance = new CurrencyPairsMetadata();

    private CurrencyPairsMetadata() {
        _pairs = new HashMap<>();

        addPair(new CurrencyPairMetadata(new CurrencyPair("AUDJPY", 2, 3, false), 1050));
        addPair(new CurrencyPairMetadata(new CurrencyPair("AUDNZD", 4, 5, false), 1100));
        addPair(new CurrencyPairMetadata(new CurrencyPair("AUDUSD", 4, 5, true), 1150));
        addPair(new CurrencyPairMetadata(new CurrencyPair("CADJPY", 2, 3, false), 1200));
        addPair(new CurrencyPairMetadata(new CurrencyPair("CHFJPY", 2, 3, false), 1250));
        addPair(new CurrencyPairMetadata(new CurrencyPair("GBPJPY", 2, 3, true), 1400));
        addPair(new CurrencyPairMetadata(new CurrencyPair("GBPUSD", 4, 5, true), 1450));
        addPair(new CurrencyPairMetadata(new CurrencyPair("EURCHF", 4, 5, false), 1600));
        addPair(new CurrencyPairMetadata(new CurrencyPair("EURGBP", 4, 5, false), 1650));
        addPair(new CurrencyPairMetadata(new CurrencyPair("EURJPY", 2, 3, true), 1700));
        addPair(new CurrencyPairMetadata(new CurrencyPair("EURUSD", 4, 5, true), 1000));
        addPair(new CurrencyPairMetadata(new CurrencyPair("NZDUSD", 4, 5, true), 1900));
        addPair(new CurrencyPairMetadata(new CurrencyPair("USDCAD", 4, 5, true), 1550));
    }

    public int getTickFrequencyMs(String symbol) {
        return _pairs.get(symbol).getTickFrequencyMs();
    }

    public CurrencyPair[] getPairs() {
        return _pairs.values()
                .stream()
                .map(md-> md.getCurrencyPair())
                .toArray(size -> new CurrencyPair[size]);
    }

    private void addPair(CurrencyPairMetadata pairMetadata) {
        _pairs.put(pairMetadata.getCurrencyPair().getSymbol(), pairMetadata);
    }

    class CurrencyPairMetadata {

        private CurrencyPair _currencyPair;
        private int _tickFrequencyMs;

        CurrencyPairMetadata(CurrencyPair pair, int tickFrequencyMs) {
            _currencyPair = pair;
            _tickFrequencyMs = tickFrequencyMs;
        }

        public CurrencyPair getCurrencyPair() {
            return _currencyPair;
        }

        public int getTickFrequencyMs() {
            return _tickFrequencyMs;
        }
    }
}
