package com.accelfin.server.demo.fx.dtos.mappers;

import com.accelfin.server.messaging.MessageEnvelope;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.fx.dtos.CashBlotterSubscribeResponseDto;
import com.accelfin.server.demo.fx.dtos.CashBlotterUpdateResponseDto;
import com.accelfin.server.demo.fx.dtos.BlotterUpdateTypeDto;
import com.accelfin.server.demo.fx.model.common.BlotterUpdateType;
import com.accelfin.server.demo.fx.model.cash.blotter.SpotTrade;
import com.accelfin.server.demo.fx.model.events.operationEvents.CashBlotterSubscribeOperationEvent;
import com.google.inject.Inject;
import com.google.protobuf.Message;

import java.util.List;

public class CashBlotterMapper implements BlotterMapper<SpotTrade> {

    private CashTradeMapper _tradeMapper;

    @Inject
    public CashBlotterMapper(CashTradeMapper tradeMapper) {
        _tradeMapper = tradeMapper;
    }

    public InboundMessageEvent mapBlotterSubscribeOperationEvent(MessageEnvelope envelope) {
        // request payload would be CashBlotterSubscribeRequestDto, but it has no content
        return new CashBlotterSubscribeOperationEvent();
    }

    @Override
    public Message mapToSubscriptionResponseDto(boolean success) {
        return CashBlotterSubscribeResponseDto
                .newBuilder()
                .setIsSuccess(success)
                .build();
    }

    public Message mapToResponseDto(BlotterUpdateType updateType, List<SpotTrade> trades) {
        BlotterUpdateTypeDto updateTypeDto = BlotterUpdateTypeMapper.mapToDto(updateType);
        return CashBlotterUpdateResponseDto.newBuilder()
                .addAllTrades(() -> trades.stream().map(_tradeMapper::mapToDto).iterator())
                .setUpdateType(updateTypeDto)
                .build();
    }

    public static CashBlotterSubscribeResponseDto mapForSuccess() {
        return CashBlotterSubscribeResponseDto.newBuilder()
                .setIsSuccess(true)
                .build();
    }
}
