package com.accelfin.server.demo.fx.model.events;

import java.math.BigDecimal;
import java.time.Instant;

public class FxSpotPriceEvent {
    public static FxSpotPriceEvent None = new FxSpotPriceEvent();

    private String _id;
    private String _symbol;
    private Instant _spotDate;
    private BigDecimal _mid;
    private BigDecimal _ask;
    private BigDecimal _bid;
    private BigDecimal _spread;
    private Instant _creationDate;
    private boolean _isTradable;

    private FxSpotPriceEvent() {
    }

    public FxSpotPriceEvent(
            String id,
            String symbol,
            Instant spotDate,
            BigDecimal mid,
            BigDecimal ask,
            BigDecimal bid,
            BigDecimal spread,
            Instant creationDate,
            boolean isTradable
    ) {

        this._id = id;
        this._symbol = symbol;
        this._spotDate = spotDate;
        this._mid = mid;
        this._ask = ask;
        this._bid = bid;
        this._spread = spread;
        this._creationDate = creationDate;
        this._isTradable = isTradable;
    }

    public String getId() {
        return _id;
    }

    public String getSymbol() {
        return _symbol;
    }

    public Instant getSpotDate() {
        return _spotDate;
    }

    public BigDecimal getMid() {
        return _mid;
    }

    public BigDecimal getAsk() {
        return _ask;
    }

    public BigDecimal getBid() {
        return _bid;
    }

    public BigDecimal getSpread() {
        return _spread;
    }

    public Instant getCreationDate() {
        return _creationDate;
    }

    public boolean getIsTradable() {
        return _isTradable;
    }

    @Override
    public String toString() {
        return "FxSpotPriceEvent{" +
                "_symbol='" + _symbol + '\'' +
                ", _mid=" + _mid +
                ", _ask=" + _ask +
                ", _bid=" + _bid +
                ", _spread=" + _spread +
                ", _creationDate=" + _creationDate +
                '}';
    }
}
