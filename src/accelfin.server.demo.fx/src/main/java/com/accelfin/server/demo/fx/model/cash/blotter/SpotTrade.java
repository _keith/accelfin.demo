package com.accelfin.server.demo.fx.model.cash.blotter;

import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.model.User;
import com.accelfin.server.demo.fx.model.common.TradeBase;

import java.math.BigDecimal;
import java.time.Instant;

public class SpotTrade extends TradeBase {
    private BigDecimal _notional;
    private Side _side;
    private String _priceId;
    private BigDecimal _rate;

    public SpotTrade(
            String tradeId,
            CurrencyPair currencyPair,
            BigDecimal notional,
            Side side,
            String priceId,
            BigDecimal rate,
            Instant submittedAt,
            User user
    ) {
        super(tradeId, currencyPair, submittedAt, user);
        _notional = notional.setScale(2, BigDecimal.ROUND_FLOOR);
        _side = side;
        _priceId = priceId;
        _rate = rate;
    }

    public BigDecimal getNotional() {
        return _notional;
    }

    public Side getSide() {
        return _side;
    }

    public String getPriceId() {
        return _priceId;
    }

    public BigDecimal getRate() {
        return _rate;
    }

}
