package com.accelfin.server.demo.fx.model.cash.blotter;

import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.demo.FxOperationNameConst;
import com.accelfin.server.demo.fx.dtos.GetHistoricalTradeResponseDto;
import com.accelfin.server.demo.fx.dtos.mappers.BlotterMapper;
import com.accelfin.server.demo.fx.dtos.mappers.HistoricalTradeMapper;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.common.BlotterBase;
import com.accelfin.server.demo.fx.model.common.TradeRepository;
import com.accelfin.server.demo.fx.model.events.SpotTradeExecutedEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.CashBlotterSubscribeOperationEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.GetHistoricalTradesEvent;
import com.esp.EventContext;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CashBlotter extends BlotterBase<SpotTrade> {

    private static Logger logger = LoggerFactory.getLogger(CashBlotter.class);

    private ConnectionBroker _connectionBroker;
    private TradeRepository<SpotTrade> _tradeRepository;
    private HistoricalTradeMapper _historicalTradeMapper;

    @Inject
    public CashBlotter(
            Router<FxService> router,
            ConnectionBroker connectionBroker,
            TradeRepository<SpotTrade> tradeRepository,
            BlotterMapper<SpotTrade> blotterMapper,
            HistoricalTradeMapper historicalTradeMapper) {
        super(router, connectionBroker, tradeRepository, blotterMapper);
        _connectionBroker = connectionBroker;
        _tradeRepository = tradeRepository;
        _historicalTradeMapper = historicalTradeMapper;
    }

    @Override
    protected void setOperationStatus(){
        _connectionBroker.setOperationStatus(FxOperationNameConst.CashBlotterUpdate, true);
        _connectionBroker.setOperationStatus(FxOperationNameConst.CashBlotterSubscribe, true);
        _connectionBroker.setOperationStatus(FxOperationNameConst.GetHistoricalTrades, true);
    }

    @Override
    protected String getBlotterOperationName() {
        return FxOperationNameConst.CashBlotterUpdate;
    }

    @ObserveEvent(eventClass = CashBlotterSubscribeOperationEvent.class)
    public void onBlotterSubscribeOperationEvent(CashBlotterSubscribeOperationEvent event) {
        processSubscriptionEvent(event);
    }

    @ObserveEvent(eventClass = SpotTradeExecutedEvent.class)
    public void onSpotTradeExecutedEvent(SpotTradeExecutedEvent event, EventContext eventContext) {
        onTradeExecutionEvent(event, eventContext);
    }

    @ObserveEvent(eventClass = GetHistoricalTradesEvent.class)
    public void onGetHistoricalTradesEvent(GetHistoricalTradesEvent event) {
        List<SpotTrade> trades = _tradeRepository.getTrades(
                event.getStart(),
                event.getEnd()
        );
        GetHistoricalTradeResponseDto response = _historicalTradeMapper.mapToDto(trades);
        logger.debug("Sending {} historical trades matching range [{}-{}]", trades.size(), event.getStart(), event.getEnd());
        _connectionBroker.sendRpcResponse(
                event,
                response
        );
    }
}
