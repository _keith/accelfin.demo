package com.accelfin.server.demo.fx.model.orders.execution;

import com.accelfin.server.demo.common.messaging.ConnectionBroker;
import com.accelfin.server.SessionToken;
import com.accelfin.server.messaging.events.InitEvent;
import com.accelfin.server.demo.FxOperationNameConst;
import com.accelfin.server.BigDecimalUtils;
import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.dtos.mappers.OrderBookMapper;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.events.CurrencyPairsResolvedEvent;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.accelfin.server.demo.fx.model.events.SessionsExpiredEvent;
import com.accelfin.server.demo.fx.model.events.SimpleOrderExecutedEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.OrderBookSubscriptionUpdateOperationEvent;
import com.accelfin.server.demo.fx.model.orders.blotter.SimpleOrder;
import com.accelfin.server.demo.fx.model.session.Session;
import com.accelfin.server.demo.fx.model.staticData.ReferenceData;
import com.esp.ObservationStage;
import com.esp.ObserveEvent;
import com.esp.Router;
import com.esp.disposables.DisposableBase;
import com.google.inject.Inject;
import com.google.protobuf.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;

public class OrderBook extends DisposableBase {
    private static Logger logger = LoggerFactory.getLogger(OrderBook.class);

    private Router<FxService> _router;
    private ConnectionBroker _connectionBroker;
    private ReferenceData _referenceData;
    private OrderRepository _buyOrders;
    private OrderRepository _sellOrders;
    private OrderBookMapper _orderBookMapper;
    private HashMap<SessionToken, List<HashSet<SessionToken>>> _subscribersSubscriptions = new HashMap<>();
    private HashMap<String, HashSet<SessionToken>> _subscriptionsByCurrencyPair = new HashMap<>();
    private HashMap<String, CurrencyPairOrderBookView> _orderBookViewsByCurrencyPair = new HashMap<>();
    private HashMap<String, FxSpotPriceEvent> _lastPriceByCurrencyPair = new HashMap<>();
    @Inject
    public OrderBook(
            Router<FxService> router,
            ConnectionBroker connectionBroker,
            ReferenceData referenceData,
            OrderRepositoryFactory orderRepositoryFactory,
            OrderBookMapper orderBookMapper
            ) {
        _router = router;
        _connectionBroker = connectionBroker;
        _referenceData = referenceData;
        _orderBookMapper = orderBookMapper;
        _buyOrders = orderRepositoryFactory.create(Side.Buy);
        _sellOrders = orderRepositoryFactory.create(Side.Sell);
    }

    public void observeEvents() {
        addDisposable(_router.observeEventsOn(this));
    }

    @ObserveEvent(eventClass = InitEvent.class)
    public void onInitEvent() {
        _connectionBroker.setOperationStatus(FxOperationNameConst.OrderBookSubscribe, true);
        _connectionBroker.setOperationStatus(FxOperationNameConst.OrderBookUpdate, true);
    }

    @ObserveEvent(eventClass = CurrencyPairsResolvedEvent.class, stage = ObservationStage.Committed)
    public void onCurrencyPairsResolvedEvent() {
        for (CurrencyPair pair : _referenceData.currencyPairs()) {
            _subscriptionsByCurrencyPair.put(pair.getSymbol(), new HashSet<>());
        }
        _buyOrders.initialiseWithPairs(_referenceData.currencyPairs());
        _sellOrders.initialiseWithPairs(_referenceData.currencyPairs());
    }

    @ObserveEvent(eventClass = OrderBookSubscriptionUpdateOperationEvent.class)
    public void onOrderBookSubscribeOperationEvent(OrderBookSubscriptionUpdateOperationEvent event) {
        logger.debug("Order book subscribe event received for [{}]. Symbols [{}]. Sending state of the world.", event.getSessionToken(), event.getSymbols());
        List<HashSet<SessionToken>> subscribersSubscriptions = _subscribersSubscriptions.get(event.getSessionToken());
        if(subscribersSubscriptions == null) {
            subscribersSubscriptions = new ArrayList<>();
            _subscribersSubscriptions.put(event.getSessionToken(), subscribersSubscriptions);
        } else {
            // blow away existing subscriptions, we'll just add them again below if required
            for (HashSet<SessionToken> set: subscribersSubscriptions){
                set.remove(event.getSessionToken());
            }
        }
        for (String symbol : event.getSymbols()) {
            HashSet<SessionToken> currencyPairSubscriptions = _subscriptionsByCurrencyPair.get(symbol);
            if (currencyPairSubscriptions.add(event.getSessionToken())) {
                // we maintain another data structure of the users subscriptions to aid in un-subscription later
                subscribersSubscriptions.add(currencyPairSubscriptions);
                _connectionBroker.sendRpcResponse(
                        event,
                        _orderBookMapper.mapToOrderBookSubscriptionUpdateResponseDto(true)
                );
                CurrencyPairOrderBookView currencyPairOrderBookView = _orderBookViewsByCurrencyPair.get(symbol);
                if (currencyPairOrderBookView != null) {
                    dispatchUpdate(currencyPairOrderBookView, event.getSessionToken());
                }
            } else {
                throw new RuntimeException("Invalid subscriber state. Should not be subscribed as we just removed subscriptoins");
            }
        }
    }

    @ObserveEvent(eventClass = SessionsExpiredEvent.class)
    public void onSessionsExpiredEvent(SessionsExpiredEvent event) {
        StringBuilder logMessage = new StringBuilder();
        for (Session session : event.getSessions()) {
            List<HashSet<SessionToken>> subscribersSubscriptions = _subscribersSubscriptions.get(session.getSessionToken());
            if (subscribersSubscriptions != null) {
                logMessage
                    .append("Session [")
                    .append(session.getSessionToken())
                    .append("] expired removing order book subscriptions")
                    .append("\n");
                for (HashSet<SessionToken> set: subscribersSubscriptions){
                    set.remove(session.getSessionToken());
                }
                _subscribersSubscriptions.remove(session.getSessionToken());
            }
        }
        logger.debug(logMessage.toString());
    }

    @ObserveEvent(eventClass = FxSpotPriceEvent.class, stage = ObservationStage.Committed)
    public void onFxSpotPriceReceivedEvent(FxSpotPriceEvent event) {
        _buyOrders.tryFillOrders(event);
        _sellOrders.tryFillOrders(event);
        _lastPriceByCurrencyPair.put(event.getSymbol(), event);
        updateOrderBookViewForPair(event);
    }

    @ObserveEvent(eventClass = SimpleOrderExecutedEvent.class)
    public void onSimpleOrderExecutedEvent(SimpleOrderExecutedEvent event) {
        SimpleOrder order = event.getTrade();
        Side side = order.getSide();
        FxSpotPriceEvent lastPrice = _lastPriceByCurrencyPair.get(order.getCurrencyPair().getSymbol());
        if (side.equals(Side.Buy)) {
            if (lastPrice != null && BigDecimalUtils.greaterThan(order.getSubmittedRate(), lastPrice.getAsk())) {
                logger.warn("Buy Order {} will immediately execute", order);
            }
            _buyOrders.add(order);
        } else if (side.equals(Side.Sell)) {
            if (lastPrice != null && BigDecimalUtils.lessThan(order.getSubmittedRate(), lastPrice.getBid())) {
                logger.warn("Sell Order {} will immediately execute", order);
            }
            _sellOrders.add(order);
        } else {
            throw new RuntimeException();
        }
        if(lastPrice != null) {
            updateOrderBookViewForPair(lastPrice);
        }
    }

    private void updateOrderBookViewForPair(FxSpotPriceEvent lastPrice) {
        List<OrderBookRung> rungs = _buyOrders.getOrderBookRungs(5, lastPrice);
        rungs.add(new OrderBookRung(BigDecimal.ZERO, lastPrice.getMid(),OrderBookRungType.MID));
        rungs.addAll(_sellOrders.getOrderBookRungs(5, lastPrice));
        CurrencyPairOrderBookView update = new CurrencyPairOrderBookView(lastPrice.getSymbol(), rungs);
        _orderBookViewsByCurrencyPair.put(lastPrice.getSymbol(), update);
        HashSet<SessionToken> subscribers = _subscriptionsByCurrencyPair.get(lastPrice.getSymbol());
        if(subscribers.size() > 0) {
            dispatchUpdate(
                    update,
                    subscribers.toArray(new SessionToken[subscribers.size()])
            );
        }
    }

    private void dispatchUpdate(
            CurrencyPairOrderBookView currencyPairOrderBookView,
            SessionToken... subscribers
    ) {
        Message responseDto = _orderBookMapper.mapToOrderBookUpdateResponseDto(currencyPairOrderBookView);
        _connectionBroker.sendStreamResponseMessage(
                FxOperationNameConst.OrderBookUpdate,
                responseDto,
                false,
                subscribers
        );
    }
}
