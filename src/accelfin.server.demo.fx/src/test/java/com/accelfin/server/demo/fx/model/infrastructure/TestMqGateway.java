package com.accelfin.server.demo.fx.model.infrastructure;

import com.accelfin.server.messaging.gateways.MqGateway;

public class TestMqGateway implements MqGateway {
    @Override
    public void connect() throws Exception {

    }

    @Override
    public void observeEvents() {

    }

    @Override
    public void dispose() {

    }
}
