package com.accelfin.server.demo.fx.model.infrastructure;

public class TestUsers {
    public TestUser _keith;
    public TestUser _ray;
    private FxModelTestBase _testBase;

    public TestUsers(FxModelTestBase testBase) {
        _testBase = testBase;
        _keith = new TestUser(
                "keith",
                "password",
                "keith-clientSessionId",
                testBase,
                testBase.Router,
                testBase.Clock
        );
        _ray = new TestUser(
                "ray",
                "password",
                "ray-clientSessionId",
                testBase,
                testBase.Router,
                testBase.Clock
        );
    }

    public TestUser keith() {
        return _keith;
    }

    public TestUser ray() {
        return _ray;
    }
}