package com.accelfin.server.demo.fx.model;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class BigDecimalUtils {
    public static void assertBigDecimalEquals(BigDecimal left, BigDecimal right) {
        assertEquals(left.unscaledValue().longValue(), right.unscaledValue().longValue());
        assertEquals(left.scale(), right.scale());
    }
}
