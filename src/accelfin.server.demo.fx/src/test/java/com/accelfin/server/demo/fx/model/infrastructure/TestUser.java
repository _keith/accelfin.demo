package com.accelfin.server.demo.fx.model.infrastructure;

import com.accelfin.server.Clock;
import com.accelfin.server.messaging.OperationNames;
import com.accelfin.server.SessionToken;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.events.operationEvents.ClientHeartbeatOperationEvent;
import com.esp.Router;

public class TestUser {
    private SessionToken _sessionToken;
    private String _userName;
    private String _password;
    private String _clientSessionId;
    private FxModelTestBase _testBase;
    private Router<FxService> _router;
    private Clock _clock;

    public TestUser(
            String userName,
            String password,
            String clientSessionId,
            FxModelTestBase testBase,
            Router<FxService> router,
            Clock clock
    ) {
        _userName = userName;
        _password = password;
        _clientSessionId = clientSessionId;
        _testBase = testBase;
        _router = router;
        _clock = clock;
    }

    public SessionToken getSessionToken() {
        return _sessionToken;
    }

    public String getUserName() {
        return _userName;
    }

    public String getPassword() {
        return _password;
    }

    public String getClientSessionId() {
        return _clientSessionId;
    }

    public TestUser logon() {
        _sessionToken = _testBase.Scenarios.logUserOn(_userName, _password, _clientSessionId);
        return this;
    }

    public void publishHeartbeat() {
        ClientHeartbeatOperationEvent event = new ClientHeartbeatOperationEvent(_clock.now());
        event.setSessionToken(_sessionToken);
        event.setOperationName(OperationNames.Heartbeat);
        _router.publishEvent(event);
    }
}