package com.accelfin.server.demo.fx.model;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class UserTest {
    private User _testUser;

    @Before
    public void setUp() throws Exception {
        _testUser = new User("keith", "Keith", "Woods", new String[]{"fx-cash"});
    }

    @Test
    public void hasInitialLiquidity() throws Exception {
        assertTrue(_testUser.hasLiquidity("EURUSD"));
    }

    @Test
    public void initialLiquidityIs1000000() throws Exception {
        assertTrue(BigDecimal.valueOf(1000000).equals(_testUser.getLiquidity("EURUSD")));
        assertTrue(BigDecimal.valueOf(1000000).equals(_testUser.getLiquidity("EURUSD")));
    }

    @Test
    public void decreaseLiquidity() throws Exception {
        BigDecimal decrement = BigDecimal.valueOf(10);
        boolean wasDecremented = _testUser.decreaseLiquidity("EURUSD", decrement);
        assertTrue(BigDecimal.valueOf(1000000).subtract(decrement).equals(_testUser.getLiquidity("EURUSD")));
        assertTrue(wasDecremented);
    }

    @Test
    public void canNotDecreaseLiquidityPast0() throws Exception {
        assertTrue(_testUser.decreaseLiquidity("EURUSD", BigDecimal.valueOf(999999)));
        assertTrue(BigDecimal.valueOf(1).equals(_testUser.getLiquidity("EURUSD")));
        assertTrue(_testUser.decreaseLiquidity("EURUSD", BigDecimal.valueOf(1)));
        assertTrue(BigDecimal.valueOf(0).equals(_testUser.getLiquidity("EURUSD")));
        assertFalse(_testUser.decreaseLiquidity("EURUSD", BigDecimal.valueOf(1)));
        assertTrue(BigDecimal.valueOf(0).equals(_testUser.getLiquidity("EURUSD")));
    }
}