package com.accelfin.server.demo.fx.model.infrastructure;

import com.accelfin.server.Clock;
import com.accelfin.server.ServiceModelBase;
import com.accelfin.server.demo.fx.container.ConnectionsProvider;
import com.accelfin.server.demo.fx.dtos.mappers.FxServiceInboundMessageEventMapper;
import com.accelfin.server.messaging.gateways.MqGateway;
import com.accelfin.server.test.TestScheduler;
import com.esp.Router;
import com.google.inject.Inject;

public class TestConnectionProvider extends ConnectionsProvider {
    @Inject
    public TestConnectionProvider(
            Router<ServiceModelBase> router,
            Clock clock,
            TestScheduler scheduler,
            FxServiceInboundMessageEventMapper eventMapper) {
        super(router, clock, scheduler, eventMapper);
    }

    @Override
    protected MqGateway mqGatewayOverride() {
        return new TestMqGateway();
    }
}
