package com.accelfin.server.demo.fx.model.fakeData;

import com.accelfin.server.demo.common.model.CurrencyPair;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.accelfin.server.demo.fx.model.infrastructure.FxModelTestBase;
import org.junit.Test;

import java.time.Instant;

import static org.junit.Assert.*;

public class CurrencyPairPriceGeneratorTest extends FxModelTestBase {

    private CurrencyPairPriceGenerator _eurusdGenerator;
    private CurrencyPair _eurusd;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        initialiseModel();
        _eurusd = Model.referenceData().getCurrencyPair("EURUSD");
        _eurusdGenerator = Model.getFakeActivities().getPriceGenerator(_eurusd.getSymbol());
    }

    @Test
    public void getNextPriceHasCorrectPrecision() throws Exception {
        FxSpotPriceEvent price = _eurusdGenerator.getNextPrice(Instant.now());
        assertEquals(_eurusd.getRatePrecision(), price.getAsk().scale());
        assertEquals(_eurusd.getRatePrecision(), price.getBid().scale());
    }
}