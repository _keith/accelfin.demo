package com.accelfin.server.demo.fx.model.infrastructure;

import com.accelfin.server.demo.common.messaging.ConnectionIds;
import com.accelfin.server.SessionToken;
import com.accelfin.server.messaging.events.InboundMessageEvent;
import com.accelfin.server.demo.FxOperationNameConst;
import com.accelfin.server.BigDecimalUtils;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.model.cash.blotter.SpotTrade;
import com.accelfin.server.demo.fx.model.fakeData.FakeUsers;
import com.accelfin.server.demo.fx.model.orders.blotter.OrderType;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.accelfin.server.demo.fx.model.events.SpotTradeExecutedEvent;
import com.accelfin.server.demo.fx.model.events.operationEvents.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.UUID;

public class Scenarios {

    private final Instant _spotDate;
    private FxModelTestBase _testBase;

    public Scenarios(FxModelTestBase testBase) {
        _testBase = testBase;
        _spotDate = LocalDate.now().atStartOfDay().toInstant(ZoneOffset.UTC);
    }

    // publish a bank side price
    public FxSpotPriceEvent publishPrice(String symbol, BigDecimal bid, BigDecimal ask) throws Exception {
        // the bid must always be less for bank side pricing.
        // i.e. if the client is buying they must pay a higher ask price
        if(BigDecimalUtils.greaterThanOrEqualTo(bid, ask)) throw new RuntimeException();
        BigDecimal spread = ask.subtract(bid);
        BigDecimal mid = bid.add(ask.subtract(bid).divide(BigDecimal.valueOf(2)));
        FxSpotPriceEvent priceEvent = new FxSpotPriceEvent(
                UUID.randomUUID().toString(),
                symbol,
                _spotDate,
                mid,
                ask,
                bid,
                spread,
                _spotDate,
                true
        );
        publishSystemEvent(priceEvent);
        return priceEvent;
    }

    public void executeSpotTrade(SessionToken sessionToken, String symbol, Side side, double amount, FxSpotPriceEvent price) {
        ExecuteSpotOperationEvent operationEvent = new ExecuteSpotOperationEvent(
                symbol,
                side,
                BigDecimal.valueOf(amount),
                price.getId(),
                side == Side.Buy ? price.getAsk() : price.getBid(),
                false
        );
        publishUserEvent(sessionToken, operationEvent, FxOperationNameConst.ExecuteSpot);
    }

    public void preLoadSpotTrade(String symbol, Side side, double amount, FxSpotPriceEvent price) {
        SpotTrade spotTrade = new SpotTrade(
                UUID.randomUUID().toString(),
                _testBase.Model.referenceData().getCurrencyPair(symbol),
                BigDecimal.valueOf(amount),
                side,
                price.getId(),
                side == Side.Buy ? price.getAsk() : price.getBid(),
                _testBase.Clock.now(),
                FakeUsers.Instance.getKeith()

        );
        publishSystemEvent(new SpotTradeExecutedEvent(spotTrade));
    }

    public void executeSimpleOrder(SessionToken sessionToken, String symbol, Side side, double executeAtRate) throws Exception {
        executeSimpleOrder(sessionToken, symbol, side, executeAtRate, 100);
    }

    public void executeSimpleOrder(SessionToken sessionToken, String symbol, Side side, double executeAtRate, double notional) throws Exception {
        ExecuteSimpleOrderOperationEvent operationEvent = new ExecuteSimpleOrderOperationEvent(
                side,
                OrderType.LIMIT,
                BigDecimal.valueOf(notional),
                BigDecimal.valueOf(executeAtRate),
                symbol
        );
        publishUserEvent(sessionToken, operationEvent, FxOperationNameConst.ExecuteSimpleOrder);
    }

    public SessionToken logUserOn(String username, String password, String clientSessionId) {
        SessionToken sessionToken = createWebSessionToken(clientSessionId);
        LoginOperationEvent event = new LoginOperationEvent(username, password, clientSessionId);
        publishUserEvent(sessionToken, event, FxOperationNameConst.Login);
        return sessionToken;
    }

    public void subscribeToCashBlotter(SessionToken sessionToken) {
        CashBlotterSubscribeOperationEvent event = new CashBlotterSubscribeOperationEvent();
        publishUserEvent(sessionToken, event, FxOperationNameConst.CashBlotterSubscribe);
    }

    public void subscribeToOrderBlotter(SessionToken sessionToken) {
        OrderBlotterSubscribeOperationEvent event = new OrderBlotterSubscribeOperationEvent();
        publishUserEvent(sessionToken, event, FxOperationNameConst.OrderBlotterSubscribe);
    }

    public void updateOrderBookSubscription(SessionToken sessionToken, String... currencyPairs) {
        OrderBookSubscriptionUpdateOperationEvent event = new OrderBookSubscriptionUpdateOperationEvent(currencyPairs);
        publishUserEvent(sessionToken, event, FxOperationNameConst.OrderBookSubscribe);
    }

    private void publishUserEvent(SessionToken sessionToken, InboundMessageEvent operationEvent, String operationName) {
        operationEvent.setSessionToken(sessionToken);
        operationEvent.setOperationName(operationName);
        _testBase.Router.publishEvent(operationEvent);
    }

    private void publishSystemEvent(Object event) {
        _testBase.Router.publishEvent(event);
    }

    private SessionToken createWebSessionToken(String clientSessionId) {
        return new SessionToken(clientSessionId, ConnectionIds.WEB);
    }
}
