package com.accelfin.server.demo.fx.model.orders.execution;

import com.accelfin.server.SessionToken;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.dtos.OrderBookRungDto;
import com.accelfin.server.demo.fx.dtos.OrderBookSubscriptionUpdateResponseDto;
import com.accelfin.server.demo.fx.dtos.OrderBookUpdateResponseDto;
import com.accelfin.server.demo.fx.model.infrastructure.FxModelTestBase;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OrderBookTests extends FxModelTestBase {
    private int SESSION_KEEP_ALIVE_MINS = 60;
    private SessionToken _keithsSessionToken;
    private SessionToken _raysSessionToken;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        initialiseModel();
        logonKeith();
        logonRay();
        populateOrderBook("EURUSD");
        populateOrderBook("EURGBP");
        populateOrderBook("AUDNZD");
        // bank side prices, 0.00020 spread
        Scenarios.publishPrice("EURUSD", BigDecimal.valueOf(100090, 5), BigDecimal.valueOf(100110, 5));
        Scenarios.publishPrice("EURGBP", BigDecimal.valueOf(100090, 5), BigDecimal.valueOf(100110, 5));
        Scenarios.publishPrice("AUDNZD", BigDecimal.valueOf(100090, 5), BigDecimal.valueOf(100110, 5));
        clearWebOutboundMessages();
    }

    @Test
    public void orderBookAcksSubscribeResponse() throws Exception {
        Scenarios.updateOrderBookSubscription(_keithsSessionToken, "EURUSD");
        OrderBookSubscriptionUpdateResponseDto message1 = getWebOutboundMessagesPayload(0);
        assertTrue(message1.getIsSuccess());
    }

    @Test
    public void orderBookUpdateDtoRungsMatcheOrderBook() throws Exception {
        Scenarios.updateOrderBookSubscription(_keithsSessionToken, "EURUSD");
        OrderBookUpdateResponseDto dto = getWebOutboundMessagesPayload(1);
        // current price is bid 1.00099 mid 1.00090, ask 1.00110
        // note the annoying numbers so we get trailing zeros
        assertRung(dto, 0, OrderBookRungDto.OrderBookRungTypeDto.BUY, BigDecimal.valueOf(1.00105), BigDecimal.valueOf(500000, 2));
        assertRung(dto, 1, OrderBookRungDto.OrderBookRungTypeDto.BUY, BigDecimal.valueOf(1.00104), BigDecimal.valueOf(600000, 2));
        assertRung(dto, 2, OrderBookRungDto.OrderBookRungTypeDto.BUY, BigDecimal.valueOf(1.00103), BigDecimal.valueOf(700000, 2));
        assertRung(dto, 3, OrderBookRungDto.OrderBookRungTypeDto.BUY, BigDecimal.valueOf(1.00102), BigDecimal.valueOf(800000, 2));
        assertRung(dto, 4, OrderBookRungDto.OrderBookRungTypeDto.BUY, BigDecimal.valueOf(1.00101), BigDecimal.valueOf(900000, 2));
        assertRung(dto, 5, OrderBookRungDto.OrderBookRungTypeDto.MID, BigDecimal.valueOf(100100, 5), BigDecimal.valueOf(0, 2)); // annoying number do we get trailing zeros
        assertRung(dto, 6, OrderBookRungDto.OrderBookRungTypeDto.SELL, BigDecimal.valueOf(1.00099), BigDecimal.valueOf(900000, 2));
        assertRung(dto, 7, OrderBookRungDto.OrderBookRungTypeDto.SELL, BigDecimal.valueOf(1.00098), BigDecimal.valueOf(800000, 2));
        assertRung(dto, 8, OrderBookRungDto.OrderBookRungTypeDto.SELL, BigDecimal.valueOf(1.00097), BigDecimal.valueOf(700000, 2));
        assertRung(dto, 9, OrderBookRungDto.OrderBookRungTypeDto.SELL, BigDecimal.valueOf(1.00096), BigDecimal.valueOf(600000, 2));
        assertRung(dto, 10, OrderBookRungDto.OrderBookRungTypeDto.SELL, BigDecimal.valueOf(1.00095), BigDecimal.valueOf(500000, 2));
        assertEquals(11, dto.getRungsCount());
    }

    @Test
    @Ignore
    public void newOrdersGetAddedToCorrectRungs() throws Exception {
        Scenarios.updateOrderBookSubscription(_keithsSessionToken, "EURUSD");

        clearWebOutboundMessages();

        Scenarios.executeSimpleOrder(_keithsSessionToken, "EURUSD", Side.Sell, 1.00103, 4000);
        OrderBookUpdateResponseDto dto = getWebOutboundMessagesPayload(1);
        assertRung(dto, 1, OrderBookRungDto.OrderBookRungTypeDto.SELL, BigDecimal.valueOf(1.00103), BigDecimal.valueOf(8000.00));

        clearWebOutboundMessages();

        Scenarios.executeSimpleOrder(_keithsSessionToken, "EURUSD", Side.Buy, 1.00099, 2000);
        dto = getWebOutboundMessagesPayload(1);
        assertRung(dto, 7, OrderBookRungDto.OrderBookRungTypeDto.BUY, BigDecimal.valueOf(1.00099), BigDecimal.valueOf(4000.00));
    }

    @Test
    @Ignore
    public void executedOrdersAreRemovedFromRungs() throws Exception {
        Scenarios.updateOrderBookSubscription(_keithsSessionToken, "EURUSD");
        clearWebOutboundMessages();
        Scenarios.publishPrice("EURUSD", BigDecimal.valueOf(100090, 5), BigDecimal.valueOf(100110, 5));
        OrderBookUpdateResponseDto dto = getWebOutboundMessagesPayload(0);
        assertRung(dto, 0, OrderBookRungDto.OrderBookRungTypeDto.SELL, BigDecimal.valueOf(1.00102), BigDecimal.valueOf(3000.00));
        assertRung(dto, 1, OrderBookRungDto.OrderBookRungTypeDto.SELL, BigDecimal.valueOf(1.00101), BigDecimal.valueOf(2000.00));
        assertRung(dto, 2, OrderBookRungDto.OrderBookRungTypeDto.SELL, BigDecimal.valueOf(1.00100), BigDecimal.valueOf(1000.00));
        assertRung(dto, 3, OrderBookRungDto.OrderBookRungTypeDto.SELL, BigDecimal.valueOf(1.00099), BigDecimal.valueOf(0.00));
        assertRung(dto, 4, OrderBookRungDto.OrderBookRungTypeDto.SELL, BigDecimal.valueOf(1.00098), BigDecimal.valueOf(0.00));
        assertRung(dto, 5, OrderBookRungDto.OrderBookRungTypeDto.MID, BigDecimal.valueOf(1.00098), BigDecimal.valueOf(0.00));
        assertRung(dto, 6, OrderBookRungDto.OrderBookRungTypeDto.BUY, BigDecimal.valueOf(1.00098), BigDecimal.valueOf(3000.00));
        assertRung(dto, 7, OrderBookRungDto.OrderBookRungTypeDto.BUY, BigDecimal.valueOf(1.00097), BigDecimal.valueOf(4000.00));
        assertRung(dto, 8, OrderBookRungDto.OrderBookRungTypeDto.BUY, BigDecimal.valueOf(1.00096), BigDecimal.valueOf(5000.00));
        assertRung(dto, 9, OrderBookRungDto.OrderBookRungTypeDto.BUY, BigDecimal.valueOf(1.00095), BigDecimal.valueOf(6000.00));
        assertRung(dto, 10, OrderBookRungDto.OrderBookRungTypeDto.BUY, BigDecimal.valueOf(1.00094), BigDecimal.valueOf(7000.00));
        assertEquals(11, dto.getRungsCount());
    }

    @Test
    public void rungUpdatesNotSentAfterUsersSessionExpires() throws Exception {
        Scenarios.updateOrderBookSubscription(_keithsSessionToken, "EURUSD");
        clearWebOutboundMessages();

        Scenarios.publishPrice("EURUSD", BigDecimal.valueOf(1.00097), BigDecimal.valueOf(1.00099));
        assertTrue(ArrayUtils.contains(WebOutboundMessages.get(0).getSessionIds(), _keithsSessionToken.getSessionId()));
        assertTrue(WebOutboundMessages.get(0).getPayload().getClass().equals(OrderBookUpdateResponseDto.class));


        TestScheduler.advanceTimeBy((SESSION_KEEP_ALIVE_MINS * 60 * 1000) + 1);
        clearWebOutboundMessages();

        Scenarios.publishPrice("EURUSD", BigDecimal.valueOf(1.00096), BigDecimal.valueOf(1.00098));
        assertEquals(0, WebOutboundMessages.size());
    }

    @Test
    public void updatesSentToAllUsers() throws Exception {
        Scenarios.updateOrderBookSubscription(_keithsSessionToken, "EURUSD");
        clearWebOutboundMessages();
        Scenarios.publishPrice("EURUSD", BigDecimal.valueOf(100097, 5), BigDecimal.valueOf(100099, 5));
        String[] updateSessionIds = WebOutboundMessages.get(0).getSessionIds();
        assertTrue(ArrayUtils.contains(updateSessionIds, _keithsSessionToken.getSessionId()));
        // 2 users are logged on, just ensure the update was only sent to one
        assertEquals(1, WebOutboundMessages.get(0).getSessionIds().length);

        // no log on another uers
        Scenarios.updateOrderBookSubscription(_raysSessionToken, "EURUSD");
        clearWebOutboundMessages();

        Scenarios.publishPrice("EURUSD", BigDecimal.valueOf(100096, 5), BigDecimal.valueOf(100098, 5));
        updateSessionIds = WebOutboundMessages.get(0).getSessionIds();
        // ensure the update was sent to both users
        assertEquals(2, WebOutboundMessages.get(0).getSessionIds().length);
        assertTrue(ArrayUtils.contains(updateSessionIds, _keithsSessionToken.getSessionId()));
        assertTrue(ArrayUtils.contains(updateSessionIds, _raysSessionToken.getSessionId()));
    }

    @Test
    public void updatesOnlySentForSubscribedCurrencyPair() throws Exception {
        Scenarios.updateOrderBookSubscription(_keithsSessionToken, "EURUSD");
        clearWebOutboundMessages();
        // we've not yet subscribed for EURGBP
        Scenarios.publishPrice("EURGBP", BigDecimal.valueOf(100096, 5), BigDecimal.valueOf(100098, 5));
        // ... so shouldn't receive an update
        assertEquals(0, WebOutboundMessages.size());
        // now we subscribe
        Scenarios.updateOrderBookSubscription(_keithsSessionToken, "EURGBP", "EURUSD");
        // ignore any updates and SOW
        clearWebOutboundMessages();
        // now publish again and see if we got an update
        Scenarios.publishPrice("EURGBP", BigDecimal.valueOf(100095, 5), BigDecimal.valueOf(100097, 5));
        assertEquals(1, WebOutboundMessages.size());
        OrderBookUpdateResponseDto dto = getWebOutboundMessagesPayload(0);
        assertEquals("EURGBP", dto.getCurrencyPair());
        // now publish a EURUSD and check the update was for the correct pair
        clearWebOutboundMessages();
        Scenarios.publishPrice("EURUSD", BigDecimal.valueOf(100096, 5), BigDecimal.valueOf(100098, 5));
        assertEquals(1, WebOutboundMessages.size());
        dto = getWebOutboundMessagesPayload(0);
        assertEquals("EURUSD", dto.getCurrencyPair());
    }

    @Test
    public void canSubscribeToMultipleCurrencyPairs() throws Exception {
        Scenarios.updateOrderBookSubscription(_keithsSessionToken, "EURUSD", "EURGBP", "AUDNZD");
        clearWebOutboundMessages();

        OrderBookUpdateResponseDto dto;

        Scenarios.publishPrice("EURUSD", BigDecimal.valueOf(100096, 5), BigDecimal.valueOf(100098, 5));
        assertEquals(1, WebOutboundMessages.size());
        dto = getWebOutboundMessagesPayload(0);
        assertEquals("EURUSD", dto.getCurrencyPair());
        clearWebOutboundMessages();

        Scenarios.publishPrice("EURGBP", BigDecimal.valueOf(100096, 5), BigDecimal.valueOf(100098, 5));
        assertEquals(1, WebOutboundMessages.size());
        dto = getWebOutboundMessagesPayload(0);
        assertEquals("EURGBP", dto.getCurrencyPair());
        clearWebOutboundMessages();

        Scenarios.publishPrice("AUDNZD", BigDecimal.valueOf(100096, 5), BigDecimal.valueOf(100098, 5));
        assertEquals(1, WebOutboundMessages.size());
        dto = getWebOutboundMessagesPayload(0);
        assertEquals("AUDNZD", dto.getCurrencyPair());
        clearWebOutboundMessages();
    }

    @Test
    public void subscriptionUpdateCanRemovePairs() throws Exception {
        Scenarios.updateOrderBookSubscription(_keithsSessionToken, "EURUSD", "EURGBP", "AUDNZD");
        clearWebOutboundMessages();

        OrderBookUpdateResponseDto dto;

        Scenarios.publishPrice("EURUSD", BigDecimal.valueOf(100096, 5), BigDecimal.valueOf(100098, 5));
        assertEquals(1, WebOutboundMessages.size());
        dto = getWebOutboundMessagesPayload(0);
        assertEquals("EURUSD", dto.getCurrencyPair());
        clearWebOutboundMessages();

        Scenarios.updateOrderBookSubscription(_keithsSessionToken, "EURGBP", "AUDNZD");
        clearWebOutboundMessages();

        Scenarios.publishPrice("EURUSD", BigDecimal.valueOf(100096, 5), BigDecimal.valueOf(100098, 5));
        assertEquals(0, WebOutboundMessages.size());
    }

    private void populateOrderBook(String currencyPair) throws Exception {
        // Given a price of 90(bid) 110(ask) we have this order book structure:
        //
        // side:                       SELL	  BUY
        // price:                      90     110
        // orders:   93     92     91              109     108     107

        // Sell orders - don't think of the below ordering as rungs, it's simply orders above the current bid that are in play
        // current bid is assumed 1.00090
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Sell, 1.00091, 1000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Sell, 1.00092, 2000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Sell, 1.00093, 3000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Sell, 1.00094, 4000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Sell, 1.00095, 5000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Sell, 1.00096, 6000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Sell, 1.00097, 7000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Sell, 1.00098, 8000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Sell, 1.00099, 9000); // no 1.00100, that's the mid

        // Buy orders - don't think of the below ordering as runs, it's simply orders below the current ask that are in play
        // current ask is assumed 1.00110
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Buy, 1.00109, 1000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Buy, 1.00108, 2000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Buy, 1.00107, 3000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Buy, 1.00106, 4000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Buy, 1.00105, 5000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Buy, 1.00104, 6000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Buy, 1.00103, 7000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Buy, 1.00102, 8000);
        Scenarios.executeSimpleOrder(_keithsSessionToken, currencyPair, Side.Buy, 1.00101, 9000); // no 1.00100, that's the mid
    }

    private void logonKeith() {
        _keithsSessionToken = TestUsers.keith().logon().getSessionToken();
        clearWebOutboundMessages();
    }

    private void logonRay() {
        _raysSessionToken = TestUsers.ray().logon().getSessionToken();
        clearWebOutboundMessages();
    }

    private void assertRung(OrderBookUpdateResponseDto orderBookUpdateDto, int index, OrderBookRungDto.OrderBookRungTypeDto type, BigDecimal rate, BigDecimal notional) {
        OrderBookRungDto rung = orderBookUpdateDto.getRungs(index);
        assertEquals(type, rung.getType());

        assertEquals(rate.unscaledValue().longValue(), rung.getRate().getUnscaledValue());
        assertEquals(rate.scale(), rung.getRate().getScale());

        assertEquals(notional.unscaledValue().longValue(), rung.getMarketSizeNotional().getUnscaledValue());
        assertEquals(notional.scale(), rung.getMarketSizeNotional().getScale());
    }
}
