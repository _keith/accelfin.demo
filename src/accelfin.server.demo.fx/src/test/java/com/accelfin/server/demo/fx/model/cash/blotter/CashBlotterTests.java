package com.accelfin.server.demo.fx.model.cash.blotter;

import com.accelfin.server.SessionToken;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.dtos.CashBlotterSubscribeResponseDto;
import com.accelfin.server.demo.fx.dtos.CashBlotterUpdateResponseDto;
import com.accelfin.server.demo.fx.model.events.FxSpotPriceEvent;
import com.accelfin.server.demo.fx.model.infrastructure.FxModelTestBase;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CashBlotterTests extends FxModelTestBase {

    @Override
    public void setUp() throws Exception {
        super.setUp();
        initialiseModel();
    }

    @Test
    public void stateOfTheWorldSentToNewSessions() throws Exception {
        // pre load some fake trades that will form the SOW
        FxSpotPriceEvent price = Scenarios.publishPrice("EURUSD", BigDecimal.valueOf(1.41), BigDecimal.valueOf(1.42));
        Scenarios.preLoadSpotTrade("EURUSD", Side.Buy, 100, price);
        Scenarios.preLoadSpotTrade("EURUSD", Side.Buy, 200, price);
        Scenarios.preLoadSpotTrade("EURUSD", Side.Buy, 300, price);

        SessionToken sessionToken = TestUsers.keith().logon().getSessionToken();
        clearWebOutboundMessages();

        // subscribe and assert an ACK was sent for the subscribe
        Scenarios.subscribeToCashBlotter(sessionToken);
        CashBlotterSubscribeResponseDto message1 = getWebOutboundMessagesPayload(0);
        assertTrue(message1.getIsSuccess());

        // assert the SOW was sent as part of the subscribe
        CashBlotterUpdateResponseDto payload = getWebOutboundMessagesPayload(1);
        assertEquals(3, payload.getTradesCount());
    }
}