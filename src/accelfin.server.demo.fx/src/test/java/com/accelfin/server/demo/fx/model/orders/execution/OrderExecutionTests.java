package com.accelfin.server.demo.fx.model.orders.execution;

import com.accelfin.server.SessionToken;
import com.accelfin.server.demo.common.model.Side;
import com.accelfin.server.demo.fx.dtos.ExecuteSimpleOrderResponseDto;
import com.accelfin.server.demo.fx.model.infrastructure.FxModelTestBase;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class OrderExecutionTests extends FxModelTestBase {
    private SessionToken _sessionToken;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        initialiseModel();
        _sessionToken = TestUsers
                .keith()
                .logon()
                .getSessionToken();
        clearWebOutboundMessages();
    }

    @Test
    public void onExecuteSimpleOrderItGetsPlacedInTheOrderRepository() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Buy, 1.123);
        assertEquals(1, SimpleOrderTradeRepository.getNumberOfTrades());
    }

    @Test
    public void onExecuteSimpleOrderAckIsSentToClient() throws Exception {
        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Buy, 1.123);
        ExecuteSimpleOrderResponseDto responseDto =  getWebOutboundMessagesPayload(0);
        assertTrue(responseDto.getIsSuccess());
    }

//    @Test
//    public void executeSimpleBuyOrderFailsIfRateIsBelowAskingPrice() throws Exception {
//        Scenarios.publishPrice("EURUSD", 1.00099, 1.00101); // bank side prices
//        Scenarios.executeSimpleOrder(_sessionToken, "EURUSD", Side.Buy, 1.00100);
//        ExecuteSimpleOrderResponseDto responseDto =  getWebOutboundMessagesPayload(0);
//        assertFalse(responseDto.getIsSuccess());
//    }
}