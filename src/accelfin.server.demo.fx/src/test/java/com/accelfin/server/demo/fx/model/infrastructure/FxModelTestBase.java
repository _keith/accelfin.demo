package com.accelfin.server.demo.fx.model.infrastructure;

import com.accelfin.server.Clock;
import com.accelfin.server.test.TestScheduler;
import com.accelfin.server.demo.common.messaging.ConnectionIds;
import com.accelfin.server.messaging.OutboundMessage;
import com.accelfin.server.messaging.events.InitEvent;
import com.accelfin.server.demo.fx.container.FxContainerModule;
import com.accelfin.server.demo.fx.dtos.ClientHeartbeatRequestDto;
import com.accelfin.server.demo.fx.model.FxService;
import com.accelfin.server.demo.fx.model.common.TradeRepository;
import com.accelfin.server.demo.fx.model.cash.blotter.SpotTrade;
import com.accelfin.server.demo.fx.model.orders.blotter.SimpleOrder;
import com.accelfin.server.demo.fx.model.cash.pricing.PriceRepository;
import com.esp.CurrentThreadRouterDispatcher;
import com.esp.DefaultRouter;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import org.junit.Before;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class FxModelTestBase {

    protected FxService Model;
    protected DefaultRouter<FxService> Router;
    protected TradeRepository<SimpleOrder> SimpleOrderTradeRepository;
    protected TradeRepository<SpotTrade> SpotTradeRepository;
    protected PriceRepository PriceRepository;
    protected Clock Clock;
    protected Injector Injector;
    protected Scenarios Scenarios;
    protected TestScheduler TestScheduler;
    protected TestUsers TestUsers;
    protected ArrayList<OutboundMessage> WebOutboundMessages;
    /**
     * Clears any messages that were previously sent to the web connection
     */
    protected void clearWebOutboundMessages() {
        WebOutboundMessages.clear();
    }

    /**
     * Gets (via casting) the OutboundMessage payload DTO of the message was sent via the web connection using the given index
     */
    protected <T> T getWebOutboundMessagesPayload(int messageIndex) {
        return (T)WebOutboundMessages
                .get(messageIndex)
                .getPayload();
    }

    @Before
    public void setUp() throws Exception {
        WebOutboundMessages = new ArrayList<>();
        createInjector();
        setupModel();
        setRepositories();
        Scenarios = new Scenarios(this);
        TestUsers = new TestUsers(this);
        observeOutBoundMessages();
    }

    protected void initialiseModel() {
        Router.publishEvent(new InitEvent());
    }

    protected void setupModel() {
        Model = Injector.getInstance(FxService.class);
        Clock = Injector.getInstance(Clock.class);
        Router = Injector.getInstance(Key.get(new TypeLiteral<DefaultRouter<FxService>>(){}));
        Router.setModel(Model);
        Model.observeEvents();
    }

    protected void setRepositories() {
        SimpleOrderTradeRepository = Injector.getInstance(Key.get(new
                                                                          TypeLiteral<TradeRepository<SimpleOrder>>(){}));
        SpotTradeRepository = Injector.getInstance(Key.get(new TypeLiteral<TradeRepository<SpotTrade>>(){}));
        PriceRepository = Injector.getInstance(Key.get(new TypeLiteral<PriceRepository>(){}));
    }

    protected void createInjector() {
        TestScheduler = new TestScheduler();
        Injector = Guice.createInjector(
                new FxContainerModule(new CurrentThreadRouterDispatcher(), TestScheduler, TestConnectionProvider.class)
        );
    }

    protected void observeOutBoundMessages() {
        Router.getModelObservable().observe(model -> {
            ArrayList<OutboundMessage> outboundMessages = model.getConnections().getConnection(ConnectionIds.WEB).getOutboundMessages()
                    .stream()
                    .filter(message -> !message.getPayload().getClass().equals(ClientHeartbeatRequestDto.class)) // ignore heartbeats.... too many
                    .collect(Collectors.toCollection(ArrayList::new));
            WebOutboundMessages.addAll(outboundMessages);
        });
    }
}
