package com.accelfin.server.demo.fx.model.fakeData;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Iterator;

import static com.accelfin.server.demo.fx.model.BigDecimalUtils.assertBigDecimalEquals;

public class PriceListTests {
    private PriceList _priceList;
    private Iterator<BidAndAsk> _priceIterator;

    @Before
    public void setUp() throws Exception {
        _priceList = new PriceList();
        _priceList.add(BigDecimal.valueOf(1), BigDecimal.valueOf(2));
        _priceList.add(BigDecimal.valueOf(2), BigDecimal.valueOf(3));
        _priceList.add(BigDecimal.valueOf(3), BigDecimal.valueOf(4));
        _priceIterator =_priceList.iterator();
    }

    @Test
    public void iteratorLoopsForwardThenReverse() throws Exception {
        assertNext(1, 2);
        assertNext(2, 3);
        assertNext(3, 4);
        assertNext(2, 3);
        assertNext(1, 2);
        assertNext(2, 3);
        assertNext(3, 4);
        assertNext(2, 3);
    }

    private void assertNext(int bid, int ask) {
        BidAndAsk bidAndAsk = _priceIterator.next();
        assertBigDecimalEquals(BigDecimal.valueOf(bid), bidAndAsk.getBid());
        assertBigDecimalEquals(BigDecimal.valueOf(ask), bidAndAsk.getAsk());
    }

}